$(document).ready(function() {
	let newsTitle = $(".News .NewsCard_title");
	for (var i = 0; i < newsTitle.length; i++) {
		var length = $(newsTitle[i]).html().length;
		if (length > 150) {
			$(newsTitle[i]).html(
				$(newsTitle[i])
					.html()
					.slice(0, 150) + " ..."
			);
		}
	}

	let BreadcrTitle = $(".Breadcrumbs_wrapper .BreadcrumbsNewsItem-Title");
	for (var i = 0; i < BreadcrTitle.length; i++) {
		var length = $(BreadcrTitle[i]).html().length;
		if (length > 120) {
			$(BreadcrTitle[i]).html(
				$(BreadcrTitle[i])
					.html()
					.slice(0, 120) + " ..."
			);
		}
	}
});

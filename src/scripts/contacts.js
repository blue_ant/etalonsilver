$(function() {
    const MAP_HORIZONTAL_SHIFT = 0.001;
    var active = '_active';

    ymaps.ready(init);

    function init() {
        let buildingLocation = {
            lat: 55.808775,
            lng: 37.646775,
        };

        let polygonCoords = [
            [55.81007, 37.64956],
            [55.809774, 37.649762],
            [55.809831, 37.650121],
            [55.809303, 37.65048],
            [55.809134, 37.649833],
            [55.808811, 37.649586],
            [55.808594, 37.650203],
            [55.807939, 37.649666],
            [55.80791, 37.649789],
            [55.807851, 37.64973],
            [55.807718, 37.650175],
            [55.807828, 37.650288],
            [55.807692, 37.650749],
            [55.807784, 37.650835],
            [55.807725, 37.651006],
            [55.806757, 37.649925],
            [55.806525, 37.649356],
            [55.807304, 37.646382],
            [55.807815, 37.646213],
            [55.807827, 37.645907],
            [55.807923, 37.64591],
            [55.807929, 37.646253],
            [55.808093, 37.646191],
            [55.808722, 37.646293],
            [55.808734, 37.646143],
            [55.808974, 37.646189],
            [55.808986, 37.646355],
            [55.809682, 37.6465],
        ];

        let active = '_active';

        var myMap = new ymaps.Map("map", {
            center: [buildingLocation.lat, buildingLocation.lng - MAP_HORIZONTAL_SHIFT],
            zoom: 17,
            controls: []
        });
        var fullscreenControl = new ymaps.control.FullscreenControl({
            options: {
                maxWidth: 26,
                position: {
                    top: 200,
                    right: 40
                }
            }
        });
        var zoomControl       = new ymaps.control.ZoomControl({
            options: {
                size: "small",
                position: {
                    top: 120,
                    right: 40
                }
            }
        });

        myMap.controls.add(zoomControl);
        myMap.controls.add(fullscreenControl);

        function setCenter(lat, lng) {
            myMap.setCenter([lat, lng], 17);
        }

        function addMarker(options, template) {
            var placemark = new ymaps.Placemark(
                [options.position[0], options.position[1]],
                {
                    hintContent: options.title,
                    balloonContent: template != undefined ? template : options.title,
                },
                {
                    // Опции.
                    // Необходимо указать данный тип макета.
                    iconLayout: "default#image",
                    // Своё изображение иконки метки.
                    iconImageHref: options.icon.url,
                    // Размеры метки.
                    iconImageSize: options.icon.size,
                    // Смещение левого верхнего угла иконки относительно
                    // её "ножки" (точки привязки).
                    iconImageOffset: [(-1 * options.icon.size[0]) / 2, -1 * options.icon.size[1]],
                }
            );

            myMap.geoObjects.add(placemark);
        }

        function addPolygon() {
            var myPolygon = new ymaps.Polygon(
                [
                    // Указываем координаты вершин многоугольника.
                    // Координаты вершин внешнего контура.
                    polygonCoords,
                ],
                {
                    // Описываем свойства геообъекта.
                    // Содержимое балуна.
                    hintContent: "",
                },
                {
                    // Задаем опции геообъекта.
                    // Цвет заливки.
                    fillColor: "rgba(255, 255, 255, 0)",
                    // Цвет обводки.
                    strokeColor: "#ccc",
                    // Ширина обводки.
                    strokeWidth: 5,
                }
            );

            // Добавляем многоугольник на карту.
            myMap.geoObjects.add(myPolygon);
        }

        function addRegions($item, options) {
            myMap.setZoom(options.zoom);
            myMap.setCenter(options.position);

            $item.find("[data-map-regions]").each(function() {
                let $this         = $(this);
                let regionOptions = $this.data("map-regions");
                let position      = regionOptions.position;
                let template      = $this.find("template").html();

                $this.on('click', function() {
                    myMap.balloon.open(position, template);
                    myMap.setCenter(position);
                });

                let regObject = new ymaps.Placemark(
                    [position[0], position[1]],
                    {
                        balloonContentBody: template,
                    },
                    {
                        // Опции.
                        // Необходимо указать данный тип макета.
                        iconLayout: "default#image",
                        // Своё изображение иконки метки.
                        iconImageHref: regionOptions.icon.url,
                        // Размеры метки.
                        iconImageSize: regionOptions.icon.size,
                        // Смещение левого верхнего угла иконки относительно
                        // её "ножки" (точки привязки).
                        iconImageOffset: [(-1 * regionOptions.icon.size[0]) / 2, -1 * regionOptions.icon.size[1]],
                    }
                );

                myMap.geoObjects.add(regObject);
            });
        }

        $("#accordion").on("show.bs.collapse", function(event) {
            let $item   = $(event.target);
            let options = $item.data("map");
            let id      = $item.attr("id").split("_")[1];

            myMap.geoObjects.removeAll();

            if (id == 4) {
                addRegions($item, options);

                return;
            }

            setCenter(options.position[0], options.position[1]);
            addMarker(options);

            if (id == 1) {
                addPolygon();
            }
        });

        $('.b-contacts-list__tlt[href="#contacts_1"]').trigger("click");

        $('[data-dropdown]').each(function() {
            let $dropdown = $(this);
            let $links    = $dropdown.find('.dropdown-menu__link');
            let $button   = $dropdown.find('.dropdown__btn');

            $links.on('click', function(e) {
                let $link    = $(this);
                let $parent  = $link.parent();
                let index    = $parent.index();
                let text     = $link.text();
                let $current = $('.b-contacts-mob-blocks__item').eq(index);
                let id       = $current.attr("id").split("_")[1];
                let options  = $current.data("map");

                e.preventDefault();

                $links.removeClass(active);
                $link.addClass(active);

                $button.text(text);

                $('.b-contacts-mob-blocks__item')
                    .hide()
                    .eq(index)
                    .show();

                myMap.geoObjects.removeAll();

                if (id == 4) {
                    addRegions($current, options);

                    return;
                }

                setCenter(options.position[0], options.position[1]);
                addMarker(options);

                if (id == 1) {
                    addPolygon();
                }
            });
        });
    }

    function contactsTeam() {
        let $contactsTeamBlockBtn = $('[data-contacts-team-btn]');
        function contentAddClass() {
            let $contactsTeamBlockHeight = $('[data-contacts-team-content]').height();
            $('.Content')
                .addClass('_team')
                .css({
                    "height" : $contactsTeamBlockHeight + 47,
                    "overflow" : "hidden"
                });
        };
        function contentRemoveClass() {
            $('.Content')
                .removeClass('_team')
                .css({
                    "height" : "",
                    "overflow" : ""
                });
        };

        if(window.matchMedia('(max-width: 768px)').matches){

            if ($contactsTeamBlockBtn.hasClass('_active')) {
                contentAddClass();
            } else {
                $contactsTeamBlockBtn.on('click', function () {
                    contentAddClass();
                });
                $('.dropdown-menu__link').not($contactsTeamBlockBtn).on('click', function () {
                    contentRemoveClass();
                });
            }

        } else {
            contentRemoveClass();
        }
    }

    contactsTeam();

    $(window).on('resize', function () {
        setTimeout(contactsTeam, 300);
    });


});

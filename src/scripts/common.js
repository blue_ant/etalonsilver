const $window  = $(window);
const $body    = $('body');
const $header  = $('.Header');
const $burger  = $('.Burger');
const $wrapper = $('.main-fullpage');
const $menuOverlay = $('.FullMenuOverlay');

{
	let loadVideoOnDesktop = function() {
		let sources = document.querySelectorAll('video source');
		if ( sources.length ) {
			let video = document.querySelectorAll('video');
			if ($window.width() >= 768) {
				for (let i = 0; i<sources.length;i++) {
					sources[i].setAttribute('src', sources[i].getAttribute('data-src-url'));
				}
				for (let p = 0; p<video.length;p++) {
					video[p].load();
				}

			} else {
				for (let i = 0; i<sources.length;i++) {
					sources[i].setAttribute('src', null);
				}
			}
		}
	};
	loadVideoOnDesktop();
	$window.on('resize', function() {
		loadVideoOnDesktop();
	})
}

$(() => {
	$('[data-video]').each(function() {
		let $wrapper = $(this);
		let $inner   = $wrapper.find('.longread-video__bg');
		let $video   = $wrapper.find('iframe');
		let curSrc   = $video.attr('src');

		$inner.on('click', function() {
			$(this).remove();
			$video.attr('src', curSrc + '&autoplay=1');
		});
	});

	if ($window.width() > 1023 && $body.height() < 400) {
		setTimeout(function() {
			$(".Hide_Block").css("display", "flex");
			$header.css({
				"z-index": "0",
				"position": "static"
			});

			$('.Header_burger').css("position", "absolute")
			$("html").css("overflow", "hidden");
		}, 1000);
	} else {
		$(".Hide_Block").css("display", "none");
		$("html").css("overflow-x", "hidden");
		$header.css("z-index", false);
	}

	$burger.on("click", function() {
		let isIOS = /iPad|iPhone|iPod/.test(navigator.platform) || (navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1);
		if ( isIOS ) {
			$('.FullNavMenu').addClass('_ios');
		} else {
			$('.FullNavMenu').removeClass('_ios');
		}
		$('.FullNavMenu').toggleClass('FullNavMenu-active');
		$header.toggleClass('Header-active');
		$('body').toggleClass('lock-tablet');
		$(this).toggleClass('Burger-active');
		$menuOverlay.toggleClass('FullMenuOverlay-active');
	});

	$menuOverlay.on('click', function () {
		$burger.trigger('click');
	});

	(($header) => {
		let headerClass = 'Header-white';
		let burgerClass = 'Burger-blue';

		let initScroll = function($scroll) {
			$scroll.on('scroll', function() {
				let scrollTop = $scroll.scrollTop();

				if (scrollTop > 0) {
					$header.addClass(headerClass)
					$burger.addClass(burgerClass)
				} else {
					$header.removeClass(headerClass)
					$burger.removeClass(burgerClass)
				}
			});
		};

		if (!$header.hasClass(headerClass)) {
			$('html').css('overflow-x', 'visible');
			initScroll($window);
			initScroll($body);
		}
	})($header);

	$('[data-subscribe]').each(function() {
		let $wrapper = $(this);
		let $link    = $wrapper.find('.footer-subscribe__link');
		let $form    = $wrapper.find('.subscribe-form');
		let active   = '_active';

		$link.on('click', function(e) {
			let $this = $(this);

			e.preventDefault();

			if ($wrapper.hasClass(active)) {
				return;
			}

			$link.stop().animate({
				opacity: 0,
				height: 0
			}, function() {
				$(this).hide();
				$form.show().stop().animate({
					height: $form.get(0).scrollHeight
				});
			});
		});

		$form.on('submit', function(e) {
			let $this  = $(this);
			let url    = $this.data('action');
			let method = $this.attr('method');
			let mailBannerId = $('#banner').data('banner');
			let dataToSend = $.extend(true, $form.serializeObject(), {
				Submit: 1,
				url: window.location.href,
			});

			e.preventDefault();

			$.ajax({
				url: url,
				type: "json",
				method: method,
				data: dataToSend,
				success: function(response) {
					let date   = new Date();
					let expire = new Date(date.getFullYear() + 1, date.getMonth(), date.getDate(), 23, 59, 59);
					if (response.code === 0) {
						$wrapper.html('<p>' + response.success + '</p>');
						Cookies.set('banner-' + mailBannerId, '1',  { expires: expire }, { secure: true });
					}
				}
			})
		});
	});

	(($banners) => {
        if(Cookies.get('banner-' + $banners.data('banner')) ) {
			return;
		}

		let interval;

		$body.append("<style>.comagic-call-generator-background {display: none}</style>");

		countDown();

        function countDown() {
            let i = 0;

            interval = setInterval(function() {
                i = ++i;

                if (i === 15) bannersInit();
            }, 1000);

            $(document).on('mousemove', function(e) {
                if (e.clientY == 0) bannersInit();
            });
        }

        function bannersInit() {
            clearInterval(interval);

            $(document).off('mousemove');

            function disableBanner(order) {
                let now            = new Date();
                let midnight       = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 23, 59, 59);
                let daysToMidnight = (midnight - now) / 1000 / 60 / 60 / 24;

				if (!Cookies.get('banner-' + order)) {
					Cookies.set('banner-' + order, '1', { expires: daysToMidnight }, { secure: true });
				}
            }

            let orders = new Array;

            $banners.each(function(i, e) {
                let $this = $(this);
                let order = $this.data('banner');

                if (!Cookies.get('banner-' + order)) {
                    orders.push(order);
                }
            });

            let minOrder   = Math.min.apply(null, orders)
            let $minBanner = $('[data-banner="' + minOrder + '"]');

            if ($minBanner.find('a').length) {
                $minBanner.find('a').on('click', function() {
                    disableBanner(minOrder);
                });
            }

            $.fancybox.open($minBanner, {
            	touch: false,
                beforeClose: function() {
                    disableBanner(minOrder);
                },
                hideScrollbar: false
            });

            let $close = $('.close');

            $close.on('click touchstart', function() {
            	$.fancybox.close();
            });
        }
	})($('[data-banner]'));

	$('.page-form').on('submit', function(e) {
        e.preventDefault();
        let $form     = $(this);
        let url       = $form.data('action');
        let isPopup   = $form.hasClass('popup-form');
		let bannerId  = $form.closest('[data-banner]').data('banner');

        let dataToSend = $.extend(true, $form.serializeObject(), {
            Submit: 1,
            url: window.location.href,
        });

        $.ajax({
            url     : $form.data("action"),
            type    : $form.attr("method"),
            data    : dataToSend,
            success : function(response) {
                let errorCode = parseInt(response.code);

                if (errorCode === 0) {
                    $form.trigger('reset').hide();

                    $(".popup-form-wrapper").css("display", "none");
            		$(".thanks-form-wrapper").css("display", "block");
					if (isPopup) {
						let date   = new Date();
						let expire = new Date(date.getFullYear() + 1, date.getMonth(), date.getDate(), 23, 59, 59);
						Cookies.set('banner-' + bannerId, '1',  { expires: expire }, { secure: true });
					}
                } else {
                    console.log("Не удалось отправить форму! Попробуйте позже или обратитесть по телефону...");
                }
            }
        });
    });
});

$.fn.serializeObject = function() {
   var o = {};
   var a = this.serializeArray();

   $.each(a, function() {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
   });
   return o;
};

{
	let flatBackLink = $('[data-flat-back]');
	if (flatBackLink) {
		let href = flatBackLink.attr('href');
		flatBackLink.on('click', function (event) {
			if (window.history.length > 2) {
				event.preventDefault();
				window.history.back();
			}
		});
	}
}

let $fullNavMenu = $('.FullNavMenu');
let $fullNavMenuHeight = $fullNavMenu.outerHeight();
let fullNavMenu = () => {
	if ($window.width() > 767 && $window.height() <= $fullNavMenuHeight) {
		$fullNavMenu.css ({
			'height' : '100%',
			'overflow-y' : 'scroll'

		});
	} else {
		$fullNavMenu.css ({
			'height' : '',
			'overflow-y' : ''
		});
	}
};
fullNavMenu();
$window.resize(function() {
	fullNavMenu();
});

/*Помогите нам стать лучше*/
let $helpUsLink   = $('[data-helpus-link]');
let $helpUsWindow = $('[data-helpus-window]');
let $helpUsClose  = $('[data-helpus-close]');
$helpUsLink.on('click', function () {
	$helpUsWindow.addClass('_open');
});
$helpUsClose.on('click', function () {
	$helpUsWindow.removeClass('_open');
});
/* / Помогите нам стать лучше*/

/*Простой текстовый поп-ап*/
let $simplePopup	  = $('[data-simplepopup]');
let $simplePopupClose = $('[data-simplepopup-close]');
$simplePopupClose.on('click', function () {
	$simplePopup.removeClass('_open');
});
/* / Простой текстовый поп-ап*/
/*Кука к простому поп-апу*/
function simplePPCookes() {
	let $cookesName = $simplePopup.data('cookes');
	if (!Cookies($cookesName)) {
		$simplePopup.addClass('_open');
	}
	Cookies($cookesName, true, {
		expires: 1,
		path: '/'
	});
}
simplePPCookes();
/* / Кука к простому поп-апу*/
/*Кука согласие о сборе данных*/
$('[data-cookiesagree]').each(function() {
	let $wrapper = $(this);
	$wrapper.addClass('_hide');

	//Проверяем соглашались ли ранее.
	if (!Cookies.get('policy')) {
		$wrapper.removeClass('_hide');

		$('[data-cookiesagree-btn]').on('click', function() {
			$wrapper.addClass('_hide');
			//Устанавливаем куки на срок - 30 дней.
			Cookies.set('policy', '1', { expires : 30 });

		});

		return;
	}
});
/* / Кука согласие о сборе данных*/
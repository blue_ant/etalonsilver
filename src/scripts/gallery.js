$(document).ready(function() {
	$(".Gallery-wrapper .GalleryLink").on("mouseover", function() {
		TweenMax.to($(this).find(".GalleryLink__showenCont"), 0.7, { css: { opacity: "0" } });
		TweenMax.to($(this).find(".GalleryLink__hidden"), 0.7, { css: { transform: "scale(1.2)", display: "block" } });
		TweenMax.to($(this).find(".GalleryLink__hiddenCont"), 0.7, { css: { opacity: "1" } });
	});

	$(".Gallery-wrapper .GalleryLink").on("mouseout", function() {
		TweenMax.to($(this).find(".GalleryLink__showenCont"), 0.7, { css: { opacity: "1" } });
		TweenMax.to($(this).find(".GalleryLink__hidden"), 0.7, { css: { transform: "scale(1.4)", display: "none" } });
		TweenMax.to($(this).find(".GalleryLink__hiddenCont"), 0.7, { css: { opacity: "0" } });
	});

	$(window).resize(function() {
		if ($(window).width() > 1919 && $(window).height() > 900) {
			let link1 = $(".Gallery-wrapper .GalleryLink__1").height();
			let link2 = $(".GalleryLink.GalleryLink__2").height();
			let link3 = $(".GalleryLink.GalleryLink__3").height();

			$(".GalleryLink__1 .GalleryLink__inner, .Gallery-wrapper .GalleryLink__1 .GalleryLink__hidden").height(
				link1 * 0.87
			);
			$(".GalleryLink__2 .GalleryLink__inner, .Gallery-wrapper .GalleryLink__2 .GalleryLink__hidden").height(
				link2 * 0.78
			);
			$(".GalleryLink__3 .GalleryLink__inner, .Gallery-wrapper .GalleryLink__3 .GalleryLink__hidden").height(
				link2 * 0.78
			);
		}
	});

	if ($(window).width() > 1919 && $(window).height() > 900) {
		let link1 = $(".Gallery-wrapper .GalleryLink__1").height();
		let link2 = $(".GalleryLink.GalleryLink__2").height();

		$(".GalleryLink__1 .GalleryLink__inner, .Gallery-wrapper .GalleryLink__1 .GalleryLink__hidden").height(
			link1 * 0.87
		);
		$(".GalleryLink__2 .GalleryLink__inner, .Gallery-wrapper .GalleryLink__2 .GalleryLink__hidden").height(
			link2 * 0.78
		);
		$(".GalleryLink__3 .GalleryLink__inner, .Gallery-wrapper .GalleryLink__3 .GalleryLink__hidden").height(
			link2 * 0.78
		);
	}
});



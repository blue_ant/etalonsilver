let sliderBusiness;
sliderBusiness = new Swiper(".swiper-container", {
    spaceBetween: 0,
    //loop: true,
    loopedSlides: 700,
    slidesPerView: 1,
    centeredSlides: false,
    initialSlide: 1,
    resizeReInit: true,
    hashNavigation: true,
    observer: true,
    observeParents: true,
    observeSlideChildren: true,
    navigation: {
        nextEl: ".BusinessFurnish_nextButton.swiper-button-next",
        prevEl: ".BusinessFurnish_prevButton.swiper-button-prev",
    },

    breakpoints: {
        1023: {
            slidesPerView: 1,
            centeredSlides: true,
            resistanceRatio: 0,
        },

        1279: {
            slidesPerView: 1,
            centeredSlides: true,
        },

        2560: {
            slidesPerView: 3,
            centeredSlides: true,
            resistanceRatio: 0,
            resistance: false,
            threshold: 90000
        },
    },

    on: {
        init: function() {
        },
    },
});

let counter = $(".BusinessFurnish_slideCount"),
    sum = $(".BusinessFurnish_slideSum");

var totalSlides = sliderBusiness.slides.length;
$(sum).html(totalSlides);

$(counter).html(sliderBusiness.activeIndex + 1);
sliderBusiness.on("slideChange", function() {
    $(counter).html(sliderBusiness.activeIndex + 1);
});



//=include includes/blocks/validate.js

class Scheme {
  constructor(el) {
    this.$el = $(el);
    let $svgCont = this.$el.find(".Scheme_svgContainer");
    let $svg = (this.$svg = $svgCont.children(".Scheme_svg"));
    const isDesktop = device.desktop();

    this.setSvgHeightExplicity();

    let panzoom = svgPanZoom($svg.get(0), {
      mouseWheelZoomEnabled: false,
      preventMouseEventsDefault: isDesktop,
      contain: true,
      minZoom: 1,
      dblClickZoomEnabled: false,
      panEnabled: false,
    });

    $svgCont.addClass("Scheme_svgContainer-activated");

    this.$el.find(".Scheme_btn-plus").on("click", (event) => {
      event.preventDefault();
      panzoom.zoomIn();
      $svgCont.addClass("Scheme_svgContainer-draggable");
      panzoom.enablePan();
    });

    this.$el.find(".Scheme_btn-minus").on("click", function(event) {
      event.preventDefault();
      let currentZoom = Math.round(panzoom.getZoom() * 100) / 100;
      if (currentZoom === 1) {
        panzoom.resetPan();
        $svgCont.removeClass("Scheme_svgContainer-draggable");
        panzoom.disablePan();
      } else {
        panzoom.zoomOut();
      }
    });

    $(window).on(
      "resize",
      _.debounce(() => {
        if ($svg.is(":visible")) {
          this.setSvgHeightExplicity();
          panzoom.resize();
          panzoom.reset();
        }
      }, 80)
    );

    let lastMouseDownXcoords;

    $svg
      .on("mousedown", ".Scheme_item-active", function(event) {
        if (event.which === 1) {
          event.preventDefault();
          lastMouseDownXcoords = event.pageX;
        }
      })
      .on("mouseup", ".Scheme_item-active", (event) => {
        let mouseDownXcoordsShift;

        if (event.which === 1) {
          if (lastMouseDownXcoords > event.pageX) {
            mouseDownXcoordsShift = lastMouseDownXcoords - event.pageX;
          } else {
            mouseDownXcoordsShift = event.pageX - lastMouseDownXcoords;
          }

          if (mouseDownXcoordsShift < 2) {
            this.handleItemClick(event.currentTarget);
          }
        }
      });

    if (isDesktop) {
      $svg
        .on("mousedown", function() {
          if ($svgCont.hasClass("Scheme_svgContainer-draggable")) {
            $svgCont.addClass("Scheme_svgContainer-dragging");
          }
        })
        .on("mouseup", function() {
          $svgCont.removeClass("Scheme_svgContainer-dragging");
        });

      $.widget.bridge("uitooltip", $.ui.tooltip);

      $svg.uitooltip({
        items: ".Scheme_item-active",
        classes: {
          "ui-tooltip": "Scheme_tooltip",
          "ui-tooltip-content": "Scheme_ttInner",
        },
        track: true,
        hide: false,
        show: false,
        position: {
          my: "left+12 top+12",
        },

        content: function() {
          let data = $(this).data();
          let num = data.itemId;
          let price = (data.itemPrice / 1000000).toLocaleString("ru-RU");
          let area = data.itemArea.toLocaleString("ru-RU");
          return `
              <div class="Scheme_ttToprow">
                <span class="Scheme_ttNum">Место ${num}</span>
                <span class="Scheme_ttArea">${area} м²</span>
              </div>
              <div class="Scheme_ttPrice">${price} млн р.</div>`;
        },
      });
    }
  }

  setSvgHeightExplicity() {
    // IE11 svg scaling fix
    if (!!window.MSInputMethodContext && !!document.documentMode) {
      let widthSource = this.$svg.attr("width");
      let heightSource = this.$svg.attr("height");
      let aspectRatio = widthSource / heightSource;
      let actualWidth = this.$svg.width();
      let explicitHeight = Math.round(actualWidth / aspectRatio);
      this.$svg.css("min-height", explicitHeight);
    }
  }

  handleItemClick(el) {
    let $item = $(el);
    let num = $item.data("item-id");
    let house = $item.data("house");
    let price = ($item.data("item-price") / 1000000).toLocaleString("ru-RU");
    let area = $item.data("item-area").toLocaleString("ru-RU");

    let $getInfoModal = $("#parkingGetInfoModal");

    $getInfoModal.find("#numValParkingModal").html(num);
    $getInfoModal.find("#priceValParkingModal").html(price);
    $getInfoModal.find("#areaValParkingModal").html(area);

    $("#parking_modal_num").val(num);
    $("#parking_modal_house").val(house);

    $.fancybox.open($getInfoModal, {
      modal: true,
      afterClose: function() {
        if (this.type === "inline") {
          let $form = this.$content.find("form");
          let validator = $form.trigger("reset").data("validator");
          if (validator) {
            validator.resetForm();
            $form.find("input.error").removeClass("error");
          }

          let $formContainer = this.$content.find(".Modal_inner-withForm");
          let $successText = this.$content.find(".Modal_inner-formSuccess");

          if ($successText.length) {
            $successText.hide();
            $formContainer.show();
          }
        }
      },
    });
  }
}

{
  let scheme;
  let $parkingSchemeCont = $("#parkingScheme");
  if ($parkingSchemeCont.is(":visible")) {
    scheme = new Scheme($parkingSchemeCont);
  }

  $(window).on(
    "resize.schemeInit",
    _.debounce(function(event) {
      event.preventDefault();
      if (!scheme && $parkingSchemeCont.is(":visible")) {
        scheme = new Scheme($parkingSchemeCont);
        $(window).off(".schemeInit");
      }
    }, 50)
  );
}

{
  let tableMarkup = "";
  let $tbl = $(".Parking_table .Table");

  $("#parkingScheme")
    .find(".Scheme_item-active")
    .each(function(index, el) {
      let data = $(el).data();
      let num = data.itemId;
      let price = (data.itemPrice / 1000000).toLocaleString("ru-RU");
      let area = data.itemArea.toLocaleString("ru-RU");
      tableMarkup += `<tr class="Table_tr" data-item-id="${num}"><td class="Table_td Table_td-centerTxt">№${num}</td><td class="Table_td Table_td-centerTxt">${area} м²</td><td class="Table_td Table_td-centerTxt">${price} млн Р</td></tr>`;
    });

  $tbl
    .find("tbody")
    .html(tableMarkup)
    .on("click", ".Table_tr", function(event) {
      event.preventDefault();
      let id = event.currentTarget.dataset.itemId;
      Scheme.prototype.handleItemClick(
        $("#parkingScheme").find(`[data-item-id="${id}"]`)
      );
    });

  $tbl
    .paginate({
      elemsPerPage: 10,
      maxButtons: 5,
      containerClass: "Pager",
      listClass: "Pager_list",
      pageClass: "Pager_item",
      anchorClass: "Pager_link",
      previousClass: "Pager_prev",
      nextClass: "Pager_next",
      disabledClass: "disabled",
      activeClass: "active",
      previousSetClass: "Pager_prevSet",
      nextSetClass: "Pager_nextSet",
      showAllListClass: "Pager_showAllList",
      nextText:
        '<svg class="SvgIco" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="5" height="14" viewBox="0 0 5 14"><path class="SvgIco_path" d="M4.648 7.5q0 0.102-0.078 0.18l-3.641 3.641q-0.078 0.078-0.18 0.078t-0.18-0.078l-0.391-0.391q-0.078-0.078-0.078-0.18t0.078-0.18l3.070-3.070-3.070-3.070q-0.078-0.078-0.078-0.18t0.078-0.18l0.391-0.391q0.078-0.078 0.18-0.078t0.18 0.078l3.641 3.641q0.078 0.078 0.078 0.18z"></path></svg>',
      previousText:
        '<svg class="SvgIco" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="5" height="14" viewBox="0 0 5 14"><path class="SvgIco_path" d="M4.898 4.25q0 0.102-0.078 0.18l-3.070 3.070 3.070 3.070q0.078 0.078 0.078 0.18t-0.078 0.18l-0.391 0.391q-0.078 0.078-0.18 0.078t-0.18-0.078l-3.641-3.641q-0.078-0.078-0.078-0.18t0.078-0.18l3.641-3.641q0.078-0.078 0.18-0.078t0.18 0.078l0.391 0.391q0.078 0.078 0.078 0.18z"></path></svg>',
    })
    .show()
    .prev()
    .appendTo(".Parking_tablePagination");
}

{
  let $parkingModalBlock = $("#parkingGetInfoModal");

  $("#parkingGetInfoModal")
    .find("form")
    .on("submit", function(event) {
      event.preventDefault();

      let $form = $(event.target);
      let dataToSend = $.extend(true, $form.serializeObject(), {
          Submit: 1,
          url: window.location.href
      });


      _.delay(() => {
        if ($form.find(".error").length === 0) {
          $.ajax({
            url: $form.data("url"),
            type: $form.prop("method"),
            data: dataToSend,
          })
            .done(function() {
              $parkingModalBlock.find(".Modal_inner-withForm").hide();
              $parkingModalBlock.find(".Modal_inner-formSuccess").show();
            })
            .fail(function() {
              alert(
                "Не удалось отправить форму! Попробуйте позже или обратитесь к адинистрации сайта."
              );
            });
        }
      }, 100);
    });
}

$("#parkingCallMeButton").on("click", function(event) {
  event.preventDefault();
  $(".Header_callMe")
    .eq(0)
    .trigger("click");
});

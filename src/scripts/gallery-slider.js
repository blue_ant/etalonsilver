$(document).ready(function() {
    let BreadcrTitle = $(".Breadcrumbs_wrapper .BreadcrumbsNewsItem-Title");
    for (var i = 0; i < BreadcrTitle.length; i++) {
        var length = $(BreadcrTitle[i]).html().length;
        if (length > 120) {
            $(BreadcrTitle[i]).html(
                $(BreadcrTitle[i])
                    .html()
                    .slice(0, 120) + " ..."
            );
        }
    }

    var swiper = new Swiper(".swiper-container", {
        spaceBetween: 0,
        loop: true,
        //loopedSlides: 10,
        loopAdditionalSlides: 0,
        slidesPerView: "auto",
        centeredSlides: true,
        // initialSlide: 1,
        resizeReInit: true,
        hashNavigation: true,
        scrollbarHide: true,
        resistanceRatio: 0,
        navigation: {
            nextEl: ".Gallery-btn.swiper-button-next",
            prevEl: ".Gallery-btn.swiper-button-prev",
        },

        breakpoints: {
            767: {
                // slidesPerView: 1,
                freeMode: true,
                centeredSlides: false,
                scrollbar: {
                    el: '.swiper-scrollbar',
                    hide: true,
                    draggable: true,
                }
            },

            1281: {
                // slidesPerView: 1.2,
                centeredSlides: true,
            },

            2560: {
                // slidesPerView: 1.5,
                centeredSlides: true,
            },
        },

        on: {
            init: function() {
                initSwipe();
            },
            slideNextTransitionStart: function () {
                slideSignHide();
            },
            slideChangeTransitionEnd: function () {
                slideSign();
            }
        },
    });

    {
        let isActiveSlide = $('.swiper-slide-active').length;
        if (isActiveSlide) {
            let activeSlideHash = $('.swiper-slide-active').data('hash');
            window.location.hash = activeSlideHash;
        }

    }


    $(window).resize(function() {
        swiper.destroy(true, true);
        swiper = new Swiper(".swiper-container", {
            spaceBetween: 0,
            loop: true,
            //loopedSlides: 10,
            slidesPerView: "auto",
            centeredSlides: true,
            // initialSlide: 1,
            resizeReInit: true,
            hashNavigation: true,
            scrollbarHide: true,
            resistanceRatio: 0,
            navigation: {
                nextEl: ".Gallery-btn.swiper-button-next",
                prevEl: ".Gallery-btn.swiper-button-prev",
            },

            breakpoints: {
                767: {
                    // slidesPerView: 1,
                    freeMode: true,
                    centeredSlides: false,
                    scrollbar: {
                        el: '.swiper-scrollbar',
                        hide: true,
                        draggable: true,
                    }
                },

                1281: {
                    // slidesPerView: 1.2,
                    centeredSlides: true,
                },

                2560: {
                    // slidesPerView: 1.5,
                    centeredSlides: true,
                },
            },

            on: {
                init: function() {
                    initSwipe();
                },
                slideNextTransitionStart: function () {
                    slideSignHide();
                },
                slideChangeTransitionEnd: function () {
                    slideSign();
                }
            },
        });
    });

    let counter = $(".Gallery_slideCount"),
        sum = $(".Gallery_slideSum");


    let slides = [];

    _.forEach(swiper.slides, function(slide) {
        let isDuplicate = slide.classList.contains('swiper-slide-duplicate');
        if ( !isDuplicate ) {
            slides.push(slide);
        }
    });
    let totalSlides = slides.length;
    $(sum).html(totalSlides);

    $(counter).html(swiper.realIndex + 1);
    swiper.on("slideChange", function() {
        // if ($(window).width() > 1023) {
        //     if (swiper.previousIndex - swiper.activeIndex < 0 && swiper.activeIndex != 0) {
        //         $(".swiper-slide-active").css({ opacity: "0.4" });
        //         $(".swiper-slide-active")
        //             .next()
        //             .css({ opacity: "1" });
        //         $(".swiper-slide-active")
        //             .next()
        //             .next()
        //             .css({ opacity: "0.4" });
        //     }
        //
        //     if (swiper.activeIndex > 0) {
        //         $(".swiper-slide-active").css({ opacity: "0.4" });
        //         $(".swiper-slide-active")
        //             .prev()
        //             .css({ opacity: "1" });
        //         $(".swiper-slide-active")
        //             .prev()
        //             .prev()
        //             .css({ opacity: "0.4" });
        //     }
        //
        //     if (swiper.activeIndex == 0) {
        //         $(".swiper-slide-active")
        //             .prev()
        //             .css({ opacity: "1" });
        //         $(".swiper-slide-active").css({ opacity: "0.4" });
        //     }
        // }

        $(counter).html(swiper.realIndex + 1);
    });

    function initSwipe() {
        // if ($(window).width() > 1023) {
        //     $(".swiper-slide-active").css({ opacity: "1" });
        //     $(".swiper-slide-active")
        //         .prev()
        //         .css({ opacity: "0.4" });
        //     $(".swiper-slide-active")
        //         .next()
        //         .css({ opacity: "0.4" });
        // }
    }

    if ($(window).width() > 1279 && $(window).width() < 1920) {
        let line = $("#line").offset().left - 48;
        $("h1.Gallery_sliderNav_title").css({ "padding-left": line + "px" });
    }

    $(window).resize(function() {
        if ($(window).width() > 1279 && $(window).width() < 1920) {
            let line = $("#line").offset().left - 48;
            $("h1.Gallery_sliderNav_title").css({ "padding-left": line + "px" });
        }
    });
});

let $slideSignDiv = $('[data-sign-div]');
function slideSignHide() {
    $slideSignDiv.addClass('_hide');
}
function slideSign() {
    let $slideSignTxt     = $('.swiper-slide-active').data('sign');
    let $slideSignDivWrap = $slideSignDiv.parent();

    $slideSignDivWrap.addClass('_hide');
    if ( $slideSignTxt ) {
        $slideSignDivWrap.removeClass('_hide');
        $slideSignDiv.removeClass('_hide').html($slideSignTxt);
    }
}
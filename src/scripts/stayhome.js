function stayHomeItemHeight() {
    let $stayHomeItem = $('[data-stayhome-item]');
    let $stayHomeItemCount = $stayHomeItem.length / 2 + 1;

    for (let i = 1; i <= $stayHomeItemCount; i++) {
        let dataA = '[data-stayhome-item-' + i + 'a]';
        let dataB = '[data-stayhome-item-' + i + 'b]';

        let paddingTop = 42;
        if (window.matchMedia("(max-width: 1023px)").matches) {
            paddingTop = 32;
        }

        let $stayHomeItemA = $(dataA);
        let $stayHomeItemHeightA = $stayHomeItemA.find('.StayHomeSteps_txt').height() + paddingTop;

        let $stayHomeItemB = $(dataB);
        let $stayHomeItemHeightB = $stayHomeItemB.find('.StayHomeSteps_txt').height() + paddingTop;


        if (window.matchMedia("(min-width: 768px)").matches) {
            if ($stayHomeItemHeightA > $stayHomeItemHeightB) {
                $stayHomeItemA.css('height', $stayHomeItemHeightA + 'px');
                $stayHomeItemB.css('height', $stayHomeItemHeightA + 'px');
            } else {
                $stayHomeItemA.css('height', $stayHomeItemHeightB + 'px');
                $stayHomeItemB.css('height', $stayHomeItemHeightB + 'px');
            }
        } else {
            $stayHomeItemA.css('height', '');
            $stayHomeItemB.css('height', '');
        }
    }
}

setTimeout(stayHomeItemHeight, 200);

$(window).on('resize', function () {
    stayHomeItemHeight();
});
{
	let link = document.createElement("link");
	link.rel = "stylesheet";
	link.href = "/css/style2.css";
	document.head.appendChild(link);
}

class FlatsList {
	constructor(opts = {}) {
		let tpl;
		if (opts.tpl) {
			tpl = opts.tpl;
		} else {
			tpl = `
/*= include includes/chunks/FlatsList.tpl*/
`;
		}
		this.compiledTemplate = _.template(tpl.replace(/\s{2,}/g, ""));

		let $cont = $(opts.mountEl);
		this.mountEl = $cont.get(0);
	}

	render(data) {
		this.mountEl.innerHTML = this.compiledTemplate(data);
	}
}

class Switch {
	constructor(el, opts = {}) {
		this.$el = $(el).eq(0);
		this.$checkbox = this.$el.find(".Switch_checkbox");
		this.$labels = this.$el.find(".Switch_label");
		this.$labels.on("click", event => {
			event.preventDefault();
			this.$checkbox.trigger("click");
		});
		this.$checkbox.on("change", () => {
			this.$labels.toggleClass("Switch_label-active");
			if (opts.onChange) {
				let isChecked = this.$checkbox.is(":checked");
				opts.onChange(isChecked, this.$el);
			}
		});
	}

	// methods
}

class MortgageSlider {
	constructor(el, opts, addOpts) {
		let $el = $(el);

		if (!$el.length) {
			console.error('MortgageSlider constructor can\'t find given "el" in DOM!');
			return;
		}

		this.customCounterRender = addOpts.customCounterRender ? addOpts.customCounterRender : false;
		this.$inp = $el.find("input");

		let $counter = addOpts.$counter;
		let minAllowedVal = parseInt(this.$inp.data("min-value"));
		let maxAllowedVal = parseInt(this.$inp.data("max-value"));
		let options = {
			classes: {
				"ui-slider-handle": "MortgageSlider_handle",
				"ui-slider-range": "MortgageSlider_progress"
			},
			min: minAllowedVal,
			max: maxAllowedVal,
			range: "min",
			value: this.$inp.val(),
			create: (event /*, ui*/) => {
				this._setValue(this.$inp.val());

				$(event.target)
					.find(".MortgageSlider_handle")
					.insertBefore($(event.target).find(".MortgageSlider_progress"));
			},
			slide: (event, ui) => {
				this._setValue(ui.value);
				if (addOpts.slideEventCallback) addOpts.slideEventCallback();
			}
		};

		if (opts) {
			for (let key in opts) {
				options[key] = opts[key];
			}
		}

		let $track = $("<div class='MortgageSlider_track'></div>");
		$track.slider(options).appendTo($el);

		$counter.on("click", event => {
			if (event.currentTarget.getElementsByTagName("input").length) return false;

			let $popupInp = $("<input type='text' class='MortgageSlider_popupInp'>");
			$popupInp
				.val(this.$inp.val())
				.inputmask("integer", {
					groupSeparator: " ",
					autoGroup: true,
					allowMinus: false,
					rightAlign: false
				})
				.on("change", event => {
					event.preventDefault();
					let $tmpInp = $(event.currentTarget);
					let val = parseInt(
						$tmpInp
							.val()
							.split(" ")
							.join("")
					);
					if (isNaN(val)) val = minAllowedVal;
					this.setHandlerPosition(val);
					if (addOpts.slideEventCallback) addOpts.slideEventCallback();
				})
				.on("focusout", event => {
					$(event.currentTarget).trigger("change");
				});
			this.$counter.html($popupInp);
			$popupInp.trigger("focus");
		});

		this.minAllowedVal = minAllowedVal;
		this.maxAllowedVal = maxAllowedVal;
		this.$track = $track;
		this.$counter = $counter;
		this.$el = $el;
	}

	getValue() {
		return this.$inp.val();
	}

	_setValue(v) {
		v = parseInt(v);
		this.$inp.val(v);

		window.requestAnimationFrame(() => {
			if (this.customCounterRender) {
				let valToShow = this.customCounterRender(v);
				this.$counter.html(valToShow);
			} else {
				this.$counter.html(parseInt(v).toLocaleString("ru-RU"));
			}
		});
	}

	setHandlerPosition(v) {
		if (v < this.minAllowedVal) {
			v = this.minAllowedVal;
		}

		if (v > this.maxAllowedVal) {
			v = this.maxAllowedVal;
		}
		this._setValue(v);
		this.$track.slider("value", v);
	}
}

class MortgageCalc {
	constructor(formEl, banksData, opts) {
		this.$form = $(formEl);
		let that = this;
		if (!this.$form.length) {
			console.error('MortgageCalc constructor can\'t find given "formEl" in DOM!');
			return;
		}

		this.$form.on("submit", function(event) {
			event.preventDefault();
			return false;
		});

		$('.MortgageCalc_sum').on('change', (e) => {
			this.update();
		})

		this.banksListBuilder = opts.banksListBuilder;
		this.flatListBuilder = opts.flatListBuilder;

		this._onFormElementsChange = _.debounce(event => {
			// ignore sliders events fired throught JS-API
			if (event.target.parentNode.classList.contains("MortgageSlider") && !event.originalEvent) return false;
			this.update();
		}, 100);

		this.$creditSummCount = this.$form.find(".MortgageCalc_summVal");
		this.banksData = banksData;
		this.initSliders();
	}

	filterAndRenderBanks() {
		throw new TypeError("Cannot construct Abstract instances directly");
	}

	getAndRenderFlats() {
		throw new TypeError("Cannot construct Abstract instances directly");
	}
	initSliders() {
		throw new TypeError("Cannot construct Abstract instances directly");
	}

	update() {
		this.filterAndRenderBanks();
		this.getAndRenderFlats();
	}
}

class MortgagePaymentCalc extends MortgageCalc {
	initSliders() {
		{
			let $formSliderBlock = this.$form.find(".MortgageCalc_cell-firstPayment");

			var firstPaymentSliderInst = new MortgageSlider(
				$formSliderBlock.find(".MortgageSlider"),
				{
					change: this._onFormElementsChange
				},
				{
					slideEventCallback: (/*event, ui*/) => {
						let selectedFirstPayment = firstPaymentSliderInst.getValue();
						let selectedPrice = priceSliderInst.getValue();

						if (selectedFirstPayment >= selectedPrice * 0.9) {
							priceSliderInst.setHandlerPosition(Math.round(selectedFirstPayment * 1.15));
						}
					},
					$counter: $formSliderBlock.find(".MortgageCalc_sum"),
					customCounterRender: v => {
						return `${v.toLocaleString("ru-RU")}&nbsp;<span class="MortgageCalc_sign">₽</span>`;
					}
				}
			);
		}

		{
			let $formSliderBlock = this.$form.find(".MortgageCalc_cell-price");

			var priceSliderInst = new MortgageSlider(
				$formSliderBlock.find(".MortgageSlider"),
				{
					change: this._onFormElementsChange
				},
				{
					slideEventCallback: (/*event, ui*/) => {
						let selectedFirstPayment = firstPaymentSliderInst.getValue();
						let selectedPrice = priceSliderInst.getValue();

						if (selectedFirstPayment >= selectedPrice * 0.9) {
							firstPaymentSliderInst.setHandlerPosition(Math.round(selectedPrice * 0.9));
						}
					},
					$counter: $formSliderBlock.find(".MortgageCalc_sum"),
					customCounterRender: v => {
						return `${v.toLocaleString("ru-RU")}&nbsp;<span class="MortgageCalc_sign">₽</span>`;
					}
				}
			);
		}

		{
			let $formSliderBlock = this.$form.find(".MortgageCalc_cell-duration");

			new MortgageSlider(
				$formSliderBlock.find(".MortgageSlider"),
				{
					change: this._onFormElementsChange
				},
				{
					$counter: $formSliderBlock.find(".MortgageCalc_sum"),
					customCounterRender: v => {
						if (v === 1) {
							return "1 год";
						} else {
							return moment.duration(v, "years").humanize();
						}
					}
				}
			);
		}
	}

	testBank(singleBankParams, opts) {
		let price = opts.price;
		let firstPay = opts.firstPay;
		let duration = opts.duration;

		// check by first pay party
		let firsPayPartyPercent = Math.round(firstPay / (price / 100));

		if (!singleBankParams.firstTo) singleBankParams.firstTo = 100;

		if (firsPayPartyPercent < singleBankParams.firstFrom || firsPayPartyPercent > singleBankParams.firstTo)
			return false;

		// check by pay duration range
		if (!_.inRange(duration, singleBankParams.yearsFrom, singleBankParams.yearsTo + 1)) return false;

		return true;
	}

	prepareDataForRender(data) {
		let opts = data.opts;

		for (let i = data.banks.length - 1; i >= 0; i--) {
			let bnkdt = data.banks[i];

			// prepare banks some datas for rendering
			if (!bnkdt.rateTo) bnkdt.rateTo = bnkdt.rateFrom;
			bnkdt.rateAverage = (bnkdt.rateFrom + bnkdt.rateTo) / 2;

			//calculate monthly payment
			{
				let c = opts.price - opts.firstPay; // сумма кредита
				let p = bnkdt.rateAverage; // годовая процентная ставка
				let n = opts.duration * 12; // срок кредита в месяцах
				let a = 1 + p / 1200; // знаменатель прогрессии
				let k = (Math.pow(a, n) * (a - 1)) / (Math.pow(a, n) - 1); // коэффициент ежемесячного платежа
				let sm = k * c; // ежемесячный платёж
				bnkdt.monthlyPayment = parseInt(sm);
			}
		}

		data.banks = _.sortBy(data.banks, "monthlyPayment");

		return data;
	}

	filterAndRenderBanks() {
		let allBanksParams = this.banksData;
		let selectedOpts = this.$form.serializeObject();

		// convert all selected values to 'INT' type
		selectedOpts = _.mapValues(selectedOpts, param => {
			return parseInt(param);
		});

		let acceptableBanks = [];

		// filter banks
		allBanksParams.forEach(bankParams => {
			if (this.testBank(bankParams, selectedOpts)) {
				acceptableBanks.push(bankParams);
			}
		});

		// render banks
		let dataToRender = this.prepareDataForRender({
			banks: acceptableBanks,
			opts: selectedOpts
		});

		this.banksListBuilder.render(dataToRender);
	}

	getAndRenderFlats() {
		let selectedOpts = this.$form.serializeObject();
		$.ajax({
			url: this.$form.attr("action") || this.$form.data("flats-url"),
			type: this.$form.attr("method"),
			dataType: "json",
			data: {
				price_from: parseInt(selectedOpts.price) - 300000 ,
				price_to: parseInt(selectedOpts.price) + 1000000,
				limit: 20,
				action: "get_typicals"
			}
		})
			.done(response => {
				this.flatListBuilder.render({ flats: response.flats });
			})
			.fail(function() {
				alert("Не удалось получить данные с сервера. Попробуйте позже или обратитесь к админстрации сайта.");
			});
	}
}

class MortgageSummCalc extends MortgageCalc {
	initSliders() {
		{
			let $formSliderBlock = this.$form.find(".MortgageCalc_cell-monthlyPayment");

			var monthlyPaymentSliderInst = new MortgageSlider(
				$formSliderBlock.find(".MortgageSlider"),
				{
					change: this._onFormElementsChange
				},
				{
					$counter: $formSliderBlock.find(".MortgageCalc_sum"),
					customCounterRender: v => {
						return `${v.toLocaleString("ru-RU")}&nbsp;<span class="MortgageCalc_sign">₽</span>`;
					}
				}
			);
		}

		{
			let $formSliderBlock = this.$form.find(".MortgageCalc_cell-firstPayment");

			var firstPaymentSliderInst = new MortgageSlider(
				$formSliderBlock.find(".MortgageSlider"),
				{
					change: this._onFormElementsChange
				},
				{
					$counter: $formSliderBlock.find(".MortgageCalc_sum"),
					customCounterRender: v => {
						return `${v.toLocaleString("ru-RU")}&nbsp;<span class="MortgageCalc_sign">₽</span>`;
					}
				}
			);
		}

		{
			let $formSliderBlock = this.$form.find(".MortgageCalc_cell-duration");

			new MortgageSlider(
				$formSliderBlock.find(".MortgageSlider"),
				{
					change: this._onFormElementsChange
				},
				{
					$counter: $formSliderBlock.find(".MortgageCalc_sum"),
					customCounterRender: v => {
						if (v === 1) {
							return "1 год";
						} else {
							return moment.duration(v, "years").humanize();
						}
					}
				}
			);
		}
	}

	testBank(singleBankParams, opts) {
		let price = opts.firstPayment + opts.monthlyPayment * 12 * opts.duration;
		let firstPay = opts.firstPayment;
		let duration = opts.duration;

		// check by first pay party
		let firsPayPartyPercent = Math.round(firstPay / (price / 100));

		if (!singleBankParams.firstTo) singleBankParams.firstTo = 100;

		if (firsPayPartyPercent < singleBankParams.firstFrom || firsPayPartyPercent > singleBankParams.firstTo)
			return false;

		// check by pay duration range
		if (!_.inRange(duration, singleBankParams.yearsFrom, singleBankParams.yearsTo + 1)) return false;
		return true;
	}

	prepareDataForRender(data) {
		let opts = data.opts;

		for (let i = data.banks.length - 1; i >= 0; i--) {
			let bnkdt = data.banks[i];

			// prepare banks some datas for rendering
			if (!bnkdt.rateTo) bnkdt.rateTo = bnkdt.rateFrom;
			bnkdt.rateAverage = (bnkdt.rateFrom + bnkdt.rateTo) / 2;

			//calculate credit summ
			{
				{
					let m = opts.monthlyPayment; // ежемесячный платёж
					let p = bnkdt.rateAverage; // годовая процентная ставка
					let n = opts.duration * 12; // срок кредита в месяцах
					let a = 1 + p / 1200; // знаменатель прогрессии
					let k = (Math.pow(a, n) * (a - 1)) / (Math.pow(a, n) - 1); // коэффициент ежемесячного платежа
					let c = m / k; // сумма кредита
					bnkdt.creditSumm = parseInt(c);
				}
			}
		}

		data.banks = _.sortBy(data.banks, "creditSumm");

		delete data.opts;
		return data;
	}

	filterAndRenderBanks() {
		let allBanksParams = this.banksData;
		let selectedOpts = this.$form.serializeObject();

		// convert all selected values to 'INT' type
		selectedOpts = _.mapValues(selectedOpts, param => {
			return parseInt(param);
		});

		let acceptableBanks = [];

		// filter banks
		allBanksParams.forEach(bankParams => {
			if (this.testBank(bankParams, selectedOpts)) {
				acceptableBanks.push(bankParams);
			}
		});

		// render banks
		let dataToRender = this.prepareDataForRender({
			banks: acceptableBanks,
			opts: selectedOpts
		});

		this.banksListBuilder.render(dataToRender);

		this.lastActualCrediSumm = dataToRender.banks[0].creditSumm;
	}

	getAndRenderFlats() {
		let selectedOpts = this.$form.serializeObject();
		let firstPayment = parseInt(selectedOpts.firstPayment);
		$.ajax({
			url: this.$form.attr("action") || this.$form.data("flats-url"),
			type: this.$form.attr("method"),
			dataType: "json",
			data: {
				price_from: this.lastActualCrediSumm - 300000 + firstPayment,
				price_to: this.lastActualCrediSumm + 1000000 + firstPayment,
				limit: 20,
				action: "get_typicals"
			}
		})
			.done(response => {
				this.flatListBuilder.render({ flats: response.flats });
			})
			.fail(function() {
				alert("Не удалось получить данные с сервера. Попробуйте позже или обратитесь к админстрации сайта.");
			});
	}
}

class MortgageBanklist {
	constructor(opts) {
		let tpl = opts.tpl.replace(/\s{2,}/g, "");

		this.compiledTemplate = _.template(tpl);
		let $mountEl = $(opts.mountEl);

		$mountEl.on("click", "[data-sort-by]", event => {
			event.preventDefault();
			let $clickedHead = $(event.currentTarget);
			let $table = $clickedHead.parents("table:first");
			let $tbody = $table.find("tbody");

			let rows = $tbody.get(0).querySelectorAll("tr");
			let sortBy = $clickedHead.data("sort-by");

			let sortOrder;

			if ($clickedHead.hasClass("MortgageBanklist_headText-asc")) {
				sortOrder = "desc";
			} else {
				sortOrder = "asc";
			}

			$clickedHead
				.parents("thead:first")
				.find("[data-sort-by]")
				.removeClass("MortgageBanklist_headText-asc MortgageBanklist_headText-desc");

			$clickedHead.addClass("MortgageBanklist_headText-" + sortOrder);

			tinysort(rows, { data: `sort-${sortBy}`, order: sortOrder });
		});

		this.mountEl = $mountEl.get(0);
	}

	render(data) {
		this.mountEl.innerHTML = this.compiledTemplate(data);
	}
}

{
	let $paymentCalcCont = $("#paymentCalcContainer");
	let $summCalcCont = $("#summCalcContainer");

	new Switch(".Switch", {
		onChange: (/*isChecked*/) => {
			$paymentCalcCont.add($summCalcCont).toggle();
		}
	});
}

// START: Payment calculator
let paymentBanklistBuilder = new MortgageBanklist({
	mountEl: "#paymentCalcBanklistContainer",
	tpl: `
/*= include includes/chunks/MortgageBanklist.tpl*/
`
});

let paymentCalcFlatsBuilder = new FlatsList({
	mountEl: "#paymentCalcFlatsContainer"
});

let paymentCalc = new MortgagePaymentCalc(".MortgageCalc-payment", window.banksMortgageConditions, {
	banksListBuilder: paymentBanklistBuilder,
	flatListBuilder: paymentCalcFlatsBuilder
});
// END: Payment calculator

// START: Summ calculator
let summBanklistBuilder = new MortgageBanklist({
	mountEl: "#summCalcBanklistContainer",
	tpl: `
/*= include includes/chunks/MortgageBanklist-summ.tpl*/
`
});

let summCalcFlatsBuilder = new FlatsList({
	mountEl: "#summCalcFlatsContainer"
});

let summCalc = new MortgageSummCalc(".MortgageCalc-summ", window.banksMortgageConditions, {
	banksListBuilder: summBanklistBuilder,
	flatListBuilder: summCalcFlatsBuilder
});
// END: Summ calculator

paymentCalc.update();
summCalc.update();

<div class="ChessSliderNew swiper-container">
    <div class="ChessScheme_track swiper-wrapper">
        <% _.forEach(data, function(section){ %>
        <div class="ChessScheme_sliderItem swiper-slide">
            <div class="Chess">
                <div class="Chess_inner">
                    <div class="Chess_row Chess_row-zero">
                        <div class="Chess_rowNum">1</div>
                    </div>
                    <% _.forEach(section.floors, function(floor){ %>
                    <div class="Chess_row">
                        <% _.forEach(floor.flats, function(flat){%>
                        <a class="Chess_cell <%= flat.status === 'sale' ? '' : ' Chess_cell-sold' %>" href="<%= flat.status === 'sale' ? flat.href : 'javascript:void(0);' %>" data-id="<%=flat.id %>" data-chess-flat-link>
                            <% if (flat.apts) { %>
                            <div class="Chess_cellAppartLine"></div>
                            <% } %>
                            <% if (flat.discount) { %>
                            <div class="Chess_cellActionLine"></div>
                            <% } %>
                            <span class="Chess_cellFlatCount"><%=flat.rooms %></span>
                            <div class="Chess_wrapInfo" style="display:none;">
                                <div class="Chess_flatInfo">
                                    <div class="Chess_flatInfoRow">
                                        <div class="Chess_flatInfoNum">
                                            <% if (flat.apts) { %>
                                            Апартаменты
                                            <% } else { %>
                                            Квартира
                                            <% } %>
                                            &nbsp; #<%=flat.num%>
                                        </div>
                                        <div class="Chess_flatInfoCount"><%=flat.rooms %> комн.</div>
                                    </div>
                                    <div class="Chess_flatInfoRow">
                                        <% if (flat.status === 'sale') {%>
                                        <div class="Chess_flatInfoPrice"><%=flat.price.toLocaleString() %> ₽</div>
                                        <% } else { %>
                                        <div class="Chess_flatInfoSold">Продана</div>
                                        <% }; %>
                                        <div class="Chess_flatInfoSq"><%=flat.area %> м&sup2;</div>
                                    </div>
                                    <% if (flat.discount || flat.apts) { %>
                                    <div class="Chess_flatInfoBonus">
                                        <% if (flat.discount) { %>
                                        <div class="Chess_flatInfoBonusItem Chess_flatInfoBonusItem-Act">
                                            <span>Акция</span>
                                        </div>
                                        <% } %>
                                        <% if (flat.apts) { %>
                                        <div class="Chess_flatInfoBonusItem Chess_flatInfoBonusItem-App">
                                            <span>Апартаменты</span>
                                        </div>
                                        <% } %>
                                    </div>
                                    <% } %>
                                </div>
                            </div>
                        </a>
                        <%}); %>
                        <div class="Chess_rowNum"><%=_.padStart(floor.floorNum, 1, 0)%></div>
                    </div>
                    <%}); %>
                </div>
                <div class="Chess_title">Секция <%= section.sectionNum%></div>
            </div>
        </div>
        <%}); %>
    </div>
</div>
<div class="ChessScheme_sliderCntrl">
    <div class="ChessScheme_compass"><img src="/img/content/compass.svg" alt="alt" title="" /></div>
    <div class="ChessScheme_sliderPager">
        <div class="ChessScheme_sliderPagerInner"></div>
        <button class="ChessScheme_sliderBtnPrev"></button>
        <button class="ChessScheme_sliderBtnNext"></button>
    </div>
    <div class="ChessScheme_bild">
        <img src="https://etalonsilver.ru/upload/shop_1/0/0/3/group_3/shop_property_file_3_334.svg" alt="">
        <span class="ChessScheme_bildTxt">ул. Новоалексеевская</span>
    </div>
</div>

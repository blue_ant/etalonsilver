<table class="MortgageBanklist">
	<thead class="MortgageBanklist_head">
		<tr class="MortgageBanklist_row MortgageBanklist_row-head">
			<th class="MortgageBanklist_cell MortgageBanklist_cell-th">
				<span class="MortgageBanklist_headText">Банк</span>
			</th>
			<th class="MortgageBanklist_cell MortgageBanklist_cell-th MortgageBanklist_cell-bankRate">
				<span class="MortgageBanklist_headText MortgageBanklist_headText-bankRate" data-sort-by="bankrate">Cтавка</span>
			</th>
			<th class="MortgageBanklist_cell MortgageBanklist_cell-th">
				<span class="MortgageBanklist_headText MortgageBanklist_headText-asc" data-sort-by="payment">Платеж в месяц</span>
			</th>
		</tr>
	</thead>
	<tbody class="MortgageBanklist_tbody">
		<% _.forEach(banks, function(bank){ %>
			<tr class="MortgageBanklist_row" data-sort-bankrate="<%= bank.rateFrom %>" data-sort-payment="<%= bank.monthlyPayment %>">
				<td class="MortgageBanklist_cell MortgageBanklist_cell-td">
					<div class="MortgageBanklist_logo" style="background-image:url(<%= bank.logo %>)" title="<%= bank.name %>"></div>
				</td>
				<td class="MortgageBanklist_cell MortgageBanklist_cell-td MortgageBanklist_cell-bankRate">
					<span class="MortgageBanklist_bankRateText <%= bank.mark ? "MortgageBanklist_bankRateText-highlighted" : "" %>">
						от&nbsp;<%= bank.rateFrom %>&nbsp;%
					</span>
					</td>
				<td class="MortgageBanklist_cell MortgageBanklist_cell-td"><%= bank.monthlyPayment.toLocaleString("RU-ru") %> ₽</td>
			</tr>
		<%}); %>
	</tbody>
</table>
<% if (data.length > 0) {%>
    <% _.forEach(data, function(plan) {%>
    <a href="<%= plan.href %>" class="b-flat b-flat--slider">
        <div class="b-flat__image-wrap">
            <img src="<%= plan.pic %>" alt="" class="b-flat__image">
        </div>
        <div class="b-flat__text-wrap">
            <div class="b-flat__column">
                <span class="b-flat__type"><%= plan.rooms %>-к <% if (plan.apts==0) {%>квартира<%}else{%>апарт<%}%></span>
                <% if (plan.cost.from != plan.cost.to) {%>
                <span class="b-flat__cost"><%= plan.cost.from %> — <%= plan.cost.to %> млн ₽</span>
                <%}else{%>
                <span class="b-flat__cost"><%= plan.cost.from %> млн ₽</span>
                <%}; %>
            </div>
            <div class="b-flat__column">
                <span class="b-flat__size"><%= plan.area.from %>  м<sup>2</sup></span>
            </div>
        </div>
    </a>



    <% }); %>
<%}; %>
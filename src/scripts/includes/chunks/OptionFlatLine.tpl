<% if (flats.length > 0) {%>
<% console.log('flats', flats); %>
<% _.forEach(flats, function(flat, index){ %>
    <a href='<%= flat.href %>' >
        <div class="b-flat b-flat--line js-href-click js-sort-item js-option-item"  >
            <div class="b-flat__image-wrap js-popup-link-line">
                <img class="b-flat__image" data-href='<%= flat.href %>' data-pic="<%= flat.pic %>" src="<%= flat.pic %>" alt="" role="presentation">
                <div class="b-popup b-popup--line js-popup-block-line">
                    <span class="b-popup__arrow js-arrow"></span>
                    <a class="b-popup__link-close js-popup-close-line" href="javascript:void(0);" title="закрыть">
                        <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                            width="20px" height="20px" viewBox="0 0 20 20" style="enable-background:new 0 0 20 20;" xml:space="preserve">
                            <polygon points="20,0.7 19.3,0 10,9.3 0.7,0 0,0.7 9.3,10 0,19.3 0.7,20 10,10.7 19.3,20 20,19.3 10.7,10 "/>
                        </svg>
                    </a>
                    <div class="b-popup__image-wrapper">
                        <img class="b-popup__image" src="<%= flat.pic %>" alt="" role="presentation">
                    </div>
                </div>
            </div>
            <div class="b-flat__text-wrap">
                <span class="b-flat__flat-text" data-apts="<%= flat.apts %>" data-type="<%= flat.type %>" > <% if (flat.apts==0) {%>Квартира<%}else{%>Апартаменты<%}%></span>
                <span class="b-flat__type" data-rooms="<%= flat.rooms %>" data-sort-item="type" data-sort-numb="<%= flat.rooms %>"><%= flat.rooms %>-комн.</span>

                <% if (flat.size.from != flat.size.to) {%>
                    <span class="b-flat__size" data-size="<%= flat.size.val %>" data-sort-item="area" data-sort-numb="<%= flat.size.val %>"><%= flat.size.from %> — <%= flat.size.to %> м<sup>2</sup></span>
                <%}else{%>
                    <span class="b-flat__size" data-size="<%= flat.size.val %>" data-sort-item="area" data-sort-numb="<%= flat.size.val %>"><%= flat.size.from %> м<sup>2</sup></span>
                <%}; %>

                <% if (flat.cost.from != flat.cost.to) {%>
                    <span class="b-flat__cost" data-cost="<%= flat.cost.val %>" data-sort-item="price" data-sort-numb="<%= flat.cost.val %>"><%= flat.cost.from %> — <%= flat.cost.to %> млн ₽</span>
                <%}else{%>
                    <span class="b-flat__cost" data-cost="<%= flat.cost.val %>" data-sort-item="price" data-sort-numb="<%= flat.cost.val %>"><%= flat.cost.from %> млн ₽</span>
                <%}; %>
            </div>
            <div class="b-flat__mobileRow">
                <div class="b-flat__procent-wrapper" data-sort-item="finish" data-sort-numb="<%= flat.finish %>">
                    <% if (flat.finish) {%>
                    <span class="b-flat__finish" data-finish="" title="Отделка">
                        <svg class="finish-icon" xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 35 35">
                        <g fill="none" fill-rule="evenodd">
                            <circle class="finish-icon-circle" cx="17.5" cy="17.5" r="17.5" fill="#ededed"/>
                            <g class="finish-icon-img" stroke="#000" stroke-linejoin="round" stroke-width=".75">
                                <path d="M12.25 14.361l11.258 6.5-1 1.732-11.258-6.5zM17.778 15.82L20.83 9.5l2.598 1.5-3.942 5.807z"/>
                                <path stroke-linecap="square" d="M9.034 21.897l3-5.197 9.526 5.5-3 5.197c-2.086-1.508-3.726-2.607-4.92-3.296-1.193-.689-2.729-1.424-4.606-2.204z"/>
                                <path d="M12.781 20.441l-1.5 2.598"/>
                                <path stroke-linecap="round" d="M13.281 19.575l-1.5 2.598"/>
                            </g>
                        </g>
                    </svg>
                    </span>
                    <%}; %>
                </div>
                <div class="b-flat__procent-wrapper" data-sort-item="discount" data-sort-numb="<%= flat.discount %>">
                    <% if (flat.discount) {%>
                    <span class="b-flat__procent" data-discount="" title="Акция месяца">%</span>
                    <%}; %>
                </div>
            </div>

        </div>
    </a>
<%}); %>
<%}else{%>
<div class="b-options-page__empty-wrapper">
    <div class="b-options-page__empty">
    <svg class="b-options-page__image-empty" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27 27">
    <path fill="#B59B61" fill-rule="nonzero" d="M18.213 17.62L27 26.405l-.594.594-8.787-8.787a10.461 10.461 0 0 1-7.12 2.784C4.7 20.997 0 16.297 0 10.499 0 4.7 4.7 0 10.499 0c5.798 0 10.498 4.7 10.498 10.499 0 2.748-1.056 5.249-2.784 7.12zM10.5 20.156a9.659 9.659 0 1 0 0-19.317 9.659 9.659 0 0 0 0 19.317z"></path>
    </svg>
    <p>Планировки по заданным параметрам не найдены, пожалуйста измените параметры поиска.</p>
    </div>
</div>
<%}; %>
<div class="FlatsList">
	<h4 class="FlatsList_title">Предлагаем квартиры в&nbsp;рассчитанной ценовой категории</h4>
	<% if(flats.length) {%>
	<div class="FlatsList_cardsWrap">
		 <% _.forEach(flats, function(flt){ %>
			<a class="FlatCard" href="<%= flt.href %>" title="<%= flt.rooms %>-к квартира <%= flt.title %>">
				<div class="FlatCard_pic" style="background-image:url('<%= flt.pic %>');"></div>
				<div class="FlatCard_desc">
					<div class="FlatCard_detailsRow FlatCard_detailsRow-top">
						<div class="FlatCard_detailVal"><%= flt.rooms %>-к квартира <%= flt.title %></div>
						<div class="FlatCard_detailVal">
							<% if(flt.area.from !== flt.area.to) {%>
								<%= flt.area.from %> - <%= flt.area.to %>
							<% }else { %> 
								<%= flt.area.from %>
							<% }; %> м²
						</div>
					</div>
					<div class="FlatCard_detailsRow FlatCard_detailsRow-bottom">
						<%
							let priceMin = Math.round(flt.price.from / 10000) / 100;
							let priceMax = Math.round(flt.price.to / 10000) / 100;
						%>
						<div class="FlatCard_detailVal">
							<% if(priceMin !== priceMax) {%>
								<%= priceMin %> - <%= priceMax %>
							<% }else { %> 
								<%= priceMin %>
							<% }; %> млн ₽
						</div>
					</div>
				</div>
				<% if(flt.discount) { %>
				<div class="FlatCard_promoIcon"></div>
				<% }; %>
			</a>
		<%}); %>
	</div>
	<% } else { %>
	<div class="FlatsList_textWrap">
		<p>Планировки по заданным параметрам не найдены, пожалуйста измените параметры поиска.</p>
	</div>
	<% }; %>
</div>
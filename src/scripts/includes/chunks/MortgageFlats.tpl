<% _.forEach(flats, function(flat, index){ %>
    <div class="b-flat">
        <% if (flat.discount) {%>
            <span class="b-flat__procent">%</span>
        <%};%>
        <div class="b-flat__image-wrap">
            <img class="b-flat__image" src="<%= flat.pic %>" alt="" role="presentation">
        </div>
        <div class="b-flat__text-wrap">
            <span class="b-flat__type"><%= flat.rooms %>-к квартира Тип 1</span>
            <span class="b-flat__size"><%= flat.area.from %> — <%= flat.area.to %> м<sup>2</sup></span>
        </div>
        <span class="b-flat__cost"><%= flat.price.from %> — <%= flat.price.to %> млн. ₽</span>
    </div>
<%}); %>
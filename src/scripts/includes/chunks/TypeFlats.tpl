<% _.forEach(flats, function(flat, index){ %>
<div class="b-flat b-flat--type js-flat-type"
        data-flat="<%= flat.num %>"
        data-housing="<%= flat.house %>"
        data-entrance="<%= flat.section %>"
        data-area="<%= flat.area %>"
        data-floor="<%= flat.floor %>"
        data-view="<%= flat.view %>"
        data-price="<%= flat.price %>"
        data-procent="<%= flat.discount %>"
        data-finish="<%= flat.finish %>"
        data-room="<%= flat.rooms %>">
    <a class="b-flat__text-wrap" href="<%= flat.href %>" title>
        <span class="b-flat__item-text b-flat__item-text--first" data-val-sort="flat">
            <span class="b-flat__mobile-text">Квартира</span>№<%= flat.num %></span>
        <span class="b-flat__item-text b-flat__item-text--second" data-val-sort="housing">
            <span class="b-flat__mobile-text">Корпус</span><%= flat.house %></span>
        <span class="b-flat__item-text b-flat__item-text--third" data-val-sort="entrance">
            <span class="b-flat__mobile-text">Подъезд</span><%= flat.section %></span>
        <span class="b-flat__item-text b-flat__item-text--four" data-val-sort="area">
            <span class="b-flat__mobile-text">Площадь</span><%= flat.area.toString().split('.').join(',') %> м<sup>2</sup></span>
        <span class="b-flat__item-text b-flat__item-text--five" data-val-sort="floor">
            <span class="b-flat__mobile-text">Этаж</span><%= flat.floor %></span>
        <span class="b-flat__item-text b-flat__item-text--six" data-val-sort="view">
            <span class="b-flat__mobile-text" >Вид из окна</span><%= flat.view %></span>
        <span class="b-flat__item-text b-flat__item-text--seven" data-val-sort="price">
            <span class="b-flat__mobile-text" >Цена</span><%= flat.price.toLocaleString() %> ₽</span>
        <div class="b-flat__procent-wrapper">
            <% if (flat.finish) {%>
            <span class="b-flat__finish" title="Отделка">
                <svg class="finish-icon" xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 35 35">
                        <g fill="none" fill-rule="evenodd">
                            <circle class="finish-icon-circle" cx="17.5" cy="17.5" r="17.5" fill="#ededed"/>
                            <g class="finish-icon-img" stroke="#000" stroke-linejoin="round" stroke-width=".75">
                                <path d="M12.25 14.361l11.258 6.5-1 1.732-11.258-6.5zM17.778 15.82L20.83 9.5l2.598 1.5-3.942 5.807z"/>
                                <path stroke-linecap="square" d="M9.034 21.897l3-5.197 9.526 5.5-3 5.197c-2.086-1.508-3.726-2.607-4.92-3.296-1.193-.689-2.729-1.424-4.606-2.204z"/>
                                <path d="M12.781 20.441l-1.5 2.598"/>
                                <path stroke-linecap="round" d="M13.281 19.575l-1.5 2.598"/>
                            </g>
                        </g>
                    </svg>
            </span>
            <%}; %>
        </div>
        <div class="b-flat__procent-wrapper">
            <% if (flat.discount) {%>
            <span class="b-flat__procent" title="Акция месяца">%</span>
            <%}; %>
        </div>
    </a>
</div>
<%}); %>
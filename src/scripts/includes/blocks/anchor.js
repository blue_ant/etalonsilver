$(function () {
    var $anchorLink = $('.js-anchor-link');
    var $anchorItem = $('.js-anchor-item');
    var $header = $('.js-header');
    var $scroll = $('html, body');

    let getScroll;

    if ($anchorLink.length && $anchorItem.length) {
        $anchorLink.on('click', function () {
            var $link = $(this);
            var attr = $link.attr('data-anchor-target');
            var $item = $anchorItem.not(':not([data-anchor-id=\'' + attr + '\'])');
            var offset = $item[0].getBoundingClientRect();
            var scrollTop = getScroll($scroll);

            if ($header.length) {
                scrollTop += (offset.top - $header.height());
            } else {
                scrollTop += offset.top;
            }

            $scroll.stop().animate({ 
                scrollTop: scrollTop,
            }, 500, 'swing');
        });

        getScroll = function ($scroll) {
            var scroll = 0;
            $scroll.each(function () {

                if (scroll < this.scrollTop) {
                    scroll = this.scrollTop;
                }
            });
            return scroll;
        };
    }
});
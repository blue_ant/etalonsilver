var popupResize = function () {

    // console.log('used');
    var fixTop = function ($parent) {
        var $popup = $('.js-popup-block-line.active');
        var top = $('header').outerHeight() - $popup.offset().top;
        var bottom = $('.js-popup-block-line.active')[0].getBoundingClientRect().bottom - $('.footer')[0].getBoundingClientRect().bottom + 50;
        var bodyScroll =$('body').scrollTop();

        //если попап выходит за границы шапки
        if (top > 0) {
            if (bodyScroll) {// для нормальных браузеров
                $('body').scrollTop(
                    bodyScroll - top
                )
            } else {// для сафари
                $popup[0].scrollIntoView();
            }
        }

        //если попап вышел запределы футера
        if (bottom > 0){
            $popup.css({
                'margin-top': -bottom
            });
            $popup.find('.js-arrow').css({
                'margin-top': bottom
            });
        }

        let popupHeight = $popup.height();
        let offset      = $parent.offset().top;
        let winHeight   = $(window).height();

        if (popupHeight + offset > winHeight) {
            $popup
                .css({
                    marginTop: -1 * ((popupHeight + offset + 70) - winHeight)
                })
                .find('.js-arrow')
                .css({
                    top: ((popupHeight + offset + 70) - winHeight) + 20
                });
        }
    };

    var enter = function () {
        $(this).find(".js-popup-block-line").addClass('active');
        positionElementCenter($(this).find(".js-popup-block-line"));
        fixTop($(this));
    };

    var leave = function () {
        $(".js-popup-block-line").removeClass('active');
        $(".js-popup-block-line").attr('style', '');
    }

    var clicklink = function (event) {
        $(".js-popup-block-line").removeClass('active').css('margin-top','').find().find('.js-arrow').css('margin-top','');
        $(this).find(".js-popup-block-line").addClass('active');
        fixTop();
    }

    var clickClose = function (event) {
        event.stopPropagation();
        $(".js-popup-block-line").removeClass('active').css('margin-top','').find().find('.js-arrow').css('margin-top','');

    }

    if (window.innerWidth >= 768 && window.innerWidth < 1280) {
        $(".js-popup-link-line").off("click", clicklink);
        $(".js-popup-link-line").on("click", clicklink);

        $(".js-popup-close-line").off("click", clickClose);
        $(".js-popup-close-line").on("click", clickClose);

        // $('body').click(function() {
        //     if (!$(this.target).is('.js-popup-block-line')) {
        //       $(".js-popup-block-line").removeClass('active');
        //     }
        // });

    } else if (window.innerWidth >= 1280) {

        $(".js-popup-link-line").off('mouseenter', enter);
        $(".js-popup-link-line").on('mouseenter', enter);

        $(".js-popup-link-line").off('mouseleave', leave);
        $(".js-popup-link-line").on('mouseleave', leave);
    }
};
popupResize();

$('body').on('popup.type', function () {

    if ($(".js-popup-link-line").length && $(".js-popup-block-line").length && $(".js-popup-close-line").length) {
        popupResize();
    }
})

$(window).resize(function () {
    if ($(".js-popup-link-line").length > 0) {
        popupResize();
    }
});

function positionElementCenter($element) {
    var $arrow = $element.find('.js-arrow');
    var arrowOffset = $arrow[0].getBoundingClientRect();
    var $parent = $arrow.parents('.js-popup-link-line');
    var height = $parent.height();

    $element.css('top', 0);

    var offset = $element[0].getBoundingClientRect();
    var arTop = 0
    var top = 0;
    var def = -offset.top
    var winHeight = window.innerHeight;

    if (offset.height > winHeight) {
        top = (offset.height - winHeight) / 2;
        top = -top;
    }

    arTop = top * (-1);

    arTop = arTop + ((height / 2) + (arrowOffset.height / 2));

    $arrow.css('top', arTop);

    $element.css('top', top);
}

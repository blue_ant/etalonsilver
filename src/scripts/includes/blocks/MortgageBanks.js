window.banksMortgageConditions = [
    {
        "id": 729,
        "name": "Металлинвестбанк",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/2/9/item_729/item_706.png",
        "comment": "",
        "rateFrom": 13,
        "rateTo": 13,
        "firstFrom": 0,
        "firstTo": 9.99,
        "yearsFrom": 1,
        "yearsTo": 25,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }, {
        "id": 740,
        "name": "СМП банк",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/4/0/item_740/item_714.png",
        "comment": "*для повторных и зп клиентов. стандартные условия",
        "rateFrom": 14,
        "rateTo": 14,
        "firstFrom": 0,
        "firstTo": 14.99,
        "yearsFrom": 3,
        "yearsTo": 25,
        "isHinted": 1,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }, {
        "id": 711,
        "name": "СНГБ",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/1/1/item_711/item_711.svg",
        "comment": "через 6 мес, при оплате 10% квартиры, возможно снижение ставки до 11-11,5% ",
        "rateFrom": 12,
        "rateTo": 12,
        "firstFrom": 0,
        "firstTo": 9.99,
        "yearsFrom": 1,
        "yearsTo": 30,
        "isHinted": 1,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 710,
        "name": "Совкомбанк",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/1/0/item_710/item_710.png",
        "comment": "",
        "rateFrom": 10.4,
        "rateTo": 10.4,
        "firstFrom": 5,
        "firstTo": 9.99,
        "yearsFrom": 0,
        "yearsTo": 30,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 828,
        "name": "ТКБ",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/8/2/8/item_828/item_720.png",
        "comment": "",
        "rateFrom": 11.4,
        "rateTo": 11.4,
        "firstFrom": 5,
        "firstTo": 14.99,
        "yearsFrom": 1,
        "yearsTo": 25,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 583,
        "name": "Возрождение",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/5/8/3/item_583/item_583.png",
        "comment": "",
        "rateFrom": 11.8,
        "rateTo": 0,
        "firstFrom": 10,
        "firstTo": 19.99,
        "yearsFrom": 1,
        "yearsTo": 30,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 599,
        "name": "ПСБ",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/5/9/9/item_599/item_599.svg",
        "comment": "",
        "rateFrom": 11.8,
        "rateTo": 0,
        "firstFrom": 10,
        "firstTo": 19.99,
        "yearsFrom": 1,
        "yearsTo": 25,
        "isHinted": 1,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 588,
        "name": "Газпромбанк",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/5/8/8/item_588/item_588.svg",
        "comment": "",
        "rateFrom": 9.5,
        "rateTo": 0,
        "firstFrom": 10,
        "firstTo": 100,
        "yearsFrom": 1,
        "yearsTo": 30,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 726,
        "name": "Совкомбанк",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/2/6/item_726/item_710.png",
        "comment": "",
        "rateFrom": 9.9,
        "rateTo": 9.9,
        "firstFrom": 10,
        "firstTo": 14.99,
        "yearsFrom": 0,
        "yearsTo": 30,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 597,
        "name": "Металлинвестбанк",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/5/9/7/item_597/item_597.png",
        "comment": "",
        "rateFrom": 10.75,
        "rateTo": 10.75,
        "firstFrom": 10,
        "firstTo": 19.99,
        "yearsFrom": 1,
        "yearsTo": 25,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 715,
        "name": "Уралсиб",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/1/5/item_715/item_715.png",
        "comment": "",
        "rateFrom": 9.9,
        "rateTo": 9.9,
        "firstFrom": 10,
        "firstTo": 29.99,
        "yearsFrom": 1,
        "yearsTo": 30,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 598,
        "name": "МКБ",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/5/9/8/item_598/item_598.svg",
        "comment": "",
        "rateFrom": 11.25,
        "rateTo": 11.25,
        "firstFrom": 10,
        "firstTo": 19.99,
        "yearsFrom": 1,
        "yearsTo": 20,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 712,
        "name": "СНГБ",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/1/2/item_712/item_712.svg",
        "comment": "для з/п клиентов",
        "rateFrom": 9.3,
        "rateTo": 9.3,
        "firstFrom": 10,
        "firstTo": 14.99,
        "yearsFrom": 1,
        "yearsTo": 30,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 585,
        "name": "ВТБ",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/5/8/5/item_585/item_585.svg",
        "comment": "",
        "rateFrom": 9.7,
        "rateTo": 0,
        "firstFrom": 15,
        "firstTo": 100,
        "yearsFrom": 1,
        "yearsTo": 30,
        "isHinted": 1,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 707,
        "name": "Сбербанк",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/0/7/item_707/item_707.svg",
        "comment": "Ставка 7,5% для з/п, иные фл ставка 8%,  фл без подтверждения дохода 8,5%",
        "rateFrom": 7.4,
        "rateTo": 0,
        "firstFrom": 15,
        "firstTo": 100,
        "yearsFrom": 1,
        "yearsTo": 7,
        "isHinted": 1,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 708,
        "name": "Сбербанк ",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/0/8/item_708/item_708.svg",
        "comment": "Ставка 8% для з/п, иные фл ставка 8,5%,  фл без подтверждения дохода 9%",
        "rateFrom": 7.9,
        "rateTo": 0,
        "firstFrom": 15,
        "firstTo": 100,
        "yearsFrom": 1,
        "yearsTo": 12,
        "isHinted": 1,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 709,
        "name": "Сбербанк",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/0/9/item_709/item_709.svg",
        "comment": "",
        "rateFrom": 9.4,
        "rateTo": 0,
        "firstFrom": 15,
        "firstTo": 100,
        "yearsFrom": 1,
        "yearsTo": 30,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 727,
        "name": "Совкомбанк",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/2/7/item_727/item_710.png",
        "comment": "",
        "rateFrom": 9.4,
        "rateTo": 9.4,
        "firstFrom": 15,
        "firstTo": 100,
        "yearsFrom": 0,
        "yearsTo": 30,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 829,
        "name": "ТКБ",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/8/2/9/item_829/item_720.png",
        "comment": "",
        "rateFrom": 10.4,
        "rateTo": 10.4,
        "firstFrom": 15,
        "firstTo": 34.99,
        "yearsFrom": 1,
        "yearsTo": 25,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 731,
        "name": "Российский капитал",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/3/1/item_731/item_718.svg",
        "comment": "",
        "rateFrom": 9.75,
        "rateTo": 9.75,
        "firstFrom": 15,
        "firstTo": 49.99,
        "yearsFrom": 1,
        "yearsTo": 25,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 739,
        "name": "СМП банк",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/3/9/item_739/item_714.png",
        "comment": "",
        "rateFrom": 10.6,
        "rateTo": 10.6,
        "firstFrom": 15,
        "firstTo": 24.99,
        "yearsFrom": 3,
        "yearsTo": 25,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 713,
        "name": "СНГБ",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/1/3/item_713/item_713.svg",
        "comment": "пв 10% возможен при страховании. Без страхования мин. пв 15%",
        "rateFrom": 9,
        "rateTo": 9,
        "firstFrom": 15,
        "firstTo": 100,
        "yearsFrom": 1,
        "yearsTo": 30,
        "isHinted": 1,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 719,
        "name": "Росевробанк",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/1/9/item_719/item_719.svg",
        "comment": "",
        "rateFrom": 9.45,
        "rateTo": 9.45,
        "firstFrom": 15,
        "firstTo": 100,
        "yearsFrom": 1,
        "yearsTo": 20,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 717,
        "name": "БИН банк",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/1/7/item_717/item_717.png",
        "comment": "",
        "rateFrom": 9.5,
        "rateTo": 0,
        "firstFrom": 20,
        "firstTo": 100,
        "yearsFrom": 1,
        "yearsTo": 30,
        "isHinted": 1,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 725,
        "name": "Сбербанк ",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/2/5/item_725/item_707.svg",
        "comment": "",
        "rateFrom": 6,
        "rateTo": 0,
        "firstFrom": 20,
        "firstTo": 100,
        "yearsFrom": 1,
        "yearsTo": 30,
        "isHinted": 1,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 723,
        "name": "Возрождение",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/2/3/item_723/item_583.png",
        "comment": "",
        "rateFrom": 9.9,
        "rateTo": 0,
        "firstFrom": 20,
        "firstTo": 100,
        "yearsFrom": 1,
        "yearsTo": 30,
        "isHinted": 1,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 724,
        "name": "ПСБ",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/2/4/item_724/item_599.svg",
        "comment": "",
        "rateFrom": 9.5,
        "rateTo": 0,
        "firstFrom": 20,
        "firstTo": 100,
        "yearsFrom": 1,
        "yearsTo": 25,
        "isHinted": 1,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 706,
        "name": "Металлинвестбанк",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/0/6/item_706/item_706.png",
        "comment": "",
        "rateFrom": 10.5,
        "rateTo": 10.5,
        "firstFrom": 20,
        "firstTo": 29.99,
        "yearsFrom": 1,
        "yearsTo": 25,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 718,
        "name": "Российский капитал",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/1/8/item_718/item_718.svg",
        "comment": "",
        "rateFrom": 6,
        "rateTo": 6,
        "firstFrom": 20,
        "firstTo": 100,
        "yearsFrom": 1,
        "yearsTo": 25,
        "isHinted": 1,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 736,
        "name": "ДомРФ",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/3/6/item_736/item_734.png",
        "comment": "",
        "rateFrom": 6,
        "rateTo": 6,
        "firstFrom": 20,
        "firstTo": 100,
        "yearsFrom": 3,
        "yearsTo": 30,
        "isHinted": 1,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 737,
        "name": "ДомРФ",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/3/7/item_737/item_734.png",
        "comment": "",
        "rateFrom": 9.5,
        "rateTo": 9.5,
        "firstFrom": 20,
        "firstTo": 29.99,
        "yearsFrom": 3,
        "yearsTo": 30,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 741,
        "name": "МКБ",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/4/1/item_741/item_598.svg",
        "comment": "",
        "rateFrom": 9.99,
        "rateTo": 9.99,
        "firstFrom": 20,
        "firstTo": 100,
        "yearsFrom": 1,
        "yearsTo": 20,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 591,
        "name": "МИнБ",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/5/9/1/item_591/item_591.png",
        "comment": "",
        "rateFrom": 10,
        "rateTo": 10,
        "firstFrom": 20,
        "firstTo": 49.99,
        "yearsFrom": 5,
        "yearsTo": 30,
        "isHinted": 1,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 589,
        "name": "Глобэкс",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/5/8/9/item_589/item_589.png",
        "comment": "",
        "rateFrom": 12.5,
        "rateTo": 12.5,
        "firstFrom": 20,
        "firstTo": 100,
        "yearsFrom": 3,
        "yearsTo": 30,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 728,
        "name": "Металлинвестбанк",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/2/8/item_728/item_706.png",
        "comment": "",
        "rateFrom": 10,
        "rateTo": 10,
        "firstFrom": 30,
        "firstTo": 100,
        "yearsFrom": 1,
        "yearsTo": 25,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 716,
        "name": "Уралсиб",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/1/6/item_716/item_716.png",
        "comment": "Возможны ипотечные каникулы",
        "rateFrom": 9.4,
        "rateTo": 9.4,
        "firstFrom": 30,
        "firstTo": 100,
        "yearsFrom": 1,
        "yearsTo": 30,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 738,
        "name": "ДомРФ",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/3/8/item_738/item_734.png",
        "comment": "",
        "rateFrom": 6.98,
        "rateTo": 6.98,
        "firstFrom": 30,
        "firstTo": 100,
        "yearsFrom": 3,
        "yearsTo": 30,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 720,
        "name": "ТКБ",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/2/0/item_720/item_720.png",
        "comment": "",
        "rateFrom": 9.9,
        "rateTo": 9.9,
        "firstFrom": 35,
        "firstTo": 49.99,
        "yearsFrom": 1,
        "yearsTo": 25,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 743,
        "name": "Глобэкс",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/4/3/item_743/item_589.png",
        "comment": "",
        "rateFrom": 12,
        "rateTo": 12,
        "firstFrom": 40,
        "firstTo": 100,
        "yearsFrom": 3,
        "yearsTo": 30,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 730,
        "name": "ТКБ",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/3/0/item_730/item_720.png",
        "comment": "",
        "rateFrom": 9.4,
        "rateTo": 9.4,
        "firstFrom": 50,
        "firstTo": 100,
        "yearsFrom": 1,
        "yearsTo": 25,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 732,
        "name": "Российский капитал",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/3/2/item_732/item_718.svg",
        "comment": "",
        "rateFrom": 9.25,
        "rateTo": 9.25,
        "firstFrom": 50,
        "firstTo": 100,
        "yearsFrom": 1,
        "yearsTo": 25,
        "isHinted": 0,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }
    , {
        "id": 742,
        "name": "МИнБ",
        "logo": "http://kvartalkotelniki.ru/upload/information_system_14/7/4/2/item_742/item_591.png",
        "comment": "",
        "rateFrom": 10.5,
        "rateTo": 10.5,
        "firstFrom": 50,
        "firstTo": 100,
        "yearsFrom": 1,
        "yearsTo": 5,
        "isHinted": 1,
        "mark": Math.random() > 0.5,
        "popup": {
            "img": "/img/content/sber-big.png",
            "licence": "Лицензия 1481 от 11.08.2015",
            "content": "<p>«Абсолют банк» предлагает ипотечную программу для физических лиц на&nbsp;приобретение коммерческих помещений в&nbsp;жилом комплексе «Летний сад».</p><p><strong>Первоначальный взнос</strong>&nbsp;— от&nbsp;15% от&nbsp;стоимости приобретаемой недвижимости.</p><p><strong>Срок кредитования</strong>&nbsp;— до&nbsp;30&nbsp;лет.</p><p><strong>Процентная ставка</strong>до/после оформления права собственности&nbsp;— от&nbsp;12,49% годовых в&nbsp;рублях</p><p>Комиссии за&nbsp;рассмотрение заявки на&nbsp;кредит и&nbsp;за&nbsp;выдачу кредита&nbsp;— отсутствуют.</p><p>Мораторий на&nbsp;досрочное погашение кредита (частичное или полное)&nbsp;— отсутствует.</p><p><i>Срок действия акции ограничен.</i></p>"
        }
    }];
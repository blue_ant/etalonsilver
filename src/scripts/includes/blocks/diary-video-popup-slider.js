

$('.js-diary-video-popup').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    appendDots: '.b-diary-popup',
    customPaging : function(slider, i) {
        var thumb = $(slider.$slides[i]).data();
            return '<a class="b-diary-popup__link js-webcam-link" href="javascript:void(0);" title="">' +
                        '<svg xmlns="http://www.w3.org/2000/svg" width="18" height="22" viewBox="0 0 18 22">' +
                            '<path d="M9 18A9 9 0 1 1 9 0a9 9 0 0 1 0 18zm0-2A7 7 0 1 0 9 2a7 7 0 0 0 0 14zm0-4a3 3 0 1 1 0-6 3 3 0 0 1 0 6zm2.904 4.663l1.748-1.149 2.222 4.73c.37.788-.112 1.756-.874 1.756H3c-.724 0-1.208-.88-.914-1.662l1.778-4.73 1.828.96-1.153 3.067H13.3l-1.396-2.972z" />' +
                        '</svg>' +
                        '<span>Камера&nbsp;' + (i+1) + '</span>' +
                    '</a>';
        },
    infinite: false,
    mobileFirst: true,
    responsive: [
        {
            breakpoint: 767,
            settings: {
                arrows: true,
                prevArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--diary prev slick-disabled js-webcam-goto" title="Предыдущий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
                nextArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--diary next js-webcam-goto" title="Следующий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
            }
        }
    ]
});

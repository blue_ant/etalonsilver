// сортировка коммерческой недвижимости

let sorterCommercial = new Sorter(".Sorter");

$('.Commercial_table .Sorter').each(function (index, el) {
    $(this).addClass('Sorter'+index);
    $('.Sorter'+index).next().addClass('Commercial_tableBody'+index);
    let mobileTitleHouseCont = $(this).parent().prev('.Commercial_titleHouseMobile');
    mobileTitleHouseCont.html('Корпус ' + index);
})

$(".Commercial_table .Sorter_lbl").on("click", function(event) {
    let currentTable = $(this).parent().parent().next();
    let currentTableClass = "." + currentTable[0].classList.item(1);
    let isDesc = $(this).hasClass("Sorter_lbl-desc");
    let isAsc = $(this).hasClass("Sorter_lbl-asc");
    let val = $(this).data("val");

    if (isDesc) {
        tinysort($(".Commercial_row"), { selector: currentTableClass + " .Commercial_col-" + val + "", order: "desc", natural: true, ignoreDashes : true });
    } else if (isAsc) {
        tinysort($(".Commercial_row"), { selector: currentTableClass + " .Commercial_col-" + val + "", order: "asc", natural: true, ignoreDashes : true });
    } else {
        tinysort($(".Commercial_row"), { selector: currentTableClass + " .Commercial_col-" + val + "", order: "rand", natural: true, ignoreDashes : true });
    }
});



// сортировка на странице типовой планировки
let sorter = new Sorter(".b-Sorter");


$(".b-filter-flat--type .Sorter_lbl").on("click", function() {
    let isDesc = $(this).hasClass("Sorter_lbl-desc");
    let isAsc = $(this).hasClass("Sorter_lbl-asc");
    let val = $(this).data('type');

    if (isDesc) {
        tinysort($(".js-flat-type"), { selector: "[data-val-sort="+ val +"]", order: "desc", natural: true, ignoreDashes : true  });
    } else if (isAsc) {
        tinysort($(".js-flat-type"), { selector: "[data-val-sort="+ val +"]", order: "asc", natural: true, ignoreDashes : true  });
    } else {
        tinysort($(".js-flat-type"), { selector:  "[data-val-sort="+ val +"]", order: "rand", natural: true, ignoreDashes : true });
    }
});

setTimeout(function () {
    $('.Sorter_lbl[data-type="flat"]').trigger('click');
}, 500)
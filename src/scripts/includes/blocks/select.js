var adaptiveSelect;
    var $select = $(".js-select");

    if ($select.length > 0) {
        adaptiveSelect = function () {

            if ('chosen' in $select) {
                $select.chosen("destroy");
            }

            if (window.innerWidth >= 768) {
                // console.log($select);
                $select.chosen({
                    disable_search: true,
                    disable_search_threshold: 1,
                    width: "100%"
                }).on('change', function (event, itemSelect) {
                    $(this).parents('.js-select-wrapper').addClass('active');
                });

                // $select.css({
                //     display: '',
                //     height: '0px'
                // });

                // $select.trigger('change');
                
                // $('.chosen-results').each(function () {
                //     var ps = new PerfectScrollbar(this, {
                //         // suppressScrollX: true,
                //         wheelPropagation: true,
                //         // для скрола колёсиком
                //         useBothWheelAxes: true,
                //     });
                // });
            }
        };
        adaptiveSelect();

        $(window).resize(function () {
            if ($select.length > 0) {
                adaptiveSelect();
            }
        });
    }
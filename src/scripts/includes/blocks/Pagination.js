$(function () {

    var _eventArrowLeft = function (event, callback) {
        callback.call(this, event, event.pagination);
    }

    var _eventArrowRight = function (event, callback) {
        callback.call(this, event, event.pagination);
    }

    var _eventLink = function (event, callback) {
        callback.call(this, event, event.pagination);
    }

    var Pagination = (function () {
        function Pagination($element) {
            this.nextIndexPage = 2;
            this.currIndexPage = 1;
            this.prevIndexPage = 0;
            this.limit = 0;
            this.offset = (Boolean(LocParams) && Boolean(LocParams.getParams().offset)) ? LocParams.getParams().offset : 0;
            this.total = 0;
            this.$container = $element;

            this.$arrowLeft;
            this.$arrowRight;
            this.$items;
            this.$links;

            this.classes = {
                arrowLeft: 'js-arrow-left',
                arrowRight: 'js-arrow-right',
                items: 'js-item',
                link: 'js-link',
            };

            this.callback = {
                arrowLeft: function () { },
                arrowRight: function () { },
                link: function () { }
            };

            this.tpl = {
                arrowLeft: function (data) {
                    return '<li class="b-pagination__item b-pagination__item--prev js-arrow-left">' +
                        '<a class="b-pagination__page-change b-pagination__page-change--prev" href="javascript:void(0);">' +
                        '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 9 14" >' +
                        '<path stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"></path>' +
                        '</svg >' +
                        '</a>' +
                        '</li>';
                },
                arrowRight: function (data) {
                    return '<li class="b-pagination__item b-pagination__item--next js-arrow-right">' +
                        '<a class="b-pagination__page-change b-pagination__page-change--next" href="javascript:void(0);">' +
                        '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 9 14" >' +
                        '<path stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"></path>' +
                        '</svg>' +
                        '</a>' +
                        '</li>';
                },
                item: function (data) {
                    return '<li class="b-pagination__item js-item">' +
                        '<a class="b-pagination__link js-link ' + data.active + '" href="javascript:void(0);" title="">' + data.index + '</a>' +
                        '</li>';
                }
            }
            this.init();
        }

        return Pagination;
    })();

    Pagination.prototype.create = function () {
        if (this.prevIndexPage == 0) {
            this.prevIndexPage++;
            this.currIndexPage++;
            this.nextIndexPage++;
        }
        this._render();
    };

    Pagination.prototype.init = function () {
        var that = this;

        this.create();

        $(document).on('click', '.' + this.classes.arrowLeft, function (event) {
            event.pagination = that;
            _eventArrowLeft.call(this, event, that.callback.arrowLeft);
        });

        $(document).on('click', '.' + this.classes.arrowRight, function (event) {
            event.pagination = that;
            _eventArrowRight.call(this, event, that.callback.arrowRight);
        });
        $(document).on('click', '.' + this.classes.link, function (event) {
            event.pagination = that;
            _eventLink.call(this, event, that.callback.link)
        });
    };

    Pagination.prototype.setLocked = function (bool) {
        isLocked = bool;
    }

    Pagination.prototype.check = function () {
        this.pages = Math.ceil(this.total/this.limit);

        if (isNaN(this.pages)) return;

        // нормализация данных
        this.currIndexPage = Math.max(1, this.currIndexPage);
        this.currIndexPage = Math.min(this.pages, this.currIndexPage);

        this.prevIndexPage = Math.max(1, this.currIndexPage-1);
        this.nextIndexPage = Math.min(this.pages, this.currIndexPage+1);

        // крайние случаи
        if (this.currIndexPage===1 &&  this.nextIndexPage < this.pages) this.nextIndexPage = this.currIndexPage+2;
        if (this.currIndexPage===this.pages &&  this.prevIndexPage > 1) this.prevIndexPage = this.currIndexPage-2;

    }

    Pagination.prototype._render = function () {

        this.check();
        this.$container.empty();
        this.$container.append(this.tpl.arrowLeft());

        for (var i = this.prevIndexPage; i <= this.nextIndexPage; i++) {
            if (i <= this.pages){
                this.$container.append(this.tpl.item({
                    active: (i == this.currIndexPage) ? 'active' : '',
                    index: i
                }));
            }

        }
        this.$container.append(this.tpl.arrowRight());

        this.$arrowLeft = this.$container.find('.' + this.classes.arrowLeft);
        this.$arrowRight = this.$container.find('.' + this.classes.arrowRight);
        this.$items = this.$container.find('.' + this.classes.item);
        this.$links = this.$container.find('.' + this.classes.link);
    }

    Pagination.prototype.prev = function (isLocked) {

        if (this.currIndexPage > 1){
            this.currIndexPage--;
        }

        this._render();
    };

    Pagination.prototype.next = function (isLocked) {
        this.pages = Math.ceil(this.total/this.limit);

        if (this.currIndexPage < this.pages){
            this.currIndexPage++;
        }

        this._render();
        if (0){
            this.$links.removeClass('active');
            this.$links.filter(function () {
                return parseInt($(this).text()) == that.currIndexPage;
            }).addClass('active');

            this.$arrowRight.addClass('disabled');
        }
    };

    Pagination.prototype.goTo = function (index, isLocked) {
        this.currIndexPage = index;
        this._render();

    };

    window.Pagination = Pagination;
});

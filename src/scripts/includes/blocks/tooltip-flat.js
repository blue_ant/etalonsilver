$(function () {
    let $scheme = $(".js-tooltip-svg");
    let tooltip;
    if ($scheme.length) {
        tooltip = function () {
            $scheme.find("[data-flat-link]").each(function (index, el) {
                let data = window.floorTooltips;
                let dataFlats = data.flats[index];
                let isApts =  data.flats[index].apts;
                let isCommercial =  data.commercial;

                let template = '<div class="Tooltip" role="tooltip">' +
                        '<div class="Tooltip_wrap">' +
                            '<div class="Tooltip_text Tooltip_text-apartment">Квартира #' + dataFlats.num + '</div>' +
                            '<div class="Tooltip_text Tooltip_text-area">' + dataFlats.rooms + ' комн.</div>' +
                        '</div>'+
                        '<div class="Tooltip_wrap">' +
                            '<div class="Tooltip_text Tooltip_text-number">' + dataFlats.price.toLocaleString("ru-RU") + ' ₽</div>' +
                            '<div class="Tooltip_text Tooltip_text-area">' + dataFlats.area + ' м<sup>2</sup></div>' +
                        '</div>'+
                    '</div>';

                if ( isApts ) {
                    template = '<div class="Tooltip" role="tooltip">' +
                            '<div class="Tooltip_wrap">' +
                                '<div class="Tooltip_text Tooltip_text-apartment">Апартаменты #' + dataFlats.num + '</div>' +
                                '<div class="Tooltip_text Tooltip_text-area">' + dataFlats.rooms + ' комн.</div>' +
                            '</div>'+
                            '<div class="Tooltip_wrap">' +
                                '<div class="Tooltip_text Tooltip_text-number">' + dataFlats.price.toLocaleString("ru-RU") + ' ₽</div>' +
                                '<div class="Tooltip_text Tooltip_text-area">' + dataFlats.area + ' м<sup>2</sup></div>' +
                            '</div>'+
                        '</div>';
                }

                if ( isCommercial ){
                    template = '<div class="Tooltip" role="tooltip">'+
                        '<div class="Tooltip_wrap">'+
                            '<div class="Tooltip_text Tooltip_text-apartment">Помещение ' + dataFlats.num + '</div>'+
                            '<div class="Tooltip_text Tooltip_text-area">' + dataFlats.area + 'м<sup>2</sup></div>'+
                        '</div>'+
                        '<div class="Tooltip_wrap">'+
                            '<div class="Tooltip_text Tooltip_text-cost">Цена за м<sup>2</sup></div>'+
                            '<div class="Tooltip_text Tooltip_text-cost">Стоимость</div>'+
                        '</div>'+
                        '<div class="Tooltip_wrap">'+
                            '<div class="Tooltip_text Tooltip_text-number">' + dataFlats.pricePerMeter.toLocaleString("ru-RU")+ '₽</div>'+
                            '<div class="Tooltip_text Tooltip_text-number">' + (dataFlats.price/1000000).toFixed(1)+ ' млн ₽</div>'+
                        '</div>'+
                    '</div>';
                }

                let flatTooltip;

                let tooltipFlats = () => {
                    let winWidth = $(window).width();
                    if (winWidth > 1023) {
                        flatTooltip = null;
                        flatTooltip = tippy(this, {
                            animateFill: false,
                            animation: 'fade',
                            duration: 400,
                            distance: 5,
                            followCursor: true,
                            offset: isCommercial ? 62 : 40,
                            theme: 'tooltip-flat',
                            boundary: 'window',
                            appendTo: document.querySelector('.b-floor-map'),
                            arrow: false,
                            content: template,
                            placement: 'right-end'
                        });
                    } else {
                        let used;
                         flatTooltip = null;

                        $('[data-flat-link]').on('click', function (e) {
                            if( used !== this) used = this, e.preventDefault();
                        });

                        flatTooltip = tippy(this, {
                            animateFill: false,
                            animation: 'fade',
                            duration: 400,
                            distance: 5,
                            followCursor: true,
                            offset: isCommercial ? 62 : 40,
                            theme: 'tooltip-flat',
                            boundary: 'window',
                            appendTo: document.querySelector('.b-floor-map'),
                            arrow: false,
                            content: template,
                            placement: 'right-end',
                            trigger: 'click'
                        });
                    }
                };
                tooltipFlats();


                $(window).on('resize', function () {
                    tooltipFlats();
                });
            });

        };

        tooltip();
    }
})
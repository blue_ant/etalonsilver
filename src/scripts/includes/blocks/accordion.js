$(function () {

    // Переменные
    let $block = $('.js-accordion-block');
    let $blockInHeight = $('.js-accordion-height');
    let animate = 'all 0.3s linear';

    // Проверка
    if ($block.length && $blockInHeight.length) {

        // Событие
        $(document).on('click', '.js-accordion-block', function () {
            if (window.innerWidth >= 320 && window.innerWidth < 768) {
                if (!$(this).hasClass('active')) {
                    let $activeBlock = $('.js-accordion-block.active')
                    hideAccordion($activeBlock);
                    showAccordion($(this));
                } else {
                    hideAccordion($(this));
                }
            }
        });

        // Изменеие размера экрана
        $(window).resize(function () {

            if (window.innerWidth >= 768) {
                $('.js-accordion-block').css('transition', '');
                $('.js-accordion-block.active').removeClass('active');
                $('.js-accordion-height').attr('style', '');
            }
        });
    }

    /**
     * Раскрытие аккордиона
     * @param {jQElement} $show - элемент что нужно показыть
     */
    function showAccordion($show) {
        var targetOffset = ($show.data('target-popup'))*$('.js-accordion-block:not(.active)').outerHeight(true);


        if ($('body').scrollTop()){
            $('html, body').animate({
                scrollTop: $('.b-mortgage-bank__head').offset().top+$('body').scrollTop()+targetOffset
            }, 300);
        }else{//костыль для сафари
            let target = $show.data('target-popup');
            if (target>0){
                setTimeout(function () {
                    $('.js-accordion-block[data-target-popup="'+(target-1)+'"]')[0].scrollIntoView();
                },305)

            }
        }

        $show.css('transition', animate);
        $show.find('.js-accordion-height').stop().slideDown(function () {
            $show.addClass('active');
        });
    }

    /**
     * Скрытие аккордиона
     * @param {jQElement} $hide - элемент что нужно скрыть
     */
    function hideAccordion($hide) {
        $hide.css('transition', animate);
        $hide.find('.js-accordion-height').stop().slideUp(function () {
            $hide.removeClass('active');
        });
    }
});
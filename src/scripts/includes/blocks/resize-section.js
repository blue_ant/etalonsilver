$(function () {

    if ($(window).width() < 1024) {
        if ($('.section').length) {
            $('.section').removeClass("fp-table");
        }
    }

    if ($(window).width() > 1023) {
        if ($('.section').length) {
            $('.section').height($(window).height());
        }
        if ($('.fp-tableCell').length) {
            $('.fp-tableCell').height($(window).height());
        }
    }
});
$(function () {
    var $ajax = $('.js-ajax');
    var $paginCont = $('.js-pagination-cont');
    var pagination = {};

    let setParamsPagination;

    if ($paginCont.length) {
        pagination = new Pagination($paginCont);
        let disabledFirstOrLastPage = () => {
            let lastPage = pagination.limit > (pagination.total - pagination.offset);
            let firstPage = pagination.offset < 1;

            if (lastPage) {
                setTimeout(function (){
                    pagination.$arrowRight.addClass('arrow-disabled');
                },400);
            } else {
                pagination.$arrowRight.removeClass('arrow-disabled');
            }

            if (firstPage) {
                setTimeout(function (){
                    pagination.$arrowLeft.addClass('arrow-disabled');
                },400);

            } else {
                pagination.$arrowLeft.removeClass('arrow-disabled');
            }
        }
        disabledFirstOrLastPage();

        let scrollUp = () => {
            let $flatCount = $('.js-option-flat-count');
            let $scroll = $('body');

            if ($flatCount.length && $flatCount.offset().top < 0) {
                $scroll.animate({
                    'scrollTop': $scroll.scrollTop() + $flatCount.offset().top - $('.Header').innerHeight(),
                    'easing': 'easeOutCirc'
                }, 950);
            } else if ($(tpl.mountEl).length && $(tpl.mountEl).offset().top < 0) {
                $scroll.scrollTop(
                    $scroll.scrollTop() + $(tpl.mountEl).offset().top
                )
            }
        }

        pagination.callback.arrowLeft = function () {
            if (pagination.offset == 0) return;

            pagination.prev(false);

            pagination.offset = pagination.offset - pagination.limit;

            LocParams.setParams('offset', pagination.offset);

            setParamsPagination(pagination);

            disabledFirstOrLastPage();

            scrollUp();

            $('.js-ajax').trigger({
                type: 'ajax',
                isPagin: true,
                pagin: {
                    offset: pagination.offset,
                    limit: pagination.limit,
                    total: pagination.total,
                }
            });
        }

        pagination.callback.arrowRight = function () {

            pagination.next(false);

            pagination.offset = pagination.offset + pagination.limit;

            LocParams.setParams('offset', pagination.offset);

            setParamsPagination(pagination);

            disabledFirstOrLastPage();

            scrollUp();

            $('.js-ajax').trigger({
                type: 'ajax',
                isPagin: true,
                pagin: {
                    offset: pagination.offset,
                    limit: pagination.limit,
                    total: pagination.total,
                }
            });
        }

        pagination.callback.link = function () {
            var numb = parseInt($(this).text());

            pagination.offset = ((numb - 1) * pagination.limit);

            pagination.goTo(numb, false);

            LocParams.setParams('offset', pagination.offset);

            setParamsPagination(pagination);
            scrollUp();

            $('.js-ajax').trigger({
                type: 'ajax',
                isPagin: true,
                pagin: {
                    offset: pagination.offset,
                    limit: pagination.limit,
                    total: pagination.total,
                }
            });
        }

        $('body').on('pagin.first', function () {
            pagination.goTo(0, true);
            pagination.offset = 0;
            LocParams.setParams('offset', 0);
        });

        setParamsPagination = function (pagination) {
            $('#offset').attr('value', pagination.offset);
            $('#limit').attr('value', pagination.limit);
            $('#total').attr('value', pagination.total);
        };
    }

    if ($ajax.length) {
        $ajax.on('flats', function (event) {

            var $activeType = $('.js-link-list-title.active');
            var attr = ($activeType.length)?$activeType.attr('data-type-show'):$ajax.data('type-show');
            var tpl = getTemplate(attr);

            if (attr === 'chess') {
                let filteredFlats = event.json.filteredFlats;
                let filteredFlatsCount = filteredFlats.length;
                $('.Chess_cell').addClass('Chess_cell-notFilter');

                for (let i = 0; i < filteredFlats.length; i++) {
                    $('.Chess_cell[data-id="'+ filteredFlats[i] +'"]').removeClass('Chess_cell-notFilter');
                }

                $('.js-option-flat-count').text(filteredFlatsCount);
            } else {
                var flats = event.json.flats;
                var isLoad = event.isLoad;
                var $flatCount = $('.js-option-flat-count');

                pagination.total = 0;

                for (var i = 0; i < flats.length; ++i) {
                    var flat = flats[i];

                    flat.cost = {
                        val: Calculator.getNormalPrice(flat.price.from) + ',' + Calculator.getNormalPrice(flat.price.to),
                        from: Calculator.getNormalNumb(Calculator.getNormalPrice(flat.price.from)),
                        to: Calculator.getNormalNumb(Calculator.getNormalPrice(flat.price.to)),
                    };
                    flat.size = {
                        val: flat.area.from + ',' + flat.area.to,
                        from: Calculator.getNormalNumb(flat.area.from),
                        to: Calculator.getNormalNumb(flat.area.to)
                    };
                }

                if (typeof mind === 'undefined'){

                }



                tpl.render({
                    flats: flats
                });

                if (Boolean(event.json.offset)){
                    pagination.offset = event.json.offset;
                }
                if (Boolean(event.json) && Boolean(event.json.limit)){
                    pagination.limit = event.json.limit;

                }
                if (Boolean(event.json.total)){
                    pagination.total = event.json.total;
                }

                if (isLoad && Boolean(pagination.goTo)) {
                    let page = 1 + (event.json.offset/event.json.limit);
                    pagination.goTo(page, true);
                }

                setTimeout(function () {
                    if (pagination.total <= pagination.limit){
                        $paginCont.hide();
                    } else {
                        $paginCont.show();
                    }
                });

                if ($('.b-options-page').length && !$('.b-options-page--flat-type').length) {
                    pagination._render();
                }

                $('body').trigger('popup.type');

                if (LocParams.isParam('show')) {
                    var show = LocParams.getParams().show;
                    $('body').trigger({
                        type: 'show.option',
                        show: show
                    });
                }

                if (LocParams.isParam('sort') && LocParams.isParam('sortdir')) {
                    var sort = LocParams.getParams().sort;
                    var sortdir = LocParams.getParams().sortdir;

                    $('body').trigger({
                        type: 'sort.type',
                        sort: {
                            type: sort,
                            attr: sortdir
                        }
                    });
                }

                if ($('.js-option-flat-count').length) {
                    var $countFlat = $('.js-option-flat-count');
                    var countFlat =pagination.total?pagination.total:flats.length
                    $countFlat.text(countFlat);
                    if (countFlat){
                        $('.js-pagination-cont').show();
                    }else{
                        $('.js-pagination-cont').hide();
                    }
                }
            }


        });
    }

    var $img     = $('.js-load-img');
    var $section = $('.js-flat-cont[data-id="section"]');
    var $floor   = $('.js-flat-cont[data-id="floor"]');
    var $svg     = $('.js-tooltip-svg');

    (function($img) {
        let $wrapper = $img.closest('.b-floor-map__image-wrapper');
        let $svg     = $wrapper.find("svg");
        var img      = new Image();

        img.onload = function() {
            $.get(img.src, function(svgxml){
                var attrs = svgxml.documentElement.attributes;
                let svgWidth = attrs.width.value;
                let svgHeight = attrs.height.value;

                let svgWidthValue = svgWidth.replace("px", "");
                let svgHeightValue = svgHeight.replace("px", "");

                $svg.attr({
                    viewBox: `0 0 ${svgWidthValue} ${svgHeightValue}`,
                    width: svgWidthValue,
                    height: svgHeightValue,
                    "enable-background": `0 0 ${svgWidthValue} ${svgHeightValue}`
                }).show();

            }, "xml");
        };


        img.src = $img.attr('src');
    })($img);

    // клиентский фильтр для страницы type-flat.html - отключен, согласно новой трактовке ТЗ
    if($('.js-client-filter').length && false){
        let makeInt = function (string) {
            return parseInt(string.toString().split(' ').join(''))
        };

        let getFilter = function () {
            var data = $('.js-client-filter').serializeArray();
            var result = {};
            $.each(data,function (i,val) {
                var temp = val.name.split('-');
                var name = temp[0];
                var id = temp[1];
                if (typeof result[name]== 'undefined'){
                    result[name] = {};
                }
                if (['min','max'].indexOf(id)==-1){
                    if (typeof result[name]['value']=='undefined') result[name]['value'] = '';
                    result[name]['value']+=val.value+'|';
                }else
                    result[name][id] = val.value;
            });
            return result;

        };
        /**
         * Проверка поля по фильтру
         * @param $this
         * @returns {boolean}
         */
        let checkByFilter = function ($this) {
            var data = $this.data();
            var result = true;

            $.each(getFilter(),function (name,values) {
                var name = name;
                $.each(values,function (id,value) {
                    // случай когда есть больше/меньше
                    if (id == 'min'){
                        result = result && (makeInt(data[name]) >= makeInt(value));
                    }else if (id == 'max'){
                        result = result && (makeInt(data[name]) <= makeInt(value));
                    }else if (Boolean(name) && Boolean(data[name])) {

                        result = result && (value.split('|').indexOf(data[name].toString()) > -1);
                    }else{
                        //console.log(data, name, id,value);
                    }

                });
            });

            return result;
        };

        $('.js-client-filter').on('change', function (event) {

            $('.js-flat-type').each(function () {
                var $this = $(this);
                if (checkByFilter($this)){
                    $this.show()
                }else{
                    $this.hide();
                }
            })
        });

    }

    let toggleClass = function($elem, rm, add){
        if ($elem.hasClass(rm)){
            $elem.removeClass(rm);
            $elem.addClass(add);
        }else{
            $elem.removeClass(add);
            $elem.addClass(rm);
        }
    }

    $('.js-data-sort').on('click',function () {
        var $this = $(this);
        var type = $this.data('type');
        toggleClass($this,'asc','desc');


        $('.js-flat-type').sort(function(a, b) { // сортируем
            var asc = $this.hasClass('asc')?1:-1;
            var desc = $this.hasClass('asc')?-1:1;
            console.log('ab', a, b)

            if ($(b).data(type) > $(a).data(type)) {
                return asc;
            }
            if ($(b).data(type) < $(a).data(type)) {
                return desc;
            }
            // a должно быть равным b
            return 0;
        })
            .appendTo('.js-flat-type-container');// возвращаем в контейнер
    });
    // Страница mortgage-order.html "Оформление заявки", send.app

    if ($('.js-ajax-from').length) {
        var $form = $('.js-ajax-from');
        $form.on('send.app', function (event) {
            var $this = $(this);
            var $send = $(this).find('.js-send-form');
            var $success = $(this).find('.js-success-form');
            var $error = $(this).find('.js-error-form');
            var $textBlockSuccess = $success.find('.js-text');
            var $textBlockError = $form.find('.js-text');
            var dopClass = 'b-mortgage-form--success';
            var data = event.json;
            var text;

            if ($send.length && $success.length) {

                if ('success' in data) {
                    text = data.success;

                    $textBlockSuccess.text(text);
                    $send.stop().fadeOut(function () {
                        $this.addClass(dopClass);
                        $success.fadeIn();
                    });
                } else if ('error' in data) {
                    text = data.error;

                    $textBlockError.text(text);
                    $textBlockError.show();
                }

                $send.find("button")[0].disabled = false;
            }
        });
    }

    if ($('.js-popup-link').length) {
        $('.js-popup-link').on('popup.link', function (event) {
            var $popup = event.$popup;
            setTimeout(function () {
                $popup.find('[data-url]').each(function () {
                    var $item = $(this);
                    var url = $item.attr('data-url');
                    var width = $('.js-diary-video-popup').width() - 5; //5 - паддинги слайдера
                    var height = $('.js-diary-video-popup').height() - 5; //5 - паддинги слайдера

                    $item.find('iframe').attr('src', url + '?' + Math.ceil(width) + 'x' + Math.ceil(height));
                    $item.find('iframe').attr('data-x', width+'x'+height);
                });
            },100);


            if ($popup.find('.js-diary-video-popup').length) {
                $popup.find('.js-diary-video-popup')[0].slick.refresh();
            }
        });

        $('.js-popup-close').on('popup.close', function (event) {
            var $popup = event.$popup;
            $popup.find('[data-url]').each(function () {
                $(this).find('iframe').attr('src', '');
            });
        });

        $(document).on('click', '.js-popup-in',function (event) {
            var $el = $(event.target);
            
            if (!$el.hasClass('js-webcam-goto') && 
                !$el.hasClass('js-webcam-link') &&
                !$el.parents('.js-webcam-goto').length &&
                !$el.parents('.js-webcam-link').length) {
                $('body').trigger('popup.closeAll');
            }
        });
    }
});

$(function() {
    var $slider = $('.js-slider-flat');
    var $dots = $('.js-dots-slider');
    var active = 'b-apart-type__link--active';
    var activeBlock = 'b-apart-scheme__item--active';
    var $slides = $('.b-apart-scheme__item');
    //
    // if ($slider.length && $dots.length) {
    //     $slider.slick({
    //         slidesToShow: 1,
    //         slidesToScroll: 1,
    //         arrows: false,
    //         infinite: false,
    //         draggable: false,
    //         swipe: false,
    //         swipeToSlide: false,
    //         touchMove: false,
    //         draggable: false,
    //         accessibility: false,
    //     });
    //
    //     $dots.on('click', function () {
    //         var index = $(this).attr('data-index');
    //         var $slide = $('[data-slick-index=\'' + index + '\']');
    //
    //         if (!$(this).hasClass(active)) {
    //
    //             $dots.removeClass(active);
    //             $(this).addClass(active);
    //
    //             $('[data-slick-index]').removeClass(activeBlock);
    //             $slide.addClass(activeBlock);
    //
    //             $slider.slick('slickGoTo', index);
    //         }
    //     });
    // }
        $dots.on('click', function () {
            var index = $(this).attr('data-index');
            var $slide = $('[data-tab-index=\'' + index + '\']');

            if (!$(this).hasClass(active)) {

                $dots.removeClass(active);
                $(this).addClass(active);

                $('[data-tab-index]').removeClass(activeBlock);
                $slide.addClass(activeBlock);

            }
        });
});
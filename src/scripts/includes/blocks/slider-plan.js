$(function() {
    var $slider = $('.js-slider-plan');
    var $tabs = $('.js-tabs-plan');
    var active = 'b-flat-card__link--active';
    var activeBlock = 'b-flat-card__image-item--active';
    var $slides = $('.b-flat-card__image-item');

    $tabs.on('click', function () {
        var index = $(this).attr('data-index');
        var $slide = $('[data-tab-index=\'' + index + '\']');

        if (!$(this).hasClass(active)) {

            $tabs.removeClass(active);
            $(this).addClass(active);

            $('[data-tab-index]').removeClass(activeBlock);
            $slide.addClass(activeBlock);

        }
    });
});
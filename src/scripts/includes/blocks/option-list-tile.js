$(function () {
    var $item = $('.js-option-item');
    var $links = $('.js-link-list-title');
    var $container = $('.js-option-container');
    var $head = $('.js-head-rep');

    if ($links.length && $item.length && $container.length) {
        $links.on('click', function () {
            var data;
            var template;
            var $link = $(this);
            var attr = $link.attr('data-type-show');

            if (!$link.hasClass('active')) {
                $links.not(':not(.active)').removeClass('active');
                $link.addClass('active');

                data = getDataElements($('.js-option-item'));
                template = getTemplate(attr);

                template.render({
                    flats: data
                });

                toggleHead(attr);
                $('body').trigger('popup.type');
                LocParams.setParams('show', attr);
            }
        });
    }

    $('body').on('show.option', function (event) {

        if ('show' in event) {
            var data;
            var template;
            var attr = event.show;
            var $link = $('.js-link-list-title[data-type-show=\'' + attr + '\']');

            $links.not(':not(.active)').removeClass('active');
            $link.addClass('active');

            data = getDataElements($('.js-option-item'));
            template = getTemplate(attr);
            template.render({
                flats: data
            });

            toggleHead(attr);
            $('body').trigger('popup.type');
            LocParams.setParams('show', attr);
        }
    });

    function getDataElements($elements) {
        var data = [];

        $elements.each(function () {
            data.push({
                cost: {
                    val: $(this).find('[data-cost]').attr('data-cost'),
                    from: $(this).find('[data-cost]').attr('data-cost').split(',')[0].replace('.', ','),
                    to: $(this).find('[data-cost]').attr('data-cost').split(',')[1].replace('.', ',')
                },
                rooms: $(this).find('[data-rooms]').attr('data-rooms'),
                size: {
                    val: $(this).find('[data-size]').attr('data-size'),
                    from: $(this).find('[data-size]').attr('data-size').split(',')[0].replace('.', ','),
                    to: $(this).find('[data-size]').attr('data-size').split(',')[1].replace('.', ',')
                },
                pic: $(this).find('[data-pic]').attr('data-pic'),
                discount: ($(this).find('[data-discount]').length) ? 1 : 0,
                finish: ($(this).find('[data-finish]').length) ? 1 : 0,
                href: $(this).find('[data-href]').attr('data-href'),
                type: $(this).find('[data-type]').attr('data-type'),
                apts: $(this).find('[data-apts]').attr('data-apts'),
            });
        });

        return data;
    }

    window.getTemplate = function(attr) {

        if (attr == 'tile') {
            $container.removeClass('b-options-page__flat-content--line')
            return new MortgageBanklist({
                mountEl: ".js-option-container",
                tpl: `
                    /*=require ../chunks/OptionFlatTile.tpl*/
`,
            });

        } else if (attr == 'line') {
            $container.addClass('b-options-page__flat-content--line')
            return new MortgageBanklist({
                mountEl: ".js-option-container",
                tpl: `
                    /*=require ../chunks/OptionFlatLine.tpl*/
`,
            });
        }else if (attr == 'type') {
            $container.addClass('b-options-page__flat-content--line')
            return new MortgageBanklist({
                mountEl: ".js-flat-type-container",
                tpl: `
                    /*=require ../chunks/TypeFlats.tpl*/
`,
            });
        } else if (attr == 'chess') {
            return false;
        }
    }

    function toggleHead(attr) {
        $head.hide();
        $head.not(':not([data-type=\'' + attr + '\'])').show();
    }
});

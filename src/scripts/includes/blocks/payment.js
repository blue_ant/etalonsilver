const $document = $(document);

$('[data-mask]').each(function() {
    let $this = $(this);
    let type  = $this.attr('data-mask');
    let mask;

    console.log(type)

    if (type === 'date') {
        $this.datepicker({
            closeText: "Закрыть",
            prevText: "",
            nextText: "",
            currentText: "Сегодня",
            monthNames: [ "Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"],
            monthNamesShort: [ "Янв","Фев","Мар","Апр","Май","Июн","Июл","Авг","Сен","Окт","Ноя","Дек"],
            dayNames: [ "воскресенье","понедельник","вторник","среда","четверг","пятница","суббота"],
            dayNamesShort: [ "вск","пнд","втр","срд","чтв","птн","сбт"],
            dayNamesMin: [ "Вс","Пн","Вт","Ср","Чт","Пт","Сб"],
            weekHeader: "Нед",
            dateFormat: "dd.mm.yy",
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: "",
            showOptions: {
                direction: "up"
            },
            onSelect: function(val, inst) {

            },
            beforeShow: function (input, inst) {
            }
        });

        return;
    }

    $this.inputmask(options);
});

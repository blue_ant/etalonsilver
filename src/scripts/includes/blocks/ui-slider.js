var func = function(div) {
    var slide = $(div).slider({
        range: "min",
        min: parseInt(div.attr("min")),
        max: parseInt(div.attr("max")),
        value: parseInt(div.attr("value")),
    })
};

// func($(".js-slider-ui"));


$(function () {
    var $sliderContainer = $(".js-slider-container");

    if ($sliderContainer.length) {
        setTimeout(function () {
            $sliderContainer.each(function () {
                var $sliderCont = $(this);
                var typeRange = Boolean($sliderCont.attr('data-range')) || "min";
                var isPrice = Boolean($sliderCont.attr('data-price')) || false;
                let isChess = $sliderCont.attr('data-chess-slider');
                var $slider = $sliderCont.find('.js-slider-ui');
                var $sliderId = $('#' + $slider.attr('id'));
                var value = parseInt($slider.attr("value")) || 1;
                var min = parseInt($slider.attr("min").replace(/[\D]/g, ''));
                var max = parseInt($slider.attr("max").replace(/[\D]/g, ''));
                var notflAttr = $sliderCont.attr('data-not-fl') || false; // не последний и не первый
                var range = [{
                    $view: $sliderCont.find('.js-min'),
                    $input: function () {
                        return $sliderCont.find('#' + this.$view.attr('data-target'));
                    }
                },{
                    $view: $sliderCont.find('.js-max'),
                    $input: function () {
                        return $sliderCont.find('#' + this.$view.attr('data-target'));
                    }
                }];

                // if is hess
                let $inputs = $sliderCont.find('input');
                let $inpMin = $inputs.eq(0);
                let $inpMax = $inputs.eq(1);

                let currentMinVal = parseInt($inpMin.val());
                let currentMaxVal = parseInt($inpMax.val());

                var slide = function (ui) {
                    if (typeRange == true) {

                        if (isPrice) {
                            range[ui.handleIndex].$view.text(separate(ui.value));

                        } else {
                            range[ui.handleIndex].$view.text(ui.value);

                        }
                        range[ui.handleIndex].$input().attr('value', ui.value);

                        if (isChess) {
                            range[ui.handleIndex].$input().val(ui.value);
                            range[ui.handleIndex].$input().trigger('change');
                        }

                    } else {
                        range[0].$view.text(ui.value);
                        range[0].$input().attr('value', ui.value)
                    }
                }

                if (typeRange == true) {
                    value = [
                        currentMinVal,
                        currentMaxVal
                    ];
                }

                var setTimeOutIdSlide;

                var options = {
                    range: typeRange,
                    min: min || 0,
                    max: max || 100,
                    value: value,
                    slide: function (event, ui) {
                        slide(ui);

                        clearTimeout(setTimeOutIdSlide);
                        setTimeOutIdSlide = setTimeout(function () {
                            var name = range[ui.handleIndex].$input().attr('name');
                            range[ui.handleIndex].$input().trigger('change');
                            // if (LocParams.isParam(name)) {
                            LocParams.setParams(name, range[ui.handleIndex].$input().attr('value'));
                            // }

                            $('.js-ajax').trigger('ajax');
                        }, 150);
                    },
                    create: function (ui) {
                        var $this = $(this);

                        range[0].$view.text(value[0]);
                        range[1].$view.text(value[1]);

                        if (typeRange == true) {
                            $(this).slider("values", value);

                        } else {
                            $(this).slider("value", value);
                        }


                        if ($('[data-not-fl=\'' + notflAttr + '\']').length) {
                            $('[data-not-fl=\'' + notflAttr + '\']').on('data.done', function (event) {
                                var minVal = value[0];
                                var maxVal = value[1];
                                var iMin = parseInt(range[0].$input().attr('value'));
                                var iMax = parseInt(range[1].$input().attr('value'));
                                var nMin = range[0].$input().attr('name');
                                var nMax = range[1].$input().attr('name');

                                if (event.isTrue) {
                                    minVal++;
                                    maxVal--;


                                    if (minVal != iMin && iMin == min) {
                                        iMin++;
                                    }

                                    if (maxVal != iMax && iMax == max) {
                                        iMax--;
                                    }
                                }

                                if (min == 1) {
                                    range[0].$view.text(iMin);
                                    range[0].$input().attr('value', iMin);
                                    $this.slider("option", "min", minVal);
                                }

                                range[1].$view.text(iMax);
                                range[1].$input().attr('value', iMax);
                                $this.slider("option", "max", maxVal);

                                $this.slider("value", $this.slider("value"));

                                // при нажатии на чекбокс минимальный этаж должен меняться только в том случае, если он == 1
                                if (LocParams.isParam(nMin)) {
                                    LocParams.setParams(nMin, iMin)
                                }

                                if (LocParams.isParam(nMax)) {
                                    LocParams.setParams(nMax, iMax);
                                }
                            });


                        }
                    }
                }
                $sliderId.slider(options);


            }, 300);
        });
    }

    function getNormalNumber(val) {
        return parseInt(String(val).replace(/[\D]/g, ''));
    }

    function separate(str) {
       return String(str).replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1 ');
    }

    window.getNumbRepInput = function(name, value) {
        var $slider = $('[name=\'' + name + '\']').parents('.js-slider-container').find('.js-slider-ui');
        var min = getNormalNumber($slider.attr("min"));
        var max = getNormalNumber($slider.attr("max"));

        if (parseInt(value) < min) {
            return min;
        } else if (parseInt(value) > max) {
            return max;
        }
        return value;
    }

});
$(function() {

    $('.js-map-image').maphilight();

    $(".js-tooltip").tooltip({
        track: true
    });
});
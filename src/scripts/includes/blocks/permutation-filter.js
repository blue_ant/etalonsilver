    var $checkboxElement = $('.js-checkbox-permutation');
    var $checkboxTabletPlace = $('.js-select-tablet');
    var $checkboxMobilePlace = $('.js-default-checkbox');

    if ($checkboxMobilePlace.length > 0) {

        var checkboxAdaptive = function () {
            if (window.innerWidth >= 320 && window.innerWidth < 768) {
                $checkboxMobilePlace.append($checkboxElement);
            } else if (window.innerWidth >= 768 && window.innerWidth < 1024) {
                $checkboxTabletPlace.append($checkboxElement);
            } else if (window.innerWidth <= 1024) {
                $checkboxMobilePlace.append($checkboxElement);
            }
        };
        checkboxAdaptive();

        $(window).resize(function () {
            if ($checkboxMobilePlace.length > 0) {
                checkboxAdaptive();
            }
        });
    }
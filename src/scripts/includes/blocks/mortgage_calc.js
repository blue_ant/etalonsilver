let paymentCalc = new MortgagePaymentCalc(window.banksMortgageConditions, ".MortgageCalc-1");

let paymentCalc2 = new MortgageSummCalc(window.banksMortgageConditions, ".MortgageCalc-2");

// $inputs.on('change', function () {
$('body').on('tabs.start', function (event) {
    // console.log(event.tabs);
    
    // let $this = $(this);
    // let $parent = $this.parent();
    // let index = $parent.index();
    // let type = $parent.data('switch-type');

    // $('.switch-type__item').removeClass(active);
    // $parent.addClass(active);

    // $types.hide();

    // if (index === 0) {
    //     $types.filter('[data-type="payment"]').show();
    // } else {
    //     $types.filter('[data-type="price"]').show();
    // }

    changeBankList(event.tabs.attr);
    toggleButtonMobile(event.tabs.attr);
});

function changeBankList(type) {
    let $header = $('.js-header-colum');

    if (type === 'payment') {
        $header.html('Сумма кредита');
        paymentCalc.filterAndRenderBanks();


    } else if (type === 'price') {
        $header.html('Платеж в месяц');
        paymentCalc2.filterAndRenderBanks();
    }

    // Calculator.banksUpdate();
    // paymentCalc
    // console.dir(Calculator);

    // Calculator.banksUpdate();
}

function toggleButtonMobile(type) {
    var $price = $('.js-button-mobile-price');
    var $month = $('.js-button-mobile-month');

    if (window.innerWidth >= 320 && window.innerWidth < 768) {

        if (type === 'payment') {
            $price.hide();
            $month.show();

        } else if (type === 'price') {
            $price.show();
            $month.hide();
        }

    } else {
        $price.hide();
        $month.hide();
    }
}

$(window).resize(function () {
    toggleButtonMobile($('.js-tab-link.active').attr('data-tab-target'));
});
toggleButtonMobile($('.js-tab-link.active').attr('data-tab-target'));
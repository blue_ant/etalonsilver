$(function () {
    var $ajax = $('.js-ajax-param');
    let ajax;

    if ($ajax.length) {
        ajax = function () {
            var url = $ajax.attr('data-url');
            var type = $ajax.attr('data-type');
            var event = $ajax.attr('data-event') || false;
            var data = LocParams.getParams();

            if (!('section' in data) && !('floor' in data)) {
                data = {
                    section: 1,
                    floor: 1,
                }
                LocParams.setParams(data);
            }

            $.ajax({
                url: url,
                type: type,
                data: data,
                success: function (json) {

                    if (typeof json == 'string') {
                        json = JSON.parse(json);
                    }

                    window.flat = json;
                    window.floorTooltip = json.items;

                    if (event != false) {
                        $ajax.trigger({
                            type: event,
                            json: json,
                        });
                    }
                }
            });
        }

        $ajax.on('ajax.param', function (event) {
            ajax();
        });

        ajax();
    }
});
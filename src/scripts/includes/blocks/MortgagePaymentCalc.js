class MortgagePaymentCalc {
    constructor(banksData, formEl) {
        this.$form = $(formEl);

        if (!this.$form.length) {
            // console.error('MortgageCalc constructor can\'t find given "formEl" in DOM!');
            return;
        }

        this.$form.on("submit", function (event) {
            event.preventDefault();
            return false;
        });

        this.banksListBuilder = new MortgageBanklist({
            mountEl: (this.$form.data('mountEl'))?this.$form.data('mountEl'):"#MortgageBanklistContainer",
            tpl: `
            /*=require ../chunks/MortgageBanklist.tpl*/
`,
        });

        // Render banks for ending
        let onSliderHandlerChange = _.debounce((event) => {
            if (!event.originalEvent) return false;
            this.filterAndRenderBanks();
            Calculator.flatsUpdate();
        }, 100);

        var firstPaymentSliderInst = [];
        var priceSliderInst = [];
        {
            let $formSliderBlock = this.$form.find(".MortgageCalc_option-firstPayment");

            $formSliderBlock.each(function (i) {
                let $formSliderBlock = $(this);
                firstPaymentSliderInst[i] = new MortgageSlider(
                    $formSliderBlock.find(".MortgageSlider"), {
                        change: onSliderHandlerChange,
                    }, {
                        slideEventCallback: ( /*event, ui*/ ) => {
                            let selectedFirstPayment = firstPaymentSliderInst[i].getValue();
                            let selectedPrice = priceSliderInst[i].getValue();

                            if (selectedFirstPayment >= selectedPrice * 0.9) {
                                priceSliderInst[i].setHandlerPosition(Math.round(selectedFirstPayment * 1.15));
                            }
                        },
                        $counter: $formSliderBlock.find(".MortgageCalc_sum"),
                    }
                );
            })



        }

        {
            let $formSliderBlock = this.$form.find(".MortgageCalc_option-price");

            $formSliderBlock.each(function (i) {
                var $formSliderBlock = $(this);
                priceSliderInst[i] = new MortgageSlider(
                    $formSliderBlock.find(".MortgageSlider"), {
                        change: onSliderHandlerChange,
                    }, {
                        slideEventCallback: ( /*event, ui*/ ) => {
                            let selectedFirstPayment = firstPaymentSliderInst[i].getValue();
                            let selectedPrice = priceSliderInst[i].getValue();

                            if (selectedFirstPayment >= selectedPrice * 0.9) {
                                firstPaymentSliderInst[i].setHandlerPosition(Math.round(selectedPrice * 0.9));
                            }
                        },
                        $counter: $formSliderBlock.find(".MortgageCalc_sum"),
                    }
                );
            });


        }

        {
            let $formSliderBlock = this.$form.find(".MortgageCalc_option-duration");

            $formSliderBlock.each(function () {
                var $formSliderBlock = $(this);
                new MortgageSlider(
                    $formSliderBlock.find(".MortgageSlider"), {
                        change: onSliderHandlerChange,
                    }, {
                        $counter: $formSliderBlock.find(".MortgageCalc_sum"),
                        customCounterRender: (v) => {
                            if (v === 1) {
                                return "1 год";
                            } else {
                                return moment.duration(v, "years").locale('ru').humanize();
                            }
                        },
                    }
                );
            })
        }

        this.banksData = banksData;
    }

    testBank(singleBankParams, opts) {
        let price = opts.price;
        let firstPay = opts.firstPay;
        let duration = opts.duration;

        // check by first pay party
        let firsPayPartyPercent = Math.round(firstPay / (price / 100));

        if (!singleBankParams.firstTo) singleBankParams.firstTo = 100;

        if (firsPayPartyPercent < singleBankParams.firstFrom || firsPayPartyPercent > singleBankParams.firstTo)
            return false;

        // check by pay duration range
        if (!_.inRange(duration, singleBankParams.yearsFrom, singleBankParams.yearsTo + 1)) return false;

        return true;
    }

    prepareDataForRender(data) {
        let opts = data.opts;

        data.banks = _.sortBy(data.banks, "rateFrom");

        for (let i = data.banks.length - 1; i >= 0; i--) {
            let bnkdt = data.banks[i];

            // prepare banks some datas for rendering
            if (!bnkdt.rateTo) bnkdt.rateTo = bnkdt.rateFrom;
            bnkdt.rateAverage = (bnkdt.rateFrom + bnkdt.rateTo) / 2;

            //calculate monthly payment
            {
                let c = opts.price - opts.firstPay; // сумма кредита
                let p = bnkdt.rateAverage; // годовая процентная ставка
                let n = opts.duration * 12; // срок кредита в месяцах
                let a = 1 + p / 1200; // знаменатель прогрессии
                let k = (Math.pow(a, n) * (a - 1)) / (Math.pow(a, n) - 1); // коэффициент ежемесячного платежа
                let sm = k * c; // ежемесячный платёж

                bnkdt.monthlyPayment = parseInt(sm).toLocaleString("ru-RU");
            }
        }
        delete data.opts;
        return data;
    }

    filterAndRenderBanks() {
        let allBanksParams = this.banksData;
        let selectedOpts = this.$form.serializeObject();

        // convert all selected values to 'INT' type
        selectedOpts = _.mapValues(selectedOpts, (param) => {
            return parseInt(param);
        });

        let acceptableBanks = [];

        // filter banks
        allBanksParams.forEach((bankParams) => {
            if (this.testBank(bankParams, selectedOpts)) {
                acceptableBanks.push(bankParams);
            }
        });

        // render banks
        let dataToRender = this.prepareDataForRender({
            banks: acceptableBanks,
            opts: selectedOpts,
        });
        this.banksListBuilder.render(dataToRender);
    }
}

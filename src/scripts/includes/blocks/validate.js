$(function () {

    if ($('input.js-phone').length) {

        $('input.js-phone').on('keypress', function (event) {
            
            if (this.value.length && this.selectionStart <= 2) {
                this.selectionStart = this.value.length;
            }

            if (/[\d]/.test(event.originalEvent.key)
                && !event.originalEvent.keyCode == 8
                && !event.originalEvent.keyCode == 9
                && !event.originalEvent.keyCode == 13) {
                return /[\d]/.test(event.originalEvent.key);
            }
        });

        $('input.js-phone').mask('+7 (000) 000-00-00');
    }

    var validSettings = {
        ignore: ':hidden, input.js-ignore',
        highlight: function (element, errorClass, validClass) {
            var $elem = $(element);
            if ($elem.hasClass("js-select")) {
                $elem.siblings('.chosen-container').find('.chosen-single').addClass(errorClass);
            } else {
                $elem.addClass(errorClass);
            }
            requiredInput(this);

        },
        unhighlight: function (element, errorClass, validClass) {
            var $elem = $(element);
            if ($elem.hasClass("js-select")) {
                $elem.siblings('.chosen-container').find('.chosen-single').removeClass(errorClass);
            } else {
                $elem.removeClass(errorClass);
            }
            requiredInput(this);
        },
        errorElement: 'i',
        errorPlacement: function ($error, $element) {
            // $element.parent().append($error)
        },
    };

    // Сообшения
    $.extend($.validator.messages, {
        required: 'Поле обязательно к заполнению',
        email: 'E-mail некорректен',
        tel: 'Телефон некорректен',
        minlength: $.validator.format('Введите не менее {0} символов')
    });

    // E-mail
    $.validator.addMethod('email', function (value, element) {
        var rexp = /^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(?:\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@(?:[a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$/;
        return this.optional(element) || rexp.test(value);
    }, $.validator.messages.email);

    $(document).on('blur', 'input.js-email', function () {
        if ($(this).val().length) {
            $(this).addClass('valid');
        } else {
            $(this).removeClass('valid');
        }
    });

    $(document).on('blur', '.js-elem-valid', function () {
        if ($(this).val().length) {
            $(this).addClass('valid');
        } else {
            $(this).removeClass('valid');
        }
    });

    // Tel
    $.validator.addMethod('tel', function (value, element) {
        var rexp = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{10,13}$/;
        return this.optional(element) || rexp.test(value);
    }, $.validator.messages.tel);

    initSeporate({
        'input.js-only-numb': function () {
            this.value = this.value.replace(/[\D]/g, '');
        },
        'input.js-not-ru': function () {
            this.value = this.value.replace(/[а-яА-ЯёЁ]/g, '');
        },
        'input.js-only-char': function () {
            this.value = this.value.replace(/[^а-яА-ЯёЁ]/g, '');
        }
    });

    // Роли для полей
    $.validator.addClassRules({
        'js-email': {
            email: true
        },
        'js-phone': {
            tel: true
        }
    });

    $('body').on('validate', function () {
        $('.js-validate-form').each(function () {
            this.validator = $(this).validate(validSettings);
        });
    });

    $('body').trigger('validate');

    function initSeporate(obj) {
        for (var key in obj) {
            $(document).on('input', key, obj[key]);
        }
    }

    function requiredInput(valid) {
        var $form = $(valid.currentForm);

        if ($form.find('input.error, textarea.error').length) {
            $form.find('.js-error-note').addClass('error');
        } else {
            $form.find('.js-error-note').removeClass('error');
        }
    }
});
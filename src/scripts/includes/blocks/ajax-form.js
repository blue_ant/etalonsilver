$(function () {
    var $ajaxForm = $('.js-ajax-from');
    let ajax;

    if ($ajaxForm.length) {
        var url;
        var dataEvent;
        var type;
        var data;
        var $form;
        let dataToComagic;
        let typeForm;

        let isParking  = $ajaxForm.hasClass('PaymentForm');

        ajax = function () {
            $ajaxForm.find("button")[0].disabled = true;

            $.ajax({
                url: url,
                type: type,
                data: data,
                success: function(json, textStatus, req) {
                    let dateString = req.getResponseHeader('Date');
                    if (dateString.indexOf('GMT') === -1) {
                        dateString += ' GMT';
                    }
                    let date = new Date(dateString);
                    let dateHourse = date.getUTCHours()+3;
                    let dateMinutes = date.getUTCMinutes();

                    let isWorkingTime = (dateHourse >= 11 && dateHourse <= 20) && (dateMinutes >= 0 && dateMinutes <= 29) ? true : false;

                    dataToComagic = {
                        name: dataToComagic.name,
                        phone: dataToComagic.phone,
                        message: dataToComagic.comment
                    };

                    
                    if ( typeof Comagic !== "undefined") {
                        sendComagicRequest(dataToComagic, typeForm, function () {
                            console.log('Запрос отправлен');
                        }, isWorkingTime);
                    }

                    if (isParking) {
                        window.location.href = json.formUrl;

                        return;
                    }

                    if (typeof json == 'string') {
                        json = JSON.parse(json);
                    }

                    if (dataEvent != false) {
                        $ajaxForm.trigger({
                            type: dataEvent,
                            json: json,
                        });
                    }
                }
            });
        }

        $ajaxForm.on('submit', function (event) {
            event.preventDefault();
            $form = $(this);
            url = $(this).data('url');
            dataEvent = $(this).data('event') || false;
            type = $(this).attr('method');
            data = $(this).serialize();
            dataToComagic = $form.serializeObject();
            typeForm = $(this).data('type');
            data += '&submit=1';

            if ($(this).hasClass('js-validate-form')) {
                if ($(this).valid()) {
                    ajax();

                }
            } else {
                ajax();
            }
        });
    }
});

function sendComagicRequest(req, type, callback, isWorkingTime) {
    // isWorkingTime = (isWorkingTime === undefined || !isWorkingTime) ? false : true;
    if(isWorkingTime) {
        Comagic.sitePhoneCall({phone: req.phone}, callback);
    } else {
        let requestType = ["callback", "flat", "commerce", "parking", "tradein", "storeroom", "mortgage"];
        if(type === undefined || requestType.indexOf(type) < 0) {
            type = requestType[0];
        }
        if(req.message === undefined) {
            req.message = "";
        }
        req.message = "[" + type + "] " + req.message;
        Comagic.addOfflineRequest(req, callback);
    }
}

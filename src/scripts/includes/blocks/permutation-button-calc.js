    var $buttonPriceElement = $('.js-button-price');
    var $buttonPriceDesktopPlace = $('.js-button-desktop-price');
    var $buttonPriceMobilePlace = $('.js-button-mobile-price');

    var $buttonMonthElement = $('.js-button-month');
    var $buttonMonthDesktopPlace = $('.js-button-desktop-month');
    var $buttonMonthMobilePlace = $('.js-button-mobile-month');

    if ($buttonPriceMobilePlace.length > 0) {

        var buttonPriceAdaptive = function () {
            if (window.innerWidth >= 320 && window.innerWidth < 768) {
                $buttonPriceMobilePlace.append($buttonPriceElement);
            } else if (window.innerWidth >= 768) {
                $buttonPriceDesktopPlace.append($buttonPriceElement);
            }
        };
        buttonPriceAdaptive();

        $(window).resize(function () {
            if ($buttonPriceMobilePlace.length > 0) {
                buttonPriceAdaptive();
            }
        });
    }
    if ($buttonMonthMobilePlace.length > 0) {

        var buttonMonthAdaptive = function () {
            if (window.innerWidth >= 320 && window.innerWidth < 768) {
                $buttonMonthMobilePlace.append($buttonMonthElement);
            } else if (window.innerWidth >= 768) {
                $buttonMonthDesktopPlace.append($buttonMonthElement);
            }
        };
        buttonMonthAdaptive();

        $(window).resize(function () {
            if ($buttonMonthMobilePlace.length > 0) {
                buttonMonthAdaptive();
            }
        });
    }
var makeDotsLikeInsta = function (currentSlide, nextSlide) {
    if (typeof currentSlide=='undefined') currentSlide = 1;
    if (typeof nextSlide=='undefined') nextSlide = currentSlide;
    
    /*
        получаем номер слайда до и после
        определяем направление (пример вправо)
        normal - обычный ширина 4
        small - маленький n+1 или n-5
        smallest  - очень маленький n+2 или n-6
        hidden - >=n+3 или <=n-7
        :nth-child(n+3):nth-child(-n+8) с 3 по 8.
     */
    var direction = nextSlide - currentSlide;
    var normalW = 2;
    var makeString = function (num) {
        if (num >= 0) return '+' + num;
        return num.toString();
    };

    var baseXPath = '.js-tab-container.active .b-dots [role="presentation"]';

    var curentChild = nextSlide + 1;
    var tempSelector = baseXPath + ':nth-child(' + (curentChild) + ')';
    var $curentChild = $(tempSelector);

    setTimeout(function () {
        if ($curentChild.hasClass(smallClass) || $curentChild.hasClass(smallerClass) || $curentChild.hasClass(hiddenClass) || direction==0){

            if (direction==0){
                $(baseXPath).hide().removeClass(smallClass+' '+smallerClass+' '+hiddenClass);
            }

            //вправо
            if (direction>0){

                //normal
                tempSelector = baseXPath+':nth-child(n'+makeString(curentChild-normalW)+'):nth-child(-n'+makeString(curentChild)+')';
                $(tempSelector).removeClass(smallClass+' '+smallerClass+' '+hiddenClass);

                //small
                tempSelector = baseXPath+':nth-child('+(curentChild-normalW-1)+'),'+baseXPath+':nth-child('+(curentChild+1)+')';
                $(tempSelector).removeClass(smallerClass+' '+hiddenClass).addClass(smallClass);

                //smallest
                tempSelector =  baseXPath+':nth-child('+(curentChild-normalW-2)+'),'+baseXPath+':nth-child('+(curentChild+2)+')';
                $(tempSelector).removeClass(smallClass+' '+hiddenClass).addClass(smallerClass);

                //hidden
                tempSelector = baseXPath+':nth-child(-n'+makeString(curentChild-normalW-3)+'),'+baseXPath+':nth-child(n'+makeString(curentChild+3)+')';
                $(tempSelector).removeClass(smallClass+' '+smallerClass).addClass(hiddenClass);
                //влево
            }else{
                //normal
                tempSelector = baseXPath+':nth-child(n'+makeString(curentChild)+'):nth-child(-n'+makeString(curentChild+normalW)+')';
                $(tempSelector).removeClass(smallClass+' '+smallerClass+' '+hiddenClass);
                //small
                tempSelector = baseXPath+':nth-child('+(curentChild-1)+'),'+baseXPath+':nth-child('+(curentChild+normalW+1)+')';
                $(tempSelector).removeClass(smallerClass+' '+hiddenClass).addClass(smallClass);

                //smallest
                tempSelector =  baseXPath+':nth-child('+(curentChild-2)+'),'+baseXPath+':nth-child('+(curentChild+normalW+2)+')';
                $(tempSelector).removeClass(smallClass+' '+hiddenClass).addClass(smallerClass);

                //hidden
                tempSelector = baseXPath+':nth-child(-n'+makeString(curentChild-3)+'),'+baseXPath+':nth-child(n'+makeString(curentChild+normalW+3)+')';
                $(tempSelector).removeClass(smallClass+' '+smallerClass).addClass(hiddenClass);
            }

            setTimeout(function () {
                $(baseXPath).show();
            }, 300);
        }
    });
}

$('.js-flat-slider').on('init', function(){
    makeDotsLikeInsta(0);
});

$('.js-flat-slider').on('refresh', function(){
    var refreshSlider = setInterval(function () {
        var currentSlide = $('.js-tab-container.active .js-flat-slider.slick-initialized').slick('slickCurrentSlide');
        if (typeof currentSlide == 'number'){
            clearInterval(refreshSlider);
            makeDotsLikeInsta(currentSlide);
        }
    },100);
});


$('.js-flat-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    dotsClass: 'b-dots',
    touchThreshold: 50,
    infinite: false,
    mobileFirst: true,
    responsive: [
        // {
        //     breakpoint: 767,
        //     settings: {
        //         slidesToShow: 3,
        //         slidesToScroll: 1,
        //         // variableWidth: true,
        //     }
        // },{
        //     breakpoint: 1279,
        //     settings: {
        //         slidesToShow: 3,
        //         slidesToScroll: 1,
        //         // variableWidth: true,
        //         arrows: true,
        //         prevArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--type-flat prev slick-disabled" title="df g Предыдущий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
        //         nextArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--type-flat next" title="Следующий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
        //     }
        // }
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                variableWidth: true,
            }
        },{
            breakpoint: 1279,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                // variableWidth: true,
                arrows: true,
                prevArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--type-flat prev slick-disabled" title="df g Предыдущий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
                nextArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--type-flat next" title="Следующий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
            }
        },{
            breakpoint: 1589,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 1,
                // variableWidth: true,
                arrows: true,
                prevArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--type-flat prev slick-disabled" title="df g Предыдущий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
                nextArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--type-flat next" title="Следующий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
            }
        },{
            breakpoint: 1919,
            settings: {
                slidesToShow: 6,
                slidesToScroll: 1,
                // variableWidth: true,
                arrows: true,
                prevArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--type-flat prev slick-disabled" title="df g Предыдущий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
                nextArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--type-flat next" title="Следующий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
            }
        }
    ]
}).on('beforeChange', function(event, slick, currentSlide, nextSlide){
    makeDotsLikeInsta(currentSlide, nextSlide);
});

var smallClass = 'small';
var smallerClass = 'smallest';
var hiddenClass = 'hidden';

$('.js-flat-slider-small').on('init', function(){
    makeDotsLikeInsta(0);
});

$('.js-flat-slider-small').on('refresh', function(){
    setTimeout(function () {
        var currentSlide = $('.js-tab-container.active .js-flat-slider-small.slick-initialized').slick('slickCurrentSlide');
        makeDotsLikeInsta(currentSlide);
    },300);
});

$('.js-flat-slider-small').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    dotsClass: 'b-dots',
    infinite: false,
    touchThreshold: 50,
    mobileFirst: true,
    responsive: [
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                variableWidth: true,
            }
        },{
            breakpoint: 1279,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                // variableWidth: true,
                arrows: true,
                prevArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--type-flat prev slick-disabled" title="df g Предыдущий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
                nextArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--type-flat next" title="Следующий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
            }
        },{
            breakpoint: 1589,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
                // variableWidth: true,
                arrows: true,
                prevArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--type-flat prev slick-disabled" title="df g Предыдущий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
                nextArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--type-flat next" title="Следующий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
            }
        },{
            breakpoint: 1919,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 1,
                // variableWidth: true,
                arrows: true,
                prevArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--type-flat prev slick-disabled" title="df g Предыдущий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
                nextArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--type-flat next" title="Следующий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
            }
        }
    ]
}).on('beforeChange', function(event, slick, currentSlide, nextSlide){
    makeDotsLikeInsta(currentSlide, nextSlide);
});

if ($('.js-flat-slider-similar-plans').length) {
    let pathToFlats = $('.js-flat-slider-similar-plans').data('path');
    $.ajax({
        url: pathToFlats,
        dataType: "json",
        method: "GET",
        async: true,
    })
        .done((jsonResponse) => {
            console.log('jsonResponse', jsonResponse);
            let similarPlansSliderBuilder = new MortgageBanklist({
                mountEl: ".js-flat-slider-similar-plans",
                tpl: `
            /*=require ../chunks/SimilarPlansSlider.tpl*/
`,
            });
            similarPlansSliderBuilder.render({
                data: jsonResponse.flats,
            });

            $('.js-flat-slider-similar-plans').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                dots: true,
                dotsClass: 'b-dots',
                infinite: false,
                touchThreshold: 50,
                mobileFirst: true,
                responsive: [
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            variableWidth: true,
                        }
                    },{
                        breakpoint: 1279,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            // variableWidth: true,
                            arrows: true,
                            prevArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--type-flat prev slick-disabled" title="df g Предыдущий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
                            nextArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--type-flat next" title="Следующий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
                        }
                    },{
                        breakpoint: 1589,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 1,
                            // variableWidth: true,
                            arrows: true,
                            prevArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--type-flat prev slick-disabled" title="df g Предыдущий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
                            nextArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--type-flat next" title="Следующий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
                        }
                    },{
                        breakpoint: 1919,
                        settings: {
                            slidesToShow: 5,
                            slidesToScroll: 1,
                            // variableWidth: true,
                            arrows: true,
                            prevArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--type-flat prev slick-disabled" title="df g Предыдущий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
                            nextArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--type-flat next" title="Следующий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
                        }
                    }
                ]
            }).on('beforeChange', function(event, slick, currentSlide, nextSlide){
                makeDotsLikeInsta(currentSlide, nextSlide);
            });
        })
        .fail(() => {
            alert("Не удалось получить данные с сервера!\nПопробуйте позже.");
        });

}
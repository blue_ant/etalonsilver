// фикс бага с отображением медиа
// по условиям задачи в каждом месяце должна быть фотка.
// если это не так - что-то сломается
$( document ).ready(function() {
    $('.js-date-slider').slick('refresh')
});

/**
 * Получаем дату со смещение
 */
var makeStepDate = function (date,step) {
    var month = parseInt(date[0]);
    var year = parseInt(date[1]);
    if (month+step <= 0){
        month = (month)+step+12;
        year--;
    }else if (month+step > 12){
        month = (step+month)-12;
        year++;
    }else{
        month = (step+month);
    }

    if (month<10) month = "0"+month;
    return [month,year];
}

/**
 * Подгрузка картинок по текущей и соседней дат
 * @param date
 */
var lazyLoad = function (date) {
    var last = makeStepDate(date,-1).join('-');
    var next = makeStepDate(date,1).join('-');
    var now = date.join('-');

    $('.b-media-slider__item[data-date="'+last+'"],.b-media-slider__item[data-date="'+now+'"],.b-media-slider__item[data-date="'+next+'"]').each(function () {
        var $img = $(this).find('img');

        if (Boolean($img.data('src'))){
            $img.attr('src',$img.data('src'))
        }
    })

}

window.stopSlide = false;

var getInitSlide =function($e){
    var init = false;
    var date = $e.data('init');
    $e.find('[data-date]').each(function (i) {
        if ($(this).data('date')==date && !init){
            init = i;
        }
    })
    if (Boolean(date)==false || init==false) return 12;
    return init;
}

$('.js-date-slider').slick({
    prevArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--prev prev slick-disabled" title="Предыдущий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
    nextArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--next next" title="Следующий слайд"><svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path fill="none" fill-rule="evenodd" stroke="#000" stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: getInitSlide($('.js-date-slider')),
    rows: 1,
    variableWidth: true,
    centerMode: true,
    focusOnSelect: true,
    waitForAnimate:false,
    asNavFor: $('.js-slider-content').length?'.js-slider-content':null,
}).on('beforeChange', function(event, slick, currentSlide, nextSlide){
    // деактивация стрелочек
    var $nextSlider = $('.js-date-slider [data-slick-index="'+nextSlide+'"]');
    var dateInfo = $nextSlider.find('[data-date]').data('date');

    if (Boolean(dateInfo)){
        let date = dateInfo.split('-');

        $('.js-date-year').html(
            date[1]
        );
        //подгрузка картинок
        lazyLoad(date);
    }

    if ($('.js-date-slider [data-slick-index="'+(nextSlide+1)+'"]').hasClass('disabled')){
        $('.js-date-slider .b-slider-arrow--next').addClass('slick-disabled')
    }

}).on('afterChange', function(slick, currentSlide){

    var mediaDate = $('.js-date-slider .slick-current .b-slider-date__mounth').data('date');
    var $all = $('.js-slider-media [data-date="'+mediaDate+'"]') ;
    var count = 0;
    //связь слайдеров
    if (window.stopSlide){
        window.stopSlide = false;
    }else{
        window.stopSlide = true;
        var id = $('.js-slider-media [data-date="'+mediaDate+'"]:first').data('slick-index');
        if (typeof id!=='undefined') $('.js-slider-media').slick('slickGoTo',id);
    }

    // деактивация стрелочек
    if ($('.js-date-slider [data-slick-index="'+(currentSlide.currentSlide+1)+'"]').hasClass('disabled')){
        $('.js-date-slider .b-slider-arrow--next').addClass('slick-disabled')
    }
    if ($('.js-date-slider [data-slick-index="'+(currentSlide.currentSlide-1)+'"]').hasClass('disabled')){
        $('.js-date-slider .b-slider-arrow--prev').addClass('slick-disabled')
    }

    // счетчик внизу слайдера
    $all.each(function () {
        count++;
        if ($(this).hasClass('slick-current')) return false;
    });

    $('.js-slider-num').html(count);
    $('.js-slider-all').html($all.length);

}).on('mousedown touchstart',function (e) {
    var started = (Boolean(e.originalEvent.changedTouches))?e.originalEvent.changedTouches[0].pageX : e.originalEvent.clientX;
    $(this).data('clientX',started);

});
   $('body').on('mouseup touchend',function (e, slick, direction) {
    // магия перетаскивания временной шкалы
    var ended = (Boolean(e.originalEvent.changedTouches))?e.originalEvent.changedTouches[0].pageX : e.originalEvent.clientX;
    var moving = $('.js-date-slider').data('clientX') - ended;
    var target = $(window).width()/2;
    var diff;
    var index = false;

    if (Math.abs(moving) > 10) {
        $('.js-date-slider').data('clientX',e.originalEvent.clientX);

        $('.js-date-slider [data-slick-index]:not(.disabled)').each(function () {
            var $slide = $(this);
            var offset = $(this).offset().left+$(this).width()/2;
            if (typeof diff === 'undefined' || diff > Math.abs(target - offset)){
                diff = Math.abs(target - offset);
                index = $slide.data('slick-index');
            }
        });
        setTimeout(function () {
            $('.js-date-slider').slick('slickGoTo',index);
            //$('.js-slider-content').slick('slickGoTo',index);
        });
    }

});

setTimeout(function () {
    $('.js-date-slider .b-slider-arrow--prev').addClass('slick-disabled')
});

$('.js-slider-content').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: false,
    infinite: false,
    waitForAnimate:false,
    asNavFor: '.js-date-slider'
});


$('.js-slider-media').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: false,
    variableWidth: true,
    mobileFirst: true,
    centerMode: true,
    arrows: false,
    focusOnSelect: true,
    centerPadding: '0px',
    responsive: [
        {
          breakpoint: 767,
          settings: {
            arrows: true,
            prevArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--prev-content prev" title="Предыдущий слайд"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 9 14"><path stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
            nextArrow: '<a href="javascript:void(0);" class="b-slider-arrow b-slider-arrow--next-content next" title="Следующий слайд"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 9 14"><path stroke-linecap="square" stroke-linejoin="round" stroke-width="1.12" d="M1.512 6.889l6.111 6.15-6.11-6.15 6.11-6.15-6.11 6.15zm0 0l-.025-.025.025.025-.025.025.025-.025z"/></svg></a>',
          }
        }
    ],
    onInit: function () {
        $('.slick-center').prev().addClass('prev');
        $('.slick-center').next().addClass('next');
    },

    onBeforeChange: function () {
        $('.slick-slide').removeClass('prev next');
    },

    onAfterChange: function () {
        $('.slick-center').prev().addClass('prev');
        $('.slick-center').next().addClass('next');
    }
}).on('beforeChange', function(e,slick, currentSlide, nextSlide){

    var $media = $(e.target);
    $media.find('[data-slick-index]').removeClass('next prev');
    $media.find('[data-slick-index="'+(nextSlide+1)+'"]').addClass('next');
    $media.find('[data-slick-index="'+(nextSlide-1)+'"]').addClass('prev');

}).on('afterChange',function (e, slick, currentSlide) {
    var mediaDate = $('.js-slider-media .slick-current').data('date');
    var $all = $('.js-slider-media [data-date="'+mediaDate+'"]') ;
    var count = 1;

    //связь слайдеров
    if (window.stopSlide){
        window.stopSlide = false;
    }else{
        window.stopSlide = true;
        var id = $('.js-date-slider [data-date="'+mediaDate+'"]').parents('.slick-slide').data('slick-index');
        if (typeof id!=='undefined') $('.js-date-slider').slick('slickGoTo',id);
    }

    // счетчик внизу слайдера
    $all.each(function () {
        if ($(this).hasClass('slick-current')) return false;
        count++;
    });
    $('.js-slider-num').html(count);
    $('.js-slider-all').html($all.length);

});


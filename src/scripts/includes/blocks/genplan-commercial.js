$(function () {
    var $n = $('.js-n-com'); // Картинки

    /**
     * Получаем координаты
     * @return {Object} получаем объект координат
     */
    function getCoors() {
        var x, y; // Координаты
        var height;
        var width;

        if (window.innerWidth >= 320 && window.innerWidth < 768) {
            x = -84;
            y = 132;
            width = '88%';//getPixel('width', 108) - 15;
            height = '88%';// нужно указание процентов чтобы работало на странице /commercial-section.html

        } else if (window.innerWidth >= 768 && window.innerWidth < 1280) {
            x = 0;
            y = 0;
            width = '96%';//getPixel('width', 108) - 15;
            height = '96%';

        } else if (window.innerWidth >= 1280 && window.innerWidth < 1920) {
            x = 0;
            y = 0;
            width = '96%';//getPixel('width', 108) - 15;
            height = '96%';

        } else if (window.innerWidth >= 1920) {
            // x = 0;
            // y = 0;
            // width = getPixel('width', 94) - 25;
            x = 0;
            y = 0;
            width = '100%';//getPixel('width', 108) - 15;
            height = '100%';
        }

        // height = width * 1600 / 2560;

        return {
            x: x,
            y: y,
            height: height,
            width: width
        };
    }

    /**
     * Переводит из процентов в пиксели ширину
     * @param {String} type - тип размера width или height
     * @param {Number} procent - тут целочисленный процент без %
     * @return {Number | Float} плучаем ширину
     */
    function getPixel(type, procent) {
        var maxProcent = 100;
        var style = $n[0].getBoundingClientRect();
        var out = 0;

        return Math.round(
            $(window).width()*procent/100
        );

        if (procent > maxProcent) {
            out = procent - maxProcent;

            return ((style[type] * out) / maxProcent) + style[type];
        } else {
            return (style[type] * procent) / maxProcent;
        }
    }

    /**
     * Устанавливаем координаты для картинок
     */
    function setCoorOnImg() {

        if ($n.length) {
            var coors = getCoors(); // Тут будем хронить координаты
            $n.attr(coors);
        }
        
    }
    setCoorOnImg();

    $(window).resize(function () {
        setCoorOnImg();
    });
});
$(function () {
    var $ajax = $('.js-ajax');

    let ajax;

    if ($ajax.length) {
        var isLoad = $ajax.attr('data-load') || false;

        ajax = function (load, pagin) {
            pagin = pagin || false
            var url = $ajax.attr('data-url');
            var event = $ajax.attr('data-event') || false;
            var type = $ajax.attr('data-type') || 'get';
            var data = serialize($ajax);

            data = setParamsByGetParams(data);
            setParamsFilter($ajax, data);
            data = getFilterNotEmpty(data);

            if (pagin) {
                for (var key in pagin) {
                    data[key] = pagin[key];
                }
            }
            // $('.b-options-page__flat-content').animate({opacity: 0.4}, 300);
            $('.b-options-page__flat-content').addClass('loading');
            let $progress = $(".b-options-page__progress");
            let $chessCont = $('.Chess');
            if ($chessCont.length) {
                $chessCont.animate({ opacity: 0.5 }, 600);
            }
            $progress.show().animate({ width: "33%", opacity: 1 }, 600);

            $.ajax({
                url: url,
                type: type,
                data: data,
                xhr: () => {
                    let xhr = new window.XMLHttpRequest();

                    xhr.addEventListener("progress", (event) => {
                        if (event.lengthComputable) {
                            let percent = Math.ceil((100 * event.loaded) / event.total);
                            $progress.stop(true, false).animate({ width: percent + "%" }, 400);
                            if (percent === 100) {
                                $progress.animate({ opacity: 0 }, 350, () => {
                                    $progress.removeAttr("style");
                                });
                                $chessCont.animate({ opacity: 1 }, 600);
                            }
                        }
                    });

                    return xhr;
                },
                success: function (json) {

                    if (typeof json == 'string') {
                        json = JSON.parse(json);
                    }

                    if (event != false) {
                        $ajax.trigger({
                            type: event,
                            json: json,
                            isLoad: load,
                        });
                    }
                    // $('.b-options-page__flat-content').animate({opacity: 1}, 300);
                    $('.b-options-page__flat-content').removeClass('loading');
                }
            });
        }

        $ajax.on('ajax', function (event) {

            if ('isPagin' in event && event.isPagin) {
                ajax(false, event.pagin);
            } else {
                $('body').trigger('pagin.first');
                ajax(false);
            }
        });

        if (isLoad) {
            ajax(isLoad);
        }
    }

    function serialize($element) {
        var o = {};

        $element.find("[name]").each(function () {

            // if (this.type == 'checkbox' && $(this).prop('checked')) {
            if (this.type == 'checkbox') {

                if (this.name.indexOf('[]') >= 0) {

                    if (!Array.isArray(o[this.name])) {
                        o[this.name] = [];
                    }

                    if ($(this).prop('checked')) {
                        o[this.name].push(this.value);
                    }

                } else {
                    o[this.name] = $(this).prop('checked');
                }
            }

            // if ((this.type == 'text' || this.tagName == 'SELECT') && this.value.length) {
            if ((this.type == 'text')) {
                o[this.name] = this.value;
            }
            if ((this.tagName == 'SELECT')) {
                o[this.name] = $(this).find(':selected').val();
            }

        });
        return o;
    }

    function getFilterNotEmpty(data) {
        var newObj = {};
        for (var key in data) {

            if (data[key] && (data[key].length || data[key] != false)) {
                newObj[key] = data[key];
            }
        }
        return newObj;
    }

    function setParamsFilter($element, data) {
        $element.find("[name]").each(function () {
            if ($(this).attr('name') in data) {
                var key = $(this).attr('name');
                if (this.tagName == 'SELECT') {

                    $(this).find('[value="'+data[key]+'"]').prop('selected', true);
                    $(this).trigger("chosen:updated");

                } else if (this.type == 'checkbox') {

                    if (key.indexOf('[]') >= 0) {

                        if (data[key].indexOf(this.value) >= 0) {
                            $(this).prop('checked', true);
                        } else {
                            $(this).prop('checked', false);
                        }

                    } else {
                        if (LocParams.isParam(key) && $('[data-not-fl=\'' + key + '\']').length) {
                            $('[data-not-fl=\'' + key + '\']').trigger({
                                type: 'data.done',
                                isTrue: eval(LocParams.getParams()[key])
                            });
                        }

                        $(this).prop('checked', eval(data[key]));
                    }

                } else if (this.type == 'text') {
                    $(this).attr('value', data[key]);
                    $('body').trigger({
                        type: 'slider',
                        json: {
                            name: key,
                            value: data[key]
                        }
                    });
                }
            }
        });
    }

    function setParamsByGetParams(data) {
        var params = LocParams.getParams();

        for (var key in params) {

            if (key in data) {

                if (key.indexOf('[]') >= 0) {
                    data[key] = params[key];
                } else {
                    data[key] = getNumbRepInput(key, params[key]);
                }
            }
        }

        return data;
    }
});

$(function () {
    resizeHideBlock(true);

    $(window).resize(function () {
        resizeHideBlock(false)
    });

    function resizeHideBlock(isInit) {
        if ($(window).width() > 1023 && $(window).height() < 400) {
            if (isInit) {
                setTimeout(function () {
                    hideFlex();
                }, 1000);
            } else {
                hideFlex();
            }
        } else {
            if ($('.Hide_Block').length) {
                $('.Hide_Block').css('display', 'none');
            }
            $('html').css('overflow-x', 'hidden');
        }
    }

    function hideFlex() {
        if ($('.Hide_Block').length) {
            $('.Hide_Block').css('display', 'flex');
        }
        $('html').css('overflow', 'hidden');
    }

    if ($('.Topbar .Topbar_icon').length) {
        $('.Topbar .Topbar_icon').on('click', function () {

            if ($('.Header').length && $('.Close_icon').length) {
                $('.Header').toggleClass('Topbar_active');
                $('.Close_icon').toggleClass('show');
            }
            //$('body').css({'overflow' : 'hidden'});
        });
    }

    if ($('.Close_icon').length) {
        $('.Close_icon').on('click', function () {

            if ($('.Close_icon').length && $('.Header.Topbar_active').length) {
                $('.Close_icon').removeClass('show');
                //$('body').css({'overflow' : 'visible'});
                $('.Header.Topbar_active').removeClass('Topbar_active');
            }
        });
    }
});
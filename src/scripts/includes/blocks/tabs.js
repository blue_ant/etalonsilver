$(function () {
    let $tabLink = $('.js-tab-link');
    let $tabCont = $('.js-tab-container');

    let fadeInTab;
    let fadeOutTab;

    if ($tabLink.length && $tabCont.length) {

        $tabLink.on('click', function (event) {
            event.preventDefault();
            let $link = $(this);
            let attr = $link.attr('data-tab-target');
            let $tab = $tabCont.not(':not([data-tab-id=\'' + attr + '\'])'); // Таб что нашли через атрибут

            // Активные таб и ссылка
            let $activeTab = $tabCont.not(':not(.active)');
            let $activeLink = $tabLink.not(':not(.active)');


            if (!$link.hasClass('active')) {
                fadeOutTab($activeTab, function () {
                    $('.js-flat-slider-small.slick-initialized').slick('refresh');
                    $('.js-flat-slider.slick-initialized').slick('refresh');

                    $activeTab.removeClass('active');
                    $activeLink.removeClass('active');

                    fadeInTab($tab, function () {
                        $link.addClass('active');
                        $tab.addClass('active');
                        

                        $('body').trigger({
                            type: 'tabs.end',
                            tabs: {
                                attr: attr
                            }
                        });
                    });
                });

                $('body').trigger({
                    type: 'tabs.start',
                    tabs: {
                        attr: attr
                    }
                });
            }
        });

        /**
         * Плавное появление таба
         * @param {jQElement} $tab - таб который нужно показать
         * @param {Function} callback - конец анимации
         */
        fadeInTab =function ($tab, callback) {
            callback = callback || false;
            $tab.stop().fadeIn(function () {
                if (callback) callback();
            });
        }

        /**
         * Плавное скрытие таба
         * @param {jQElement} $tab - таб который нужно скрыть
         * @param {Function} callback - конец анимации
         */
        fadeOutTab = function ($tab, callback) {
            callback = callback || false;
            $tab.stop().fadeOut(function () {
                if (callback) callback();
            });
        }
    }
});
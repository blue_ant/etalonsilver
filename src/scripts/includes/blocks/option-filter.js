$(function () {
    var $filterEvent = $('.js-filter-event');
    var $sliders = $('.js-slider-ui');
    const submitHandler = _.debounce(function () {
        $('.js-ajax').trigger('ajax');
    }, 800);

    if ($filterEvent.length && $sliders.length) {
        $filterEvent.on('change', function (event) {
            var $elem = $(event.target);
            var name = $elem.attr('name');

            if ( name !== 'section') {
                //
                setTimeout(function () {
                    $('body').trigger('pagin.first');
                });
                // if (LocParams.isParam(name)) {
                // if ($elem.attr('type') == 'checkbox') {
                //     LocParams.setParams(name, val);
                // } else {
                //     LocParams.setParams(name, $elem.val())
                // }
                // }

                if ($elem.attr('type') == 'checkbox') {

                    if ($elem.attr('name').indexOf('[') >= 0) {

                        if ($elem.prop('checked')) {
                            LocParams.setParams(name, $elem.val());
                        } else {
                            LocParams.removeParam(name, $elem.val());
                        }

                    } else {
                        LocParams.setParams(name, $elem.prop('checked'));
                    }
                }

                if ($elem[0].nodeName == 'SELECT') {
                    LocParams.setParams(name, $elem.val());
                }

                if ($elem.attr('type') == 'checkbox') {

                    if ($('[data-not-fl=\'' + name + '\']').length) {
                        $('[data-not-fl=\'' + name + '\']').trigger({
                            type: 'data.done',
                            isTrue: $elem.prop('checked')
                        });
                    }
                }


                submitHandler();
            }

        });

        LocParams.on(function (event) {
           location.reload(); 
        });
    }
});
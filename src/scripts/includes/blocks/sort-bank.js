$(function () {
    let $linkSorts = $('.js-sort-link');
    let baseSorts = '.js-sort-item';
    let itemSorts = baseSorts;
    let $contSorts = $('.js-sort-content');
    /**
     * Перемещение строк между блоками
     * при изменении разрешения
     */
    var fixTable = function () {
        var width = $(window).width();
        if(width < 768){
            //собираем названия корпусов
            let korpuses = [];
            $('.js-sort-item').each(function () {
                var c = $(this).data('korpus');

                if (korpuses.indexOf(c)==-1){
                    korpuses.push(c);
                }
            });

            if (korpuses.length >1 ){
                for (let i=2;i<=korpuses.length;i++){
                    let korpus = korpuses[i-1];
                    let $clonedSort = $('.b-options-page__filter-flat:first').clone();
                    let ci = 'c'+i;

                    if ($('.b-options-page__thead[data-korpus="'+korpus+'"]').length==0){
                        // Создаем заголовок
                        $('.js-sort-content:last').after(
                            $('<div>')
                                .addClass('b-options-page__thead b-options-page__thead--nofirst b-options-page__thead--commercial')
                                .attr('data-korpus',korpus)
                                .html('<span>'+korpus+'</span>')
                        );

                        // копируем меню сортировки
                        $clonedSort.find('.js-sort-link').attr('data-target',ci);
                        $('.b-options-page__thead:last').after(
                            $clonedSort
                        );

                        //перемещаем элементы в новый блок
                        $('.b-options-page__filter-flat:last').after(
                            $('<div>')
                                .addClass('b-options-page__flat-content b-options-page__flat-content--line b-options-page__flat-content--commercial js-sort-content js-sort-mobile')
                                .attr('data-target',ci).append(
                                $('.js-sort-item[data-korpus="'+korpus+'"]').attr('data-target',ci)
                            )
                        );
                    }

                }
            }


        }else{
            // удаляем вторичные блоки
            $('.b-options-page__thead--nofirst').remove();
            $('.b-options-page__filter-flat:not(:first):not([data-type])').remove();

            //Переселяем элменты в первый блок сортировки
            $('.js-sort-item').appendTo(
                $('.js-sort-content')
            ).attr('data-target','c1');

            $('.js-sort-content:not(:first)').remove();
        }
    };

    function toggleClass($elem, rm, add) {
        $elem.removeClass(rm);
        $elem.addClass(add);
    }

    function sortAsc($items, attr) {
        getSorted($items, "asc", attr).appendTo($contSorts);
    }

    function sortDesc($items, attr) {
        getSorted($items, "desc", attr).appendTo($contSorts);
    }

    function getSorted($items, typeSort, attr) {
        let $itemsSorted = $items.sort(function (a, b) {
            let $a = $(a),
                $b = $(b);
            let $sortA = $a.find('[data-sort-item=\'' + attr + '\']');
            let numbA = $sortA.attr('data-sort-numb');
            let $sortB = $b.find('[data-sort-item=\'' + attr + '\']');
            let numbB = $sortB.attr('data-sort-numb');

            if (numbB.indexOf(',') >= 0) {
                numbB = numbB.split(',');
            } else {
                numbB = parseFloat(numbB);
            }

            if (numbA.indexOf(',') >= 0) {
                numbA = numbA.split(',');
            } else {
                numbA = parseFloat(numbA);
            }

            if (typeof numbA == 'number' && typeof numbB == 'number') {
                return numbA - numbB;
                // } else if (typeof numbA == 'number' && typeof numbB != 'number') {
                //     return numbA - numbB[1];
                // } else if (typeof numbA != 'number' && typeof numbB == 'number') {
                //     return numbA[1] - numbB;
            } else {

                var obA = {
                    from: numbA[0],
                    to: numbA[1]
                };
                var obB = {
                    from: numbB[0],
                    to: numbB[1]
                }

                var resultFrom = obA.from - obB.from;
                var resultTo = obA.to - obB.to;

                if (resultTo == 0) {
                    return resultFrom;
                } else {
                    return resultTo;
                }
            }
        });

        if (typeSort == 'desc') {
            return Array.prototype.reverse.call($itemsSorted)
        }
        return $itemsSorted;
    }

    $(document).on('click', '.js-sort-link', function () {
        var isServe = eval($(this).attr('data-sort-serve')) || false;
        var target  = $(this).data('target');
        var event = $(this).attr('data-event') || false;

        if (Boolean(target)) {
            itemSorts = baseSorts+'[data-target="'+target+'"]';
            $contSorts = $('.js-sort-content'+'[data-target="'+target+'"]');

        } else {

        }

        if ($('.js-sort-link').length 
            && $(itemSorts).length
            && $('.js-sort-content').length) {
            var attr = $(this).attr('data-sort');
            var addParam = eval($(this).attr('data-param')) || false;
            var $sortItem = $(this).parent();
            var type = '';

            // Проверка кто соритрует, сервер или мы
            if (!isServe) { // Мы

                //очищаем классы у чужих фильтров
                $sortItem.addClass('working');
                $('.b-filter-flat__item:not(.working)').removeClass('asc desc');
                $sortItem.removeClass('working');

                if (!$sortItem.hasClass('active')) {
                    $('.js-sort-link').parent().removeClass('active');
                    $sortItem.addClass('active');
                }

                if ($sortItem.hasClass('asc')) {
                    toggleClass($sortItem, 'asc', 'desc');
                    sortDesc($(itemSorts), attr);
                    type = 'desc';

                } else if ($sortItem.hasClass('desc')) {
                    toggleClass($sortItem, 'desc', 'asc');
                    sortAsc($(itemSorts), attr);
                    type = 'asc';

                } else if (!$sortItem.hasClass('desc')
                        && !$sortItem.hasClass('asc')) {
                    toggleClass($sortItem, 'asc', 'desc');
                    sortDesc($(itemSorts), attr);
                    type = 'desc';
                }


                if (addParam) {
                    LocParams.setParams({
                        sort: type,
                        sortdir: attr
                    });
                }

                if (event != false) {
                    $('body').trigger({
                        type: event,
                        sort: {
                            type: type,
                            attr: attr
                        }
                    });
                }

            } else { // Сервер

                $sortItem.addClass('working');
                $('.b-filter-flat__item:not(.working)').removeClass('asc desc');
                $sortItem.removeClass('working');

                if (!$sortItem.hasClass('active')) {
                    $('.js-sort-link').parent().removeClass('active');
                    $sortItem.addClass('active');
                }

                let isDesc = $(this).parent().hasClass('desc');
                let isAsc = $(this).parent().hasClass('asc');

                if (!isDesc && !isAsc) {
                    $sortItem.addClass('asc');
                    type = 'asc';

                } else if (isAsc) {
                    $sortItem.removeClass('asc');
                    $sortItem.addClass('desc');
                    type = 'desc';

                } else if (isDesc){
                    $sortItem.removeClass('asc');
                    $sortItem.removeClass('desc');
                    type = '';
                    attr = '';
                }


                LocParams.setParams({
                    sort: attr,
                    sortdir: type
                });

                $('.js-ajax').trigger('ajax');
            }
        }
    });

    $(window).on('resize',function () {
        fixTable();
    });

    //WTF?
    $('body').on('sort.type', function (event) {

        if ('sort' in event) {
            var $el = $('.js-sort-link[data-sort=\'' + event.sort.attr + '\']');
            var $elem = $el.parent();
            var addParam = eval($el.attr('data-param')) || false;

            if (event.sort.type == 'asc') {
                toggleClass($elem, 'desc', 'asc');

            } else {
                toggleClass($elem, 'asc', 'desc');
            }

            $('.js-sort-link').parent().removeClass('active');
            $elem.addClass('active');
            // getSorted($('.js-sort-item'), event.sort.type, event.sort.attr).appendTo($contSorts);

            // if (addParam) {
            //     LocParams.setParams({
            //         sort: type,
            //         sortdir: attr
            //     });
            // }
        }
    });

    fixTable();

    // set sort icon
    setTimeout(function () {
        if ($('.js-sort-link').length) {
            let sortVal = $('#sort').val();
            let sortDir = $('#sortdir').val();
            $('.js-sort-link[data-sort="'+ sortVal +'"]').parent().addClass(sortDir);
        }
    }, 500)

    

    $('[data-sort="rate"]').trigger('click');
    $('[data-sort="rate"]').trigger('click');



});
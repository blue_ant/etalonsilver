SVGElement.prototype.contains = function contains(node) {
    if (!(0 in arguments)) {
        throw new TypeError('1 argument is required');
    }

    do {
        if (this === node) {
            return true;
        }
    } while (node = node && node.parentNode);

    return false;
};

function declOfNum(number, titles) {
    let cases = [2, 0, 1, 1, 1, 2];

    return titles[number % 100 > 4 && number % 100 < 20 ? 2 : cases[number % 10 < 5 ? number % 10 : 5]];
}

let fitblocksCounter = 0;

class Fitblock {
    constructor(el) {
        this.$el = $(el);
        this.outer = this.$el.get(0);
        this.inner = this.outer.getElementsByClassName("Fitblock_inner")[0];
        this.pusher = this.inner.getElementsByClassName("Fitblock_pusher")[0];
        this.id = fitblocksCounter;

        if (this.pusher.complete || this.pusher.readyState === 4) {
            this.inner.style.visibility = "visible";
            this.refresh();
        } else {
            $(this.pusher).on("load", () => {
                this.inner.style.visibility = "visible";
                this.refresh();
            });
        }

        $(window).on(
            `resize.fitblock-${this.id}`,
            _.debounce(() => {
                this.refresh();
            }, 100)
        );

        fitblocksCounter++;
    }

    refresh() {
        let outer = this.outer;
        let inner = this.inner;
        let pusher = this.pusher;
        let wrapH = outer.offsetHeight;
        let wrapW = outer.offsetWidth;

        let pusherH = pusher.naturalHeight || pusher.width;
        let pusherW = pusher.naturalWidth || pusher.height;

        let rel = pusherW / pusherH;
        if (wrapW / pusherW > wrapH / pusherH) {
            pusher.style.width = `${wrapW}px`;
            pusher.style.height = "auto";
            inner.style.marginLeft = `-${wrapW / 2}px`;
            inner.style.marginTop = `-${wrapW / rel / 2}px`;
        } else {
            pusher.style.width = "auto";
            pusher.style.height = `${wrapH}px`;
            inner.style.marginLeft = `-${(wrapH * rel) / 2}px`;
            inner.style.marginTop = `-${wrapH / 2}px`;
        }
    }

    destroy() {
        $(window).off(`.fitblock-${this.id}`);
    }
}

$('.Fitblock').each(function (index, el) {
    new Fitblock($(this));
});

/*$('.SvgWrap_section').hover(
    function () {
        $(this).addClass('SvgWrap_section-hover');
    },
    function () {
        $(this).removeClass('SvgWrap_section-hover');
    }
);*/

$('.SvgWrap_section').hover(
    function () {
        /*let svgID = "#" + $(this).attr("id") + " .SvgWrap_floor";
        $(svgID).addClass('_white');*/

        let svgID = "#" + $(this).attr("id") + " .SvgWrap_floor";

        $(svgID).each(function(index, value) {
            if ( $(this).attr('data-flat') > 0 ||  $(this).attr('data-apart') > 0 ) {
                $(this).addClass('_white');
            } else {
                $(this).addClass('');
            }
        });
    },
    function () {
        $(".SvgWrap_floor").removeClass('_white');
    }
);

$('.SvgWrap_floor').hover(
    function () {
        let flatCountH = $(this).attr("data-flat");
        let flatApartH = $(this).attr("data-apart");
        if ( flatCountH > 0 || flatApartH > 0 ) {
            $(this).addClass('SvgWrap_floor-hover');
        } else {
            $(this).addClass('SvgWrap_floor-disable');
        }
    },
    function () {
        $(this).removeClass('SvgWrap_floor-hover');
        $(this).removeClass('SvgWrap_floor-disable');
    }
);

let genplanSections = new Swiper('.GenplanSectionsSlider', {
    spaceBetween: 30,
    effect: 'fade',
    speed: 400,
    preventInteractionOnTransition: true,
    threshold: 9999
});

let genplanSectionsCommercial = new Swiper('.GenplanSectionsCommercialSlider', {
    spaceBetween: 30,
    effect: 'fade',
    speed: 400,
    preventInteractionOnTransition: true,
    threshold: 9999,
    navigation: {
        nextEl: '.next',
        prevEl: '.prev',
    },
    pagination: {
        type: 'bullets',
        el: '.GenplanSectionsCommercialSlider .swiper-pagination',
        clickable: true,
        dynamicBullets: true,
        renderBullet: function (index, className) {
            return '<span class="' + className + '"><span class="prev"></span> Секция ' + (index + 1) + ' <span class="next"></span></span>';
        },
    }
});

$('.GenplanSectionsCommercialSlider').on('click', '.prev', function(){
    genplanSectionsCommercial.slidePrev(400);
});

$('.GenplanSectionsCommercialSlider').on('click', '.next', function(){
    genplanSectionsCommercial.slideNext(400);
});

$('.GenplanSectionsSlider_nav').on('click', function () {
    let slideIndex = $(this).data('slide');
    genplanSections.slideTo(slideIndex);
})

let tooltipsFloors;

let tooltipsSections = () => {
    let winWidth = $(window).width();
    if (winWidth > 1023) {
        tooltipsFloors = null;
        tooltipsFloors = tippy('.SvgWrap_floor', {
            animateFill: false,
            animation: 'scale',
            placement: 'top-end',
            duration: 400,
            distance: 20,
            theme: 'light',
            followCursor: true,
            arrow: false,
            boundary: 'window',
            appendTo: document.body,
            content: function (reference ) {
                let tpl;
                let floor = reference.getAttribute('data-floor');
                let section = reference.getAttribute('data-section');
                let flatCount = reference.getAttribute('data-flat');
                let apartCount = reference.getAttribute('data-apart');
                let commercialFlats = reference.getAttribute('data-commercial-flats');
                let isCommercial = reference.hasAttribute('data-commercial-flats');
                if (isCommercial) {
                    tpl = `<div class="SectionTooltip SectionTooltip-commercial">
                        <div class="SectionTooltip_row SectionTooltip_row-commercial SectionTooltip_row-amount"><span>`+ commercialFlats +`</span> `+  declOfNum(commercialFlats, ["помещение", "помещения", "помещений"]) + ` <br /> в продаже</div>
                        <div class="SectionTooltip_row SectionTooltip_row-commercial">
                            <div class="SectionTooltip_col"><span>`+ section +`</span> секция</div>
                            <div class="SectionTooltip_col"><span>`+ floor +`</span> этаж</div>
                        </div>
                        <div>`;
                } else {
                    if (flatCount > 0) {
                        tpl = `<div class="SectionTooltip">
                            <div class="SectionTooltip_row"><span>` + section + `</span> секция</div>
                            <div class="SectionTooltip_row"><span>` + floor + `</span> этаж</div>
                            <div class="SectionTooltip_row"><span>` + flatCount + `</span> ` + declOfNum(flatCount, ["квартира", "квартиры", "квартир"]) + ` в продаже</div>
                           <div>`;
                    } else if (flatCount == 0) {
                        tpl = `<div class="SectionTooltip">
                            <div class="SectionTooltip_row"><span>Все квартиры проданы</span></div>
                           <div>`;
                    } else if (apartCount > 0)  {
                        tpl = `<div class="SectionTooltip">
                            <div class="SectionTooltip_row"><span>` + section + `</span> секция</div>
                            <div class="SectionTooltip_row"><span>` + floor + `</span> этаж</div>
                            <div class="SectionTooltip_row"><span>` + apartCount + `</span> ` + declOfNum(apartCount, ["апартамент", "апартамента", "апартаментов"]) + ` в продаже</div>
                           <div>`;
                    } else if (apartCount == 0) {
                        tpl = `<div class="SectionTooltip">
                            <div class="SectionTooltip_row"><span>Все апартаменты проданы</span></div>
                           <div>`;
                    }
                }
                return tpl;
            }

        });
    } else {
        let used;
        tooltipsFloors = null;

        $('.SvgWrap_section a').on('click', function (e) {
            if( used !== this) used = this, e.preventDefault();
        });

        tooltipsFloors = tippy('.SvgWrap_floor', {
            animateFill: false,
            animation: 'scale',
            placement: 'top-end',
            duration: 400,
            distance: 20,
            theme: 'light',
            trigger: 'click',
            followCursor: true,
            boundary: 'window',
            appendTo: document.body,
            content: function (reference ) {
                let tpl;
                let floor = reference.getAttribute('data-floor');
                let section = reference.getAttribute('data-section');
                let flatCount = reference.getAttribute('data-flat');
                let apartCount = reference.getAttribute('data-apart');
                let commercialFlats = reference.getAttribute('data-commercial-flats');
                let isCommercial = reference.hasAttribute('data-commercial-flats');
                if (isCommercial) {
                    tpl = `<div class="SectionTooltip SectionTooltip-commercial">
                        <div class="SectionTooltip_row SectionTooltip_row-commercial SectionTooltip_row-amount"><span>`+ commercialFlats +`</span> `+  declOfNum(commercialFlats, ["помещение", "помещения", "помещений"]) + ` <br /> в продаже</div>
                        <div class="SectionTooltip_row SectionTooltip_row-commercial">
                            <div class="SectionTooltip_col"><span>`+ section +`</span> секция</div>
                            <div class="SectionTooltip_col"><span>`+ floor +`</span> этаж</div>
                        </div>
                        <div>`;
                } else {
                    if (flatCount > 0) {
                        tpl = `<div class="SectionTooltip">
                            <div class="SectionTooltip_row"><span>` + section + `</span> секция</div>
                            <div class="SectionTooltip_row"><span>` + floor + `</span> этаж</div>
                            <div class="SectionTooltip_row"><span>` + flatCount + `</span> ` + declOfNum(flatCount, ["квартира", "квартиры", "квартир"]) + ` в продаже</div>
                           <div>`;
                    } else if (flatCount == 0) {
                        tpl = `<div class="SectionTooltip">
                            <div class="SectionTooltip_row"><span>Все квартиры проданы</span></div>
                           <div>`;
                    } else if (apartCount > 0)  {
                        tpl = `<div class="SectionTooltip">
                            <div class="SectionTooltip_row"><span>` + section + `</span> секция</div>
                            <div class="SectionTooltip_row"><span>` + floor + `</span> этаж</div>
                            <div class="SectionTooltip_row"><span>` + apartCount + `</span> ` + declOfNum(apartCount, ["апартамент", "апартамента", "апартаментов"]) + ` в продаже</div>
                           <div>`;
                    } else if (apartCount == 0) {
                        tpl = `<div class="SectionTooltip">
                            <div class="SectionTooltip_row"><span>Все апартаменты проданы</span></div>
                           <div>`;
                    }
                }
                return tpl;
            }
        });

    }
};

tooltipsSections();

$(window).on('resize', function () {
    tooltipsSections();
})













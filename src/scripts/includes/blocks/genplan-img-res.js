$(function () {
    var $n = $('.js-n'); // Картинки

    /**
     * Получаем координаты
     * @return {Object} получаем объект координат
     */
    function getCoors() {
        var x, y; // Координаты
        var height;
        var width;

        if (window.innerWidth >= 320 && window.innerWidth < 768) {
            x = -166;
            y = 4;
            width = '107';//getPixel('width', 108) - 15;
            height = '107';

        } else if (window.innerWidth >= 768 && window.innerWidth < 1280) {
            x = 100;
            y = 2;
            width = '108';//getPixel('width', 108) - 15;
            height = '108';

        } else if (window.innerWidth >= 1280 && window.innerWidth < 1920) {
            x = 85;
            y = 1;
            width = '93';//getPixel('width', 108) - 15;
            height = '93';
        } else if (window.innerWidth >= 1920) {
            // x = 0;
            // y = 0;
            // width = getPixel('width', 94) - 25;
            x = 85;
            y = 1;
            width = '93';//getPixel('width', 108) - 15;
            height = '93';
        }

        // height = width * 1600 / 2560;

        return {
            x: x,
            y: y,
            height: height + '%',
            width: width + '%'
        };
    }

    /**
     * Переводит из процентов в пиксели ширину
     * @param {String} type - тип размера width или height
     * @param {Number} procent - тут целочисленный процент без %
     * @return {Number | Float} плучаем ширину
     */
    // function getPixel(type, procent) {
    //     var maxProcent = 100;
    //     var style = $n[0].getBoundingClientRect();
    //     var out = 0;

    //     return Math.round(
    //         $(window).width()*procent/100
    //     );

    //     if (procent > maxProcent) {
    //         out = procent - maxProcent;

    //         return ((style[type] * out) / maxProcent) + style[type];
    //     } else {
    //         return (style[type] * procent) / maxProcent;
    //     }
    // }

    /**
     * Устанавливаем координаты для картинок
     */
    function setCoorOnImg() {

        if ($n.length) {
            var coors = getCoors(); // Тут будем хронить координаты
            $n.attr(coors);
        }
        
    }
    setCoorOnImg();


    /**
     * Спецзаказ для уникального отображения
     */
    function reziseImgGenplan() {
        var $target = $('.b-floor-select--genplan');
        if ($target.length==0) return;
        var $parent = $target.parent();
        var height = $parent.height();
        var width = height*(800/643);// так надо
        var wwidth = $(window).width();

        if (wwidth >=768 && wwidth<1024){
            if (width>=wwidth){
                $target.css({
                    height:height,
                    width:width
                })
            }else{
                height = wwidth*(643/800);
                $target.css({
                    height:height,
                    width:'100%'
                })
            }
        }else{
            $target.css({
                height:'',
                width:''
            })
        }

    }
    reziseImgGenplan();

    $(window).resize(function () {
        setCoorOnImg();
        reziseImgGenplan();
    });
});
$(function () {
    let $ymaps = $('#ymaps');
    let $ymapsFilters = $('.js-yandex-filter');
    let $ymapsFiltersShowAll = $('.js-ymaps-filter-show-all');
    let $ymapsFiltersHideAll = $('.js-ymaps-filter-hide-all');
    var placemarksCollection;
    var myMap;
    var items;

    let ymapsInit;
    let ymapsAddPlacemarkOnMap;
    let ymapsDeletePlacemarkOnMap;
    let getItemsFiltered;
    let ymapsInitPlacemark;
    let ymapsFiltersEventChange;
    let ymapsShowEventClick;
    let ymapsHideEventClick;

    if ($ymaps.length && $ymapsFilters.length &&
        $ymapsFiltersShowAll.length &&
        $ymapsFiltersHideAll.length) {

        ymapsInit = function () {
            let url = $ymaps.attr('data-url');
            var myPlacemark;
            var zoomControl = new ymaps.control.ZoomControl({
                options: {
                    size: "small",
                    position: {
                        top: 15,
                        right: 15,
                    }
                }
            });

            var fullscreenControl = new ymaps.control.FullscreenControl({
                options: {
                    position: {
                        top: 81,
                        right: 15
                    }
                }
            });

            $.get(url, function (json) {
                items = json.data.items;
                let center = json.data.center;
                if (json.status) {
                    myMap = new ymaps.Map("ymaps", {
                        center: center.coordinate,
                        zoom: 15,
                        controls: [fullscreenControl, zoomControl],
                    });

                    ymapsTouchScroll(myMap);
                    myMap.behaviors.disable(['scrollZoom']);

                    if (center.img) {
                        myPlacemark = new ymaps.Placemark(center.coordinate, {}, {
                            iconLayout: 'default#image',
                            iconImageHref: center.img,
                            iconImageSize: [119, 122],
                            iconImageOffset: [-55, -60],
                            zIndex: 1000
                        });
                    } else {
                        myPlacemark = new ymaps.Placemark(center.coordinate);
                    }

                    myMap.geoObjects.add(myPlacemark);
                    placemarksCollection = new ymaps.GeoObjectCollection();
                    ymapsInitPlacemark();
                    myMap.events.add('touchstart pointerdown MSPointerDown mousedown touchmove pointermove MSPointerMove mousemove touchend pointerup MSPointerUp mouseup touchcancel pointercancel MSPointerCancel mousecancel orientationchange transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd click', function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                    });
                }
            });
        };
        // Событие для пометки
        ymapsFiltersEventChange = function () {
            ymapsInitPlacemark()
        };

        // Событие для показывания
        ymapsShowEventClick = function () {
            $ymapsFilters.prop('checked', true);
            ymapsInitPlacemark();
        };

        // Событие для скрывания
        ymapsHideEventClick =function () {
            $ymapsFilters.prop('checked', false);
            ymapsInitPlacemark();
        };

        // Запуск карты
        ymaps.ready(ymapsInit);

        // События
        $ymapsFilters.on('change', ymapsFiltersEventChange)
        $ymapsFiltersShowAll.on('click', ymapsShowEventClick);
        $ymapsFiltersHideAll.on('click', ymapsHideEventClick);

        /**
         * Установка точек на карте
         * @param {jQElement} $checkeds - все поля с галочкой
         */
        ymapsAddPlacemarkOnMap = function (items) {
            for (let i = 0; i < items.length; ++i) {
                let item = items[i];
                let img = item.img;
                let color = item.color;
                let coors = item.coordinates;

                for (let n = 0; n < coors.length; ++n) {
                    let coor = coors[n];

                    if (img) {
                        placemarksCollection.add(new ymaps.Placemark(coor, {}, {
                            iconLayout: 'default#image',
                            iconImageHref: img,
                            iconImageSize: [42, 42],
                            // iconImageOffset: [-3, -42]
                        }))

                    } else {
                        placemarksCollection.add(new ymaps.Placemark(coor, {}, {
                            preset: 'islands#icon',
                            iconColor: color
                        }));
                    }
                }
                myMap.geoObjects.add(placemarksCollection);
            }
        }

        // Удоляем все точки на карте
        ymapsDeletePlacemarkOnMap = function () {
            placemarksCollection.removeAll();
        }

        getItemsFiltered = function ($checkeds) {
            let newItems = [];
            $checkeds.each(function () {
                let $this = $(this);

                for (let key in items) {
                    let id = items[key]['target-id'];
                    if (id == $this.attr('id')) {
                        newItems.push(items[key]);
                        break;
                    }
                }
            });
            return newItems;
        };

        // Запускаем и помечаем на карте то что выбрано в фильтре
        ymapsInitPlacemark = function () {
            let $ymapsFiltersChecked = $ymapsFilters.not(':not(:checked)');
            var itemFilter = getItemsFiltered($ymapsFiltersChecked);

            ymapsDeletePlacemarkOnMap();

            if ($ymapsFiltersChecked.length) {
                ymapsAddPlacemarkOnMap(itemFilter);
            }
        };
        
    }

});
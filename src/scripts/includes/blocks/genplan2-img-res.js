$(function () {
    var $n = $('.js-n'); // Картинки



    /**
     * Спецзаказ для уникального отображения
     */
    function reziseImgGenplan() {
        var $target = $('.b-floor-select--genplan');
        if ($target.length==0) return;
        var $parent = $target.parent();
        var height = $parent.height();
        // var width = height*(800/643);// так надо (1 вариант)
        var width = height*(2559/1383);// так надо (2 вариант)

        var wwidth = $(window).width();

        if (wwidth >=768 && wwidth<1024){
            if (width>=wwidth){
                $target.css({
                    height:height,
                    width:width
                })
            }else{
                // height = wwidth*(643/800); // (1 вариант)
                height = wwidth*(1383/2559); // (2 вариант)
                $target.css({
                    height:height,
                    width:'100%'
                })
            }
        }else{
            $target.css({
                height:'',
                width:''
            })
        }

    }
    reziseImgGenplan();

    $(window).resize(function () {
        reziseImgGenplan();
    });
});
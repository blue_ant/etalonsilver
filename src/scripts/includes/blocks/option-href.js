$(function () {
    
    $(document).on('click', '.js-href-click', function () {
        var $hrefBlock = $('.js-href-click');

        if ($hrefBlock.length) {
            var target = $(this).attr('data-target') || false;
            var href = $(this).find('[data-href]').attr('data-href');

            if (target == false) {
                target = '_self';
            }

            if (href.length) {
                window.location.href = href;
                // window.open(href, target);
            }
        }
    });
});
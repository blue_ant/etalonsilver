$(function () {
    let $popups = $('.js-popup-block');
    let $links = $('.js-popup-link');
    let $closes = $('.js-popup-close');

    if ($links.length && $popups.length) {

        $(document).on('click', '.js-popup-link', function (event) {
            // event.stopPropagation();

            var eventData = $(this).data('popup-event') || false;

            // if (window.innerWidth >= 768) {

                if (!$(this).hasClass('active')) {
                    showPopupBank($(this));

                } else {

                    if ($(event.target).hasClass('js-popup-block')) {
                        hidePopupBank();
                    }
                }
            // }

            if (eventData) {
                $(this).trigger({
                    type: eventData,
                    $popup: getActivePopup($(this)),
                });
            }

        });

        $(document).on('click', '.js-popup-close', function (event) {
            // event.stopPropagation();
            var eventData = $(this).data('popup-event') || false;

            if (eventData) {
                $(this).trigger({
                    type: eventData,
                    $popup: getActivePopup($('.js-popup-link.active'))
                });
            }
            hidePopupBank();
        });

        $(window).resize(function () {

            if (window.innerWidth >= 320 && window.innerWidth < 768) {
                hidePopupBank();
            }
        });

        $(document).on('keydown', function (event) {

            if (event.keyCode == 27 && event.key == "Escape") {
                hidePopupBank();
            }
        });
    }

    function getActivePopup($element) {
        return $('.js-popup-block').not(':not([data-popup-id=\'' + $element.attr('data-target-popup') + '\'])');
    }

    function showPopupBank($show) {
        let $popup = getActivePopup($show);
        $show.addClass('active');
        $popup.css({
            display: 'flex',
            opacity: 0,
        }).animate({
            opacity: 1,
        }, 400, function () {
            $popup.css({
                display: '',
                opacity: '',
            });
        });
    }

    function hidePopupBank() {
        let $active = $('.js-popup-link.active');
        let $popup = getActivePopup($active);

        $popup.stop().fadeOut(function () {
            $active.removeClass('active');
            $popup.css('display', '');
        });
    }

    $('body').on('popup.closeAll', hidePopupBank);

    // function transitionEnd($element, callback) {
    //     $element.on('transitionend webkitTransitionEnd oTransitionEnd otransitionend MSTransitionEnd', function () {
    //         callback();
    //     });
    // }
});
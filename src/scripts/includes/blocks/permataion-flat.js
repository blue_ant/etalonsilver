    var $flatElement = $('.js-flat-header');
    var $flatTabletPlace = $('.js-default-header');
    var $flatMobilePlace = $('.js-mobile-flat');

    if ($flatMobilePlace.length > 0) {

        var flatAdaptive = function () {
            if (window.innerWidth >= 320 && window.innerWidth < 1024) {
                $flatMobilePlace.before($flatElement);
            } else if (window.innerWidth >= 1024) {
                $flatTabletPlace.before($flatElement);
            }
        };
        flatAdaptive();

        $(window).resize(function () {
            if ($flatMobilePlace.length > 0) {
                flatAdaptive();
            }
        });
    }

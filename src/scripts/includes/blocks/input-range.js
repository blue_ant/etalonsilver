$(function () {
    var $sliderRangeContainers = $('.js-slider-range-container');

    if ($sliderRangeContainers.length) {
        $sliderRangeContainers.each(function () {
            var $this = $(this);
            var $sliderRange = $(this).find(".js-slider-range");
            var $sliderRangeId = $('#' + $sliderRange.attr('id'));
            var $amonut = $(this).find('.js-slider-range-input');
            var min = parseFloat($amonut.data('min-val'));
            var max = parseFloat($amonut.data('max-val'));
            var cur = parseFloat($amonut.data('current-val'));
            var step = $amonut.data('step');
            var prefix = $amonut.data('prefix');
            var values = $amonut.data('values');
            var year = $amonut.data('year');
            var arrayStep = [];
            var curentStep = 0;
            var uiValue = 0;
            var options = {
                reverse: true,
                onChange: function (cep, e, $field, options) {
                    var value = getNormalNumber($field.val());
                    if (!('slide' in e)) {
                        curentStep = 0
                        arrayStep = [];
                        normalStep();
                    }

                    setMasked(prefix, $field, value, options);
                    $sliderRangeId.slider('value', value);
                }
            };

            function getMountOrYear(val) {
                if (getMount(val)) {
                    return getMount(val);
                } else {
                    return val + getPrefix(val, prefix) + '.';
                }
            }

            function setInContainInfo() {
                if ($this.parents('.js-slider-range-wrapper').length) {
                    var $sliderRangeWrapper = $this.parents('.js-slider-range-wrapper');
                    var $minMoney = $sliderRangeWrapper.find('.js-min-money');
                    var $midMoney = $sliderRangeWrapper.find('.js-mid-money');
                    var $maxMoney = $sliderRangeWrapper.find('.js-max-money');
                    var $minYear = $sliderRangeWrapper.find('.js-min-year');
                    var $midYear = $sliderRangeWrapper.find('.js-mid-year');
                    var $maxYear = $sliderRangeWrapper.find('.js-max-year');
                    // var numbersPrefix = [" млн.", " тыс."];

                    if ($minMoney.length && $midMoney.length && $maxMoney.length) {
                        $minMoney.text(getMoneyPrefix(min, " млн.", " тыс."));
                        $midMoney.text(getMoneyPrefix(max / 2, " млн.", " тыс."));
                        $maxMoney.text(getMoneyPrefix(max, " млн.", " тыс."));

                    } else if ($minYear.length && $midYear.length && $maxYear.length) {
                        $minYear.text(getMountOrYear(min));

                        if (values) {
                            $midYear.text(getMountOrYear(getMiddleNumb(arrayStep)));
                        } else {
                            $midYear.text(getMountOrYear(max / 2));
                        }
                        $maxYear.text(getMountOrYear(max));
                    }
                }
            }
            // setInContainInfo();

            var optionsSlider = {
                range: "min",
                value: cur,
                min: min,
                max: max,
                slide: function (event, ui) {
                    $amonut.val(ui.value);

                    if (!arrayStep.length && !values) {
                        stapes()

                    } else if (values) {
                        stapesYear();
                    }

                    if (step && !values) {
                        $amonut.val(arrayStep[ui.value]);

                    } else if (values) {
                        uiValue = ui.value;
                        $amonut.val(arrayStep[ui.value]);
                        $amonut.attr('data-is-mounth', arrayStep[ui.value]);
                    }

                    if (year && !values) {
                        setMaskedYear(prefix, $amonut, ui.value);
                    }
                    $amonut.trigger({
                        type: 'input',
                        slide: true
                    });

                    $('body').trigger('ui.slider-range-slide');
                },
                create: function (event, ui) {
                    $amonut.val(cur);
                    $amonut.trigger({
                        type: 'input',
                        slide: true
                    });

                    if (step && !values) {
                        stapes();

                    } else if (values) {
                        stapesYear(function (val) {
                            $amonut.attr('data-is-mounth', val);
                            $('body').trigger('create');
                        });
                    }

                    setMasked(prefix, $amonut, cur, options);
                    $amonut.off('blur', sliderInputBlur);
                    $amonut.on('blur', sliderInputBlur);

                    if (year) {
                        $amonut.off('input', yearMaskKeyDown);
                        $amonut.on('input', yearMaskKeyDown);
                    }
                    setInContainInfo();
                }
            }

            // Отдельная фукция для события Blur
            function sliderInputBlur() {
                var value = $(this).val().replace(/[^\d]/g, '');

                if (value < min) {
                    value = min;
                } else if (value > max) {
                    value = max;
                }

                setMasked(prefix, $(this), value, options);

                if (!year) {
                    $(this).val($(this).masked(value));
                }

                if (!arrayStep.length && !year) {
                    stapes()
                }

                if (arrayStep.length) {
                    $sliderRangeId.slider('value', arrayStep.indexOf(getNumberClosest(value, arrayStep)));
                } else {
                    if (value < min) {
                        $sliderRangeId.slider('value', 0);

                    } else if (value > max) {
                        $sliderRangeId.slider('value', (max / step));

                    } else {
                        $sliderRangeId.slider('value', value);
                    }
                }
            }

            /**
             * Маска для годов
             */
            function yearMaskKeyDown() {
                var value = $(this).val().replace(/[^\d]/g, '');
                var len = String(value).length;
                setMaskedYear(prefix, $(this), value);

                if (this.selectionEnd > len) {
                    this.selectionEnd = len;
                }
            }

            // Запускаем ползунок (В jq ui это slider)
            $sliderRangeId.slider(optionsSlider);

            if ($this.parents('.js-slider-range-wrapper').length) {
                var $sliderRangeWrapper = $this.parents('.js-slider-range-wrapper');
                var $sliderRangeSelect = $sliderRangeWrapper.find('.js-select-input');
                $sliderRangeSelect.on('change', function (event) {
                    var $opt = $(this).find('option[data-prefix=\'' + $(this).val().trim() + '\']');
                    min = parseInt($opt.data('min-val'));
                    max = parseInt($opt.data('max-val'));
                    cur = parseInt($opt.data('current-val'));

                    if (prefix) {
                        prefix = $opt.data('prefix');
                    }
                    step = $opt.data('step');
                    $sliderRangeId.slider("destroy");
                    setInContainInfo();
                    $sliderRangeId.slider(optionsSlider);
                    $sliderRangeId.slider('value', arrayStep.indexOf(getNumberClosest(cur, arrayStep)));

                    if ($('.js-symbol').length) {
                        $('.js-symbol').text($opt.data('prefix'));
                    }
                    $('body').trigger('create');
                });
            }

            /**
             * Получаем обычное число
             * @param {String} val - значение
             * @return {Number} - число
             */
            function getNormalNumber(val) {
                return parseFloat(val);
            }

            /**
             * Получаем префикс для денег
             * @param {Number} value - значение
             * @param {String} million - префикс милионы
             * @param {String} thousand - префикс тысячи
             * @return {String} - число с префиксом
             */
            function getMoneyPrefix(value, million, thousand) {
                return (value >= 1000000) ? value / 1000000 + million : value / 1000 + thousand;
            }

            /**
             * Получаем ближайшее число из массива
             * @param {Number} value - значение
             * @param {Array} array - массив чисел
             * @return {Number} - число близкое со значение
             */
            function getNumberClosest(number, array) {
                return array.reduce(function (prev, curr) {
                    return (Math.abs(curr - number) < Math.abs(prev - number) ? curr : prev);
                });
            }

            /**
             * Склонение числительных
             * @param {Number} number - число
             * @param {Array} mark - массив префиксов
             * @return {String} - получаем префик
             * Пример: declOfNum(1, ['год','года','лет']) - выведет год
             */
            function declOfNum(number, titles) {
                var cases = [2, 0, 1, 1, 1, 2];
                return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
            }

            /**
             * Нармолизуем и получаем префикс
             * @param {Number} number - число
             * @param {String|Array-String} prefix - вводим префикс или префиксы через ","
             * @return {String} - получаем префик
             */
            function getPrefix(number, newPrefix) {
                newPrefix = newPrefix || '';

                if (newPrefix != '') {
                    if (newPrefix.indexOf(',') >= 0) {
                        newPrefix = declOfNum(parseInt(number), newPrefix.split(','))
                    }
                }
                return ' ' + newPrefix.replace(/[\s]/g, '');
            }

            /**
             * Установки маски
             * @param {String} prefix - обычный префикс
             * @param {jQElement} $field - элемент jquery
             * @param {String | Number} cur - число
             * @param {Object} options - настройки для jqmask
             */
            function setMasked(prefix, $field, cur, options) {
                if (year) {
                    setMaskedYear(prefix, $field, cur);
                } else {
                    if (prefix != '') {

                        if (prefix.indexOf(',') >= 0) {
                            $field.mask('#0' + getPrefix(cur, prefix), options);

                        } else {
                            $field.mask('# ##0' + getPrefix(cur, prefix), options);
                        }

                    } else {
                        $field.mask('# ##0', options);
                    }
                }
            }

            /**
             * Получаем месяц
             * @param {Float} number - число от 0.1 до 0.9
             * @return {String} - возврошаем месяца с префиксом или false
             */
            function getMount(number) {
                if (number < 1) {
                    return Math.round(12 * (number * 100) / 100) + ' мес.';
                }
                return false;
            }

            // Нормализация шагов при вводе в инпут
            function normalStep() {
                $sliderRangeId.slider("option", "min", min);
                $sliderRangeId.slider("option", "max", max);
                $sliderRangeId.slider("option", "step", 1);
            }

            // Шаги для нормальных чисел (деньги)
            function stapes() {
                if (step) {
                    var indexStep = (max / step) + 1;
                    var newStep = 0;
                    curentStep = 0
                    arrayStep = [];

                    if (step < min) {
                        newStep += min;
                    }

                    for (var i = 0; i < indexStep; ++i) {

                        if (i == 0) {
                            arrayStep.push(min);

                        } else if (newStep < max) {
                            newStep += step;

                            if (newStep >= max) {
                                arrayStep.push(max);

                            } else {
                                if (newStep > min) {
                                    arrayStep.push(newStep);
                                }
                            }
                        }

                        if (newStep < cur) {
                            curentStep = i;
                        }
                    }

                    $sliderRangeId.slider("option", "min", 0);
                    $sliderRangeId.slider("option", "max", arrayStep.length - 1);
                    $sliderRangeId.slider("option", "value", curentStep);
                    $sliderRangeId.slider("option", "step", 1);
                }
            }

            // Масак для годов
            function setMaskedYear(prefix, $field, cur) {

                if (String(arrayStep[uiValue]).indexOf('.') >= 0) {
                    $amonut.val(arrayStep[uiValue]);

                    if (getMount(arrayStep[uiValue])) {
                        $field.val(getMount(arrayStep[uiValue]));

                    } else {
                        $field.val(arrayStep[uiValue] + getPrefix(arrayStep[uiValue], prefix));
                    }

                } else {
                    $field.val(cur + getPrefix(cur, prefix));
                }
            }

            function getArrayMin(array, n) {
                while (array.length > n) {
                    array.shift();
                    array.pop();
                }
                return array;
            }
            function getMiddleNumb(array) {
                if (array.length % 2 == 0) { // четное
                    var arr = getArrayMin(array.concat(), 2);
                    return (+arr[0] + +arr[1]) / 2;
                } else { // не четное
                    var arr = getArrayMin(array.concat(), 1);
                    return +arr[0];
                }
            }

            // Шаги для годов
            function stapesYear(callback) {
                callback = callback || false;

                if (year) {
                    arrayStep = values.split(',');
                    var closest = getNumberClosest(cur, arrayStep);
                    curentStep = arrayStep.indexOf(closest);
                    uiValue = curentStep;
                    $sliderRangeId.slider("option", "min", 0);
                    $sliderRangeId.slider("option", "max", arrayStep.length - 1);
                    $sliderRangeId.slider("option", "value", curentStep);
                    $sliderRangeId.slider("option", "step", 1);
                    if (callback) callback(arrayStep[curentStep]);
                }
            }
        });
    }
});
$(function() {
    if ($(window).width() > 1023) {
        if ($(".Section_1").length) {
            TweenMax.to($(".Section_1"), 2.5, { css: { opacity: "1" } });
            setTimeout(firstAnim, 2000);
        }
    }

    function firstAnim() {
        TweenMax.to($(".Section_1 .Topbar"), 1.2, { css: { top: "0" } });
        TweenMax.to($(".Section_1 .AnimBlock"), 1.2, {
            css: { transform: "scale(1.1)" }
        });
        TweenMax.to($(".Section_1 .CenterBlock_img"), 0.8, {
            css: { transform: "scale(1)" }
        });
        let link = TweenMax.to($(".Section_1 .CenterBlock_link"), 1, {
            css: { margin: "0 0 20px 0", opacity: "1" }
        });
        link.delay(1.5);
        let linkSpan = TweenMax.to($(".Section_1 .CenterBlock_line"), 0.5, {
            css: { height: "10%" }
        });
        linkSpan.delay(1.8);
    }

    $(document).ready(function() {
        if ($(".longread-fullpage").length) {
            $(".longread-fullpage").fullpage({
                verticalCentered: true,
                responsiveWidth: 1024,
                responsiveHeight: 700,
                scrollingSpeed: 700,
                scrollOverflow: true,
                licenseKey: "OPEN-SOURCE-GPLV3-LICENSE",
                normalScrollElements: "#ymaps",
                scrollOverflowOptions: {
                    preventDefault: false,
                    preventDefaultException: {
                        tagName: /^(INPUT|TEXTAREA|BUTTON|SELECT|LABEL|YMAPS)$/
                    }
                },
                onLeave: function(link, index, direction) {
                    if (window.matchMedia('(min-width: 1023px)').matches) {
                        if (index == 2 && direction == "down") {
                            $(".Header").addClass("Header-white");
                            $('.Burger').addClass('Burger-blue')
                        }

                        if (index == 1 && direction == "up") {
                            $(".Header").removeClass("Header-white");
                            $('.Burger').removeClass('Burger-blue')
                        }
                    }
                }
            });
        }
    });
});

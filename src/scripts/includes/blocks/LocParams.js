$(function () {

    var _getUrlParams = function () {
        var str = location.search;
        if (str.indexOf('?') < 0) {
            return {};
        }
        var array = str.replace('?', '').split('&');
        var obj = {};

        for (var i = 0; i < array.length; ++i) {
            var ar = array[i];
            var name = ar.split('=')[0];
            var value = ar.split('=')[1];

            if (name.indexOf('[]') >= 0) {

                if (!Array.isArray(obj[name])) {
                    obj[name] = [];
                }

                if (obj[name].indexOf(value) < 0) {
                    obj[name].push(value);
                }

            } else {
                obj[name] = value;
            }
            
        }
        return obj;
    }

    var _setParamsUrl = function (params) {
        var len = Object.keys(params).length;
        var index = --len;
        var str = '?';
        for (var key in params) {

            if (Array.isArray(params[key])) {
                var len = params[key].length;
                for (var i = 0; i < params[key].length; ++i) {
                    var val = params[key][i];

                    str += key + '=' + val;

                    if (i < (len - 1)) {
                        str += '&';
                    }
                }

            } else {
                str += key + '=' + params[key];
            }
            
            if (index) {
                str += '&';
            }
            index--;
        }

        window.history.replaceState('', '', str);
    }

    var _removeParamsArray = function (array, find) {
        var newAr = [];
        var index = array.indexOf(find);

        if (index >= 0) {
            for (var i = 0; i < array.length; ++i) {

                if (i != index) 
                    newAr.push(array[i])
            }
            return newAr;
        }
        return;
    }

    var LocParams = (function () {
        function LocParams() {
            this.params = _getUrlParams();
        };
        return LocParams;
    })();

    LocParams.prototype.getParams = function () {
        this.params = _getUrlParams();
        return _getUrlParams()
    }

    LocParams.prototype.isParam = function (name) {
        return location.search.indexOf(name) >= 0;
    };

    LocParams.prototype.setParams = function () {

        if (arguments.length) {

            if (typeof arguments[0] == 'object') {
                var params = arguments[0];

                for (var key in params) {
                    var param = params[key];

                    if (key.indexOf('[]') >= 0) {

                        if (!Array.isArray(this.params[key])) {
                            this.params[key] = [];
                        }

                        if (this.params[key].indexOf(param) < 0) {
                            this.params[key].push(param);
                        }

                    } else {
                        this.params[key] = param;
                    }
                }
            } else if (typeof arguments[0] == 'string' && arguments.length > 1) {
                var name = arguments[0];
                var value = arguments[1];

                if (value==true) value =1;
                if (value==false) value =0;

                if (name.indexOf('[]') >= 0) {

                    if (!Array.isArray(this.params[name])) {
                        this.params[name] = [];
                    }

                    if (this.params[name].indexOf(value) < 0) {
                        this.params[name].push(value);
                    }

                } else {
                    this.params[name] = value;
                }
            }
        }

        _setParamsUrl(this.params);
    };

    LocParams.prototype.removeParam = function (name, val) {
        var search = location.search;

        if (name.indexOf('[') >= 0) {
            var newName = '&' + name + '=' + val;
            search = search.replace(newName, '');
        }

        this.params[name] = _removeParamsArray(this.params[name], val);

        if (!this.params[name].length) {
            delete this.params[name];
        }
    }

    LocParams.prototype.on = function (callback) {
        window.onpopstate = callback;
    };

    window.LocParams = new LocParams;
})
//parseQueryString function extracted from mithrill.js library

function parseQueryString(string) {
    if (string === "" || string == null) return {};
    if (string.charAt(0) === "?") string = string.slice(1);
    var entries = string.split("&"),
        data0 = {},
        counters = {};
    for (var i = 0; i < entries.length; i++) {
        var entry = entries[i].split("=");
        var key5 = decodeURIComponent(entry[0]);
        var value = entry.length === 2 ? decodeURIComponent(entry[1]) : "";
        if (value === "true") value = true;
        else if (value === "false") value = false;
        var levels = key5.split(/\]\[?|\[/);
        var cursor = data0;
        if (key5.indexOf("[") > -1) levels.pop();
        for (var j = 0; j < levels.length; j++) {
            var level = levels[j],
                nextLevel = levels[j + 1];
            var isNumber = nextLevel == "" || !isNaN(parseInt(nextLevel, 10));
            var isValue = j === levels.length - 1;
            if (level === "") {
                var key5 = levels.slice(0, j).join();
                if (counters[key5] == null) counters[key5] = 0;
                level = counters[key5]++;
            }
            if (cursor[level] == null) {
                cursor[level] = isValue ? value : isNumber ? [] : {};
            }
            cursor = cursor[level];
        }
    }
    return data0;
}

class FlatsList {
    constructor(opts) {
        this.tpl = _.template(opts.tpl.replace(/\s{2,}/g, ''));
        this.additionalClassName = opts.additionalClassName;
        this.opts = opts;
    }

    render(cardsArray) {
        let tpl = this.tpl;
        let listMarkup = tpl(cardsArray);
        this.opts.$mountEl.html(listMarkup);
    }
}

let cardTplStr = `
/*=require ./includes/chunks/Chess_new.tpl*/
`;

let flatsListBuilder = new FlatsList({
    tpl: cardTplStr,
    $mountEl: $(".ChessScheme"),
});

{
    class SectionsSlider {
        constructor(el) {
            if (!el) {
                throw new Error('SectionsSlider constructor requires "el" argument!');
            }

            let $el = $(el);
            let trackEl = $el.find(".swiper-wrapper").get(0);

            let storedValues = parseQueryString(window.location.search);
            let numActiveSection = storedValues.section;

            let sliderOpts = {
                initialSlide: numActiveSection - 1,
                // slidesPerView: 4.5,
                slidesPerView: "auto",
                // centeredSlides: true,
                slideToClickedSlide: false,
                resizeReInit: true,
                resistanceRatio: 0,
                // noSwiping: true,
                // touchEventsTarget: 'wrapper',
                breakpoints: {
                    767: {
                        initialSlide: 0,
                        slidesPerView: 1,
                    }
                },
                navigation: {
                    nextEl: '.ChessScheme_sliderBtnNext',
                    prevEl: '.ChessScheme_sliderBtnPrev',
                },
                pagination: {
                    el: '.ChessScheme_sliderPagerInner',
                    clickable: true,
                    type: 'custom',
                    bulletClass: 'ChessScheme_sliderPagerItem',
                    bulletActiveClass: 'ChessScheme_sliderPagerItem-active',
                    renderCustom: function (swiper, current, total) {
                        let text = '';
                        for (let i = 0; i < total; i++) {
                            let activeIndex = swiper.activeIndex;
                            let isActive = (i === activeIndex);
                            let isNext = (i === (activeIndex + 1));
                            let isPrev = (i === (activeIndex - 1));
                            let isNextNull = (activeIndex + 1) > total - 1;
                            let isPrevNull = (activeIndex - 1) < 0;

                            if (isActive) {
                                text += '<span class="ChessScheme_sliderPagerItem ChessScheme_sliderPagerItem-visible ChessScheme_sliderPagerItem-active">' + (i + 1)  + '</span>';
                            } else if (isNext) {
                                text += '<span class="ChessScheme_sliderPagerItem ChessScheme_sliderPagerItem-visible">' + (i + 1) + '</span>';
                            } else if (isPrev) {
                                text += '<span class="ChessScheme_sliderPagerItem ChessScheme_sliderPagerItem-visible">' + (i + 1) + '</span>';
                            } else if (isPrevNull) {
                                if (i === (activeIndex + 2)) {
                                    text += '<span class="ChessScheme_sliderPagerItem ChessScheme_sliderPagerItem-visible">' + (i + 1) + '</span>';
                                } else {
                                    text += '<span class="ChessScheme_sliderPagerItem">' + (i + 1) + '</span>';
                                }
                            } else if (isNextNull) {
                                if (i === (activeIndex - 2)) {
                                    text += '<span class="ChessScheme_sliderPagerItem ChessScheme_sliderPagerItem-visible">' + (i + 1) + '</span>';
                                } else {
                                    text += '<span class="ChessScheme_sliderPagerItem">' + (i + 1) + '</span>';
                                }
                            }
                            else {
                                text += '<span class="ChessScheme_sliderPagerItem">' + (i + 1) + '</span>';
                            }
                        }
                        return text;
                    }
                },
                on: {
                    setTranslate: function(translate) {
                        // fix chrome bluring bug
                        trackEl.style.transform = "translate(" + translate + "px, 0)";
                    }
                }
            };

            this.slider = new Swiper($el, sliderOpts);
            this.$el = $el;

            $(window).on(
                "resize",
                _.debounce(event => {
                    event.preventDefault();
                    this.slider.destroy();
                    this.slider = new Swiper($el, sliderOpts);
                }, 100)
            );
        }

        destroy() {
            $(window).off(".ChessScheme_slider");
            this.slider.destroy();
        }
    }

    let bldBtn = $('.BldChessSwitcher_btn');
    let bldBtnActiveClass = 'BldChessSwitcher_btn-active';

    bldBtn.on('click', function () {
        bldBtn.removeClass(bldBtnActiveClass);
        $(this).addClass(bldBtnActiveClass);
        let activeBld = $(this).data('bld');
        let activeBldUrl = $(this).data('bld-url');
        let inputBld =$('#bld');
        let inputSection = $('#section');
        inputSection.val(1)
        inputBld.val(activeBld);
        inputBld.trigger('change');
    });

    class FilterForm {
        constructor(
            el,
            opts = {
                submitHandler1: $.noop,
                submitHandler2: $.noop,
            }
        ) {
            if (!el) {
                console.error('Filter class constructor requires "el" argument!');
                return;
            }

            this.offset = 0;

            const submitHandler1 = _.debounce(opts.submitHandler1, 600);
            const submitHandler2 = _.debounce(opts.submitHandler2, 1200);

            let $filterForm = $(el);
            this.$filterForm = $filterForm;

            this.restoreStateFromURL();

            if (!$filterForm.length) {
                console.error('FilterForm constructor can\'t find given "el" in DOM!');
                return;
            }


            let $inputs = $($filterForm.get(0).elements);
            $inputs.on("change", (event) => {
                event.preventDefault();
                this.offset = 0;
                this.saveStateToURL();

                let inpName = event.target.getAttribute('name');
                if ( inpName !== 'section' && inpName !== "bld") {
                    $filterForm.trigger("submit", "flats");
                } else if ( inpName === 'bld') {
                    $filterForm.trigger("submit", "bld");
                }

            });

            $filterForm.on("submit", (event, data) => {
                event.preventDefault();
                if (data === 'bld') {
                    submitHandler1($filterForm);
                } else if  (data === 'flats') {
                    submitHandler2($filterForm);
                } else {
                    submitHandler1($filterForm);
                    submitHandler2($filterForm);
                }
                this.saveStateToURL();
                return false;
            });

            $(".FiltersOpts_resetBtn").on("click", () => {
                this.resetFilterForm();
            });

            // remember thirdparties url params
            {
                let urlParams = parseQueryString(window.location.search);

                $inputs.each((index, el) => {
                    if (urlParams[el.name]) {
                        delete urlParams[el.name];
                    }
                });

                let otherPossibleParams = ["flat_num", "sort", "sortdir", "offset"];

                for (var i = otherPossibleParams.length - 1; i >= 0; i--) {
                    delete urlParams[otherPossibleParams[i]];
                }

                let thirdpartyUrlParamsStr = "";
                for (let key in urlParams) {
                    thirdpartyUrlParamsStr += `&${key}=${urlParams[key]}`;
                }

                this.thirdpartyUrlParamsStr = thirdpartyUrlParamsStr;
            }

            this.$inputs = $inputs;

            $filterForm.trigger("submit");
        }

        resetFilterForm() {
            this.$filterForm[0].reset();
        }

        saveStateToURL() {
            let queryStr = this.$filterForm.serialize();
            let newURL = location.pathname + "?" + queryStr + this.thirdpartyUrlParamsStr;
            history.replaceState({}, document.title, newURL);
        }

        restoreStateFromURL() {
            let storedValues = parseQueryString(window.location.search);

            // console.log("restored form states:", storedValues);
            let $inps = $(this.$filterForm.get(0).elements);

            for (let p in storedValues) {
                if (storedValues.offset) {
                    this.offset = storedValues.offset;
                }
                // TODO: optimize selector for filtering inputs
                $inps.filter(`[name='${p}'], [name='${p}[]']`).each((index, el) => {
                    if (el.type && (el.type === "checkbox" || el.type === "radio")) {
                        let isChecked = true;

                        if (_.isArray(storedValues[p])) {
                            isChecked = _.includes(storedValues[p], el.value);
                        }

                        el.checked = isChecked;
                    } else if (el.tagName === "SELECT") {
                        if (storedValues[p]) {
                            for (var i = 0; i < el.options.length; i++) {
                                let opt = el.options[i];
                                if (storedValues[p].includes(opt.value)) {
                                    opt.selected = "selected";
                                }
                            }
                        }
                    } else {
                        el.value = storedValues[p];
                    }
                });
            }
        }
    }

    let filter = new FilterForm("#ChessFilter", {
        submitHandler1: ($filterForm) => {

            let data = $filterForm.serializeObject();

            let dataForFirstQuery = Object.assign({}, data)
            let dataForSecondQuery = Object.assign({}, data)

            for (let p in dataForFirstQuery) {
                if (p !== 'bld' && p !== 'section') {
                    delete dataForFirstQuery[p];
                }
            }

            for (let p in dataForSecondQuery) {
                if (p === 'bld' || p === 'section') {
                    delete dataForSecondQuery[p];
                }
            }

            $.ajax({
                url: $('[data-bld-url]').data('bld-url'),
                dataType: "json",
                method: "GET",
                data: dataForFirstQuery,
            })
                .done(jsonResponse => {

                    flatsListBuilder.render({
                        data: jsonResponse.sections,
                    });


                    $('.ChessScheme_bild').addClass('ChessScheme_bild-visible');
                    $('.ChessScheme_sliderPager').addClass('ChessScheme_sliderPager-visible');

                    {
                        let inpBldVal = $('input[name="bld"]').val();
                        bldBtn.removeClass(bldBtnActiveClass);
                        $('.BldChessSwitcher_btn[data-bld="'+ inpBldVal +'"]').addClass(bldBtnActiveClass);
                    }

                    // let storedValues = parseQueryString(window.location.search);
                    // let numActiveSection = storedValues.section;

                    let sectionsSlider = new SectionsSlider(".ChessSliderNew");
                    let chessSlider = sectionsSlider.slider;

                    chessSlider.on('slideChange', function () {
                        console.log('change')
                        let sectionInp = $('input[name="section"]');
                        let activeSlide = this.activeIndex + 1;
                        console.log('activeSlide', activeSlide)
                        sectionInp.val(activeSlide);
                        sectionInp.trigger('change');
                        setSchemeSections();
                        setCompassSections();
                    });

                    let schemeWrap = $('.ChessScheme_bild');
                    let compassWrap = $('.ChessScheme_compass');
                    schemeWrap.find('img').remove();
                    compassWrap.find('img').remove();

                    jsonResponse.sections.forEach(function (sect, index) {
                        schemeWrap.append('<img src="'+ sect.schemeURL +'" title="" data-slide-section="'+ index +'"/>');
                        compassWrap.append('<img src="'+ sect.compassURL +'" title="" data-slide-section="'+ index +'"/>');
                    });

                    let setSchemeSections = ()=> {
                        let activeSlideIndex = chessSlider.activeIndex;
                        schemeWrap.find('[data-slide-section]').removeClass('_active');
                        schemeWrap.find('[data-slide-section="'+ activeSlideIndex +'"]').addClass('_active');
                    };
                    let setCompassSections = ()=> {
                        let activeSlideIndex = chessSlider.activeIndex;
                        compassWrap.find('[data-slide-section]').removeClass('_active');
                        compassWrap.find('[data-slide-section="'+ activeSlideIndex +'"]').addClass('_active');
                    };

                    setSchemeSections();
                    setCompassSections();

                    let flatTooltip;

                    let chessFlatLink = $('[data-chess-flat-link]');

                    chessFlatLink.on('click', function (e) {
                        // event.preventDefault()
                        console.log($(this).position())
                    })

                    // tippy('[data-chess-flat-link]', {
                    //     content: 'Tooltip',
                    //     trigger: 'click',
                    //     appendTo: "parent"
                    // })

                    chessFlatLink.on('mouseover', function() {
                        let tooltipFlats = () => {
                            let winWidth = $(window).width();
                            if (winWidth > 1023) {
                                flatTooltip = null;
                                flatTooltip = tippy(this, {
                                    animateFill: false,
                                    animation: 'fade',
                                    duration: 400,
                                    distance: -25,
                                    offset: 150,
                                    theme: 'tooltip-flat',
                                    arrow: false,
                                    appendTo: "parent",
                                    content: function(reference) {
                                        let data = reference.querySelector(".Chess_flatInfo").innerHTML;
                                        let template = `<div class="Chess_flatInfo" role="tooltip">`+ data +`</div>`;
                                        return template;
                                    },
                                    placement: 'right-end'
                                });
                            } else {
                                let used;
                                flatTooltip = null;

                                $('[data-chess-flat-link]').on('click', function (e) {
                                    if( used !== this) used = this, e.preventDefault();
                                });

                                flatTooltip = tippy(this, {
                                    animateFill: false,
                                    animation: 'fade',
                                    duration: 400,
                                    distance: -25,
                                    trigger: 'click',
                                    offset: -50,
                                    theme: 'tooltip-flat',
                                    arrow: false,
                                    appendTo: "parent",
                                    content: function(reference) {
                                        let data = reference.querySelector(".Chess_flatInfo").innerHTML;
                                        let template = `<div class="Chess_flatInfo" role="tooltip">`+ data +`</div>`;
                                        return template;
                                    },
                                    placement: 'right-end'
                                });
                            }
                        };
                        tooltipFlats();
                    });

                })
                .fail(() => {
                    alert("РќРµ СѓРґР°Р»РѕСЃСЊ РїРѕР»СѓС‡РёС‚СЊ РґР°РЅРЅС‹Рµ!\nРџРѕРїСЂРѕР±СѓР№С‚Рµ РїРѕР·Р¶Рµ РёР»Рё РѕР±СЂР°С‚РёС‚РµСЃСЊ Рє Р°РґРјРёРЅРёСЃС‚СЂР°С†РёРё СЃР°Р№С‚Р°.");
                });

        },
        submitHandler2: ($filterForm) => {

            let data = $filterForm.serializeObject();

            let dataForFirstQuery = Object.assign({}, data)
            let dataForSecondQuery = Object.assign({}, data)

            for (let p in dataForFirstQuery) {
                if (p !== 'bld' && p !== 'section') {
                    delete dataForFirstQuery[p];
                }
            }

            for (let p in dataForSecondQuery) {
                if (p === 'section') {
                    delete dataForSecondQuery[p];
                }
            }

            let $progress = $(".b-options-page__progress");
            let $chessCont = $('.Chess');
            if ($chessCont.length) {
                $chessCont.animate({ opacity: 0.5 }, 600);
            }
            $progress.show().animate({ width: "33%", opacity: 1 }, 600);

            $.ajax({
                url: $filterForm.attr("data-url-flats"),
                dataType: "json",
                method: $filterForm.attr("method"),
                data: dataForSecondQuery,
                xhr: () => {
                    let xhr = new window.XMLHttpRequest();
                    xhr.addEventListener("progress", (event) => {
                        if (event.lengthComputable) {
                            let percent = Math.ceil((100 * event.loaded) / event.total);
                            $progress.stop(true, false).animate({ width: percent + "%" }, 400);
                            if (percent === 100) {
                                $progress.animate({ opacity: 0 }, 350, () => {
                                    $progress.removeAttr("style");
                                });
                                $chessCont.animate({ opacity: 1 }, 600);
                            }
                        }
                    });

                    return xhr;
                },
            })
                .done((jsonResponse) => {
                    let filteredFlats = jsonResponse.filteredFlats;
                    let flatsNotNull = 'filteredFlats' in jsonResponse;

                    if ( flatsNotNull ) {
                        let filteredFlatsCount = filteredFlats.length;

                        $('.Chess_cell').addClass('Chess_cell-notFilter');

                        for (let i = 0; i < filteredFlats.length; i++) {
                            $('.Chess_cell[data-id="'+ filteredFlats[i] +'"]').removeClass('Chess_cell-notFilter');
                        }
                        $('.js-option-flat-count').text(filteredFlatsCount);

                    } else {
                        $('.Chess_cell').addClass('Chess_cell-notFilter');
                        $('.js-option-flat-count').text(0);
                    }
                })
                .fail(() => {
                    alert("РќРµ СѓРґР°Р»РѕСЃСЊ РїРѕР»СѓС‡РёС‚СЊ РґР°РЅРЅС‹Рµ СЃ СЃРµСЂРІРµСЂР°!\nРџРѕРїСЂРѕР±СѓР№С‚Рµ РїРѕР·Р¶Рµ.");
                });

        },

    });

    let optionSelect = $('#options');

    optionSelect.on('change', function(){
        changeOptionsSelectLabel();
    });

    let changeOptionsSelectLabel = ()=> {
        let onlyFlats = parseInt(optionSelect.find('option:selected').val());
        if (onlyFlats == 0) {
            optionSelect.parent().addClass('b-select--noapps');
        } else {
            optionSelect.parent().removeClass('b-select--noapps');
        }
    };
    changeOptionsSelectLabel();
}
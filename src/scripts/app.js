const $window  = $(window);
const $body    = $('body');
const animated = '_animated';
const animWidth = window.innerWidth;

let setLineHeight;

$(() => {
    if (animWidth > 767) {
        $('[data-header-anim]').each(function() {
            let $this = $(this);

            setTimeout(function() {
                TweenMax.to($this, 1.2, {
                    scale: "1.1"
                });
            }, 2000);
        });
    }

    if ($('[data-header-descr]').length) {
        let $wrapper = $('[data-header-descr]');
        let $line    = $wrapper.find('.page-header-descr__line');
        let curPos   = $wrapper.offset().top;

        setLineHeight = function () {
            let winHeigt = $window.height();

            $line.css('height', winHeigt - curPos - $wrapper.outerHeight());
        };
        setLineHeight();

        $window.on('resize orientationchange', setLineHeight);
    }

    //Запуск анимации блоков на главной

    function isElementInViewport($elem) {
        // Get the scroll position of the page.
        var scrollElem     = $('body, html');
        var viewportTop    = $(scrollElem).scrollTop();
        var viewportBottom = viewportTop + $(window).height();

        // Get the position of the element on the page.
        var elemTop    = Math.round( $elem.offset().top );
        var elemBottom = elemTop + $elem.height();

        return ((elemTop < viewportBottom) && (elemBottom > viewportTop + 50));
    }

    // Check if it's time to start the animation.
    function checkAnimation() {
        var $elems = $('[data-animation-item]');

        $elems.each(function() {
            var $self = $(this);

            // If the animation has already been started
            if ($self.hasClass(animated)) return;

            if (isElementInViewport($self)) {
                // Start the animation
                $self.addClass(animated);
            }
        });

    }

    checkAnimation();
});

const widthWindow = window.innerWidth;

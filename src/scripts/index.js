$(document).ready(function() {
    // setTimeout(function() {
    //     $('#cloud-video').get(0).play();
    //     $('#cloud-outer').get(0).play();
    // }, 300);

    $('[data-video]').fancybox({
        helpers: {
            media: true
        },
        youtube: {
            autoplay: 1,
            start: 0
        }
    });

    if ($(window).width() < 1024) {
        $(".section").removeClass("fp-table");
    }

    if ($(window).width() > 1023) {
        TweenMax.to($(".Section_1"), 2.5, { css: { opacity: "1" } });

        setTimeout(firstAnim, 2000);
    }

    if ($(window).width() > 1023 && $("body").height() < 400) {
        setTimeout(function() {
            $(".Hide_Block").css("display", "flex");

            $("html").css("overflow", "hidden");
        }, 1000);
    } else {
        $(".Hide_Block").css("display", "none");

        $("html").css("overflow-x", "hidden");
    }

    $(window).resize(function() {
        if ($(window).width() > 1024 && $(window).height() < 400) {
            $(".Hide_Block").css("display", "flex");

            $("html").css("overflow", "hidden");
        } else {
            $(".Hide_Block").css("display", "none");

            $("html").css("overflow-x", "hidden");
        }
    });
    function firstAnim() {
        TweenMax.to($(".Section_1 .Topbar"), 1.2, { css: { toSection_1p: "0" } });

        TweenMax.to($(".Section_1 .AnimBlock"), 1.2, { css: { transform: "scale(1.1)" } });

        TweenMax.to($(".Section_1 .CenterBlock_img"), 0.8, { css: { transform: "scale(1)" } });

        animMainTitle();

        let link = TweenMax.to($(".Section_1 .CenterBlock_link"), 1, { css: { margin: "0 0 20px 0", opacity: "1" } });

        link.delay(1.5);

        // высота и позиция нижней полоски от внутренней рамки видео до низа страницы
        // let heightWindow = $(window).height();
        // let innerHeight = $('.CenterBlockFrame_inner').height();
        // let offsetInner = $('.CenterBlockFrame_inner').offset().top;
        // let heightToBottomFrame = heightWindow - innerHeight - offsetInner;
        // let heightBetweenFrames = innerHeight*0.04;
        // let heightLine = heightToBottomFrame + heightBetweenFrames;
        // $('.CenterBlock_line').css({
        //     "top": heightWindow - heightLine
        // });
        //
        // let linkSpan = TweenMax.to($(".Section_1 .CenterBlock_line"), 0.5, { css: { height: heightLine } });

        // linkSpan.delay(1.8);
    }

    function startAnimation1(section) {
        TweenMax.to($(".Section_" + section + " .CenterBlock_num"), 1, {
            css: { opacity: "1", left: "0" },
            delay: 0.3,
        });

        TweenMax.to($(".Section_" + section + " .CenterBlock_title"), 1, {
            css: { opacity: "1", left: "0" },
            delay: 0.6,
        });

        TweenMax.to($(".Section_" + section + " .CenterBlock_text"), 1, {
            css: { opacity: "1", left: "0" },
            delay: 0.9,
        });

        TweenMax.to($(".Section_" + section + " .CenterBlock_btn"), 1, {
            css: { opacity: "1", marginLeft: "0" },
            delay: 1.2,
        });

        TweenMax.to($(".Section_" + section + " .AnimBlock"), 1, { css: { transform: "scale(1)" } });

        TweenMax.to($(".Section_" + section + " .CenterBlock_item_item"), 1, { css: { width: "100%" } });
    }

    function startAnimation2(section) {
        TweenMax.to($(".Section_" + section + " .AnimBlock"), 1, { css: { backgroundSize: "110% 110%" } });

        TweenMax.to($(".Section_" + section + " .CenterBlock_item_media"), 1, { css: { transform: "scale(1)" } });
    }

    function startAnimation3(section) {
        TweenMax.to($(".Section_" + section + " .CenterBlock_title"), 1, {
            css: { opacity: "1", left: "0" },
            delay: 0.6,
        });

        TweenMax.to($(".Section_" + section + " .CenterBlock_text"), 1, {
            css: { opacity: "1", left: "0" },
            delay: 0.9,
        });

        TweenMax.to($(".Section_" + section + " .CenterBlock_btn"), 1, {
            css: { opacity: "1", marginLeft: "0" },
            delay: 1.2,
        });

        TweenMax.to($(".Section_" + section + " .CenterBlock_item:nth-of-type(2)"), 1, {
            css: { transform: "scale(1)" },
        });

        $("svg #Mask-Copy__" + section + "").addClass("anim");

        $("svg #Rectangle-Copy__" + section + "").addClass("anim");
    }

    function startAnimationLeave1(section) {
        TweenMax.to($(".Section_" + section + " .CenterBlock"), 1, { css: { backgroundSize: "100%" } });

        TweenMax.to($(".Section_" + section + " .CenterBlock_item_item"), 1, { css: { width: "100%" }, delay: 0.2 });
    }

    function startAnimationLeave2(section) {
        TweenMax.to($(".Section_" + section + " .CenterBlock_item_inner_wrap"), 1, {
            css: { width: "100%" },
            delay: 0.2,
        });

        TweenMax.to($(".Section_" + section + " .CenterBlock_item_pic"), 0.1, { css: { opacity: "1" }, delay: 1.2 });

        TweenMax.to($(".Section_" + section + " .CenterBlock_item_inner_wrap"), 1, {
            css: { left: "100%" },
            delay: 1.2,
        });
    }

    function startNum(section) {
        if ($(window).width() > 1023) {
            TweenMax.to($(".Section_" + section + " .CenterBlock_num"), 1, {
                css: { opacity: "1", left: "0" },
                delay: 0.3,
            });
        }

        if ($(window).width() > 1919) {
            TweenMax.to($(".Section_" + section + " .CenterBlock_num"), 1, {
                css: { opacity: "1", left: "-63px" },
                delay: 0.3,
            });
        }
    }

    $("main").fullpage({
        verticalCentered: true,
        responsiveWidth: 1024,
        responsiveHeight: 700,
        scrollingSpeed: 700,
        afterLoad: function(link, index) {
            // if (index == 1) {
            //     if ($(".Header").hasClass("Topbar_active") == true) {
            //         TweenMax.to($(".Header"), 0.2, { css: { display: "flex", opacity: "1" } });
            //     }
            // }

            if (index == 2) {

                if ($(window).width() > 1023) {
                    startAnimation1(2);
                }

                if ($(window).width() > 1279) {
                    startAnimation2(2);
                }

                startNum(2);
            }

            if (index == 3) {
                if ($(window).width() > 1023) {
                    startAnimation3(3);
                }

                if ($(window).width() > 1919) {
                }

                startNum(3);
            }

            if (index == 4) {
                if ($(window).width() > 1023) {
                    startAnimation1(4);
                }

                if ($(window).width() > 1279) {
                    startAnimation2(4);
                }

                startNum(4);
            }

            if (index == 5) {
                if ($(window).width() > 1023) {
                    startAnimation3(5);
                }

                startNum(5);
            }

            if (index == 6) {
                if ($(window).width() > 1023) {
                    startAnimation1(6);
                }

                if ($(window).width() > 1279) {
                    startAnimation2(6);
                }

                startNum(6);
            }
        },

        onLeave: function(link, index, direction) {
            if (index == 2 && direction == "down") {
                $(".Header").addClass("Header-white");
                $(".Burger").addClass("Burger-blue");

            }

            if (index == 1 && direction == "up") {
                $(".Burger").removeClass("Burger-blue");
                $(".Header").removeClass("Header-white");
            }

            if (index == 2) {

                if ($(window).width() > 1023) {
                    // TweenMax.to($(".Header"), 0.2, { css: { display: "flex", opacity: "1" } });

                    startAnimationLeave1(2);
                }
            }

            if (index == 3) {
                if ($(window).width() > 1023) {
                    startAnimationLeave2(3);
                }
            }

            if (index == 4) {
                if ($(window).width() > 1023) {
                    startAnimationLeave1(4);
                }
            }

            if (index == 5) {
                if ($(window).width() > 1023) {
                    startAnimationLeave2(5);
                }
            }

            if (index == 6) {
                if ($(window).width() > 1023) {
                    startAnimationLeave2(6);
                }
            }
        },
    });

    if ($(window).width() < 768) {
        //$(".Section_1").css({ height: "650px" });

        $(".Section_2").css({ height: "782px" });

        $(".Section_3").css({ height: "835px" });

        $(".Section_4").css({ height: "720px" });

        $(".Section_5").css({ height: "741px" });

        $(".Section_6").css({ height: "750px" });

        $(".Section_7").css({ height: "322px" });

        $(".fp-tableCell").css({ height: "100%" });
    }

    if ($(window).width() > 767 && $(window).width() < 1024) {
        //$(".Section_1").css({ height: "1027px" });

        $(".Section_2").css({ height: "1024px" });

        $(".Section_3").css({ height: "1024px" });

        $(".Section_4").css({ height: "1024px" });

        $(".Section_5").css({ height: "1024px" });

        $(".Section_6").css({ height: "1024px" });

        $(".Section_7").css({ height: "570px" });

        $(".fp-tableCell").css({ height: "100%" });
    }

    if ($(window).width() > 1023) {
        $(".section").height($(window).height());

        $(".fp-tableCell").height($(window).height());
    }

    let heightSec_1 = $(window).height();
    console.log('heightSec_1', heightSec_1)
    $(".Section_1").css({ height: heightSec_1 });

    function animMainTitle() {
        let $startAnim = $("#start-anim");

        let $title = $("h1.CenterBlock_title");
        let $text  = $("h2.CenterBlock_text");
        let $play  = $(".CenterBlock_play");

        $title.html(
            $title
                .html()
                .replace(/[^<br>]/g, "<span>$&</span>")
                .replace(/\s/g, "&nbsp;")
        );

        $text.html(
            $text
                .html()
                .replace(/./g, "<span>$&</span>")
                .replace(/\s/g, "&nbsp;")
        );

        $startAnim.ready(startAnimation);

        $startAnim.on("load", startAnimation);

        function startAnimation() {
            $title.css("opacity", 1);
            $text.css("opacity", 1);
            $play.css("opacity", 1);

            TweenLite.set($startAnim, { autoAlpha: 0 });

            TweenMax.staggerFromTo(
                $title.find("span"),
                0.05,

                { autoAlpha: 0, scale: 2 },
                { autoAlpha: 1, scale: 1, delay: 1 },
                0.015,
                reset
            );

            TweenMax.staggerFromTo(
                $text.find("span"),
                0.08,

                { autoAlpha: 0, scale: 2 },
                { autoAlpha: 1, scale: 1, delay: 1.3 },
                0.015,
                reset
            );
        }

        function reset() {
            TweenMax.to($startAnim, 2, { autoAlpha: 1 });
        }
    }

    let setPositionHeightFrameBlockLine = () => {
        let heightWindow = $(window).height();
        let innerHeight = $('.CenterBlockFrame_inner').height();
        let offsetInner = $('.CenterBlockFrame_inner').offset().top;
        let heightToBottomFrame = heightWindow - innerHeight - offsetInner;
        let heightBetweenFrames = innerHeight*0.04;
        let heightLine = heightToBottomFrame + heightBetweenFrames;
        $('.CenterBlock_line').css({
            "top": heightWindow - heightLine,
            "height": heightLine
        });
    };

    if ($(window).width() < 1024) {
        setPositionHeightFrameBlockLine();
    }



//=include includes/blocks/validate.js
//=include includes/blocks/events.js
//=include includes/blocks/ajax.js
//=include includes/blocks/ajax-param.js
//=include includes/blocks/ajax-form.js

});


$(function() {

    var actNum = window.location.hash.slice(-1) - 1;
    let DecorStyleSlider;

    if ( actNum < 0 ) {
        actNum = 0;
    }

    let decorTabs = $( "#DecorStyleTabs" );

    if (decorTabs.length) {
        let url = window.location.href;

        let urlLenght = url.length;
        if ( !!(~url.indexOf('#')) == true ) {
            for(let i=0; i <= urlLenght; i++) {
                if ( url[i] === '#' ) {
                    url = url.slice(0, urlLenght - (urlLenght - i) )
                }
            }
        }

        let initTabs = (actNum)=> {
            decorTabs.tabs({
                active: actNum,
                activate: function(event, ui) {
                    var scrollTop = $(window).scrollTop();
                    let hash = ui.newPanel.attr('data-hash');
                    let newUrl = `${url}#${hash}`;
                    window.history.replaceState(undefined, undefined, newUrl);
                    $(window).scrollTop(scrollTop);
                }
            });
        };
        initTabs(actNum);

        let actNumForSlide = actNum + 1;
        let initialSlideIndex = $('.DecorStyle_sliderItem[data-tab="'+actNumForSlide+'"]').eq(0).data('slide-id');

        DecorStyleSlider = new Swiper ('.DecorStyle_slider', {
            // loop: true,
            initialSlide: initialSlideIndex,
            navigation: {
                nextEl: '.DecorStyle-next',
                prevEl: '.DecorStyle-prev',
            },
            resistanceRatio: 0,
            observer: true,
            observeParents: true,
            observeSlideChildren: true
        });

        DecorStyleSlider.on('slideChangeTransitionEnd', function () {
            let $activeSlide = $('.DecorStyle_sliderItem.swiper-slide-active')
            let activeTab = $activeSlide.data('tab') - 1;
            decorTabs.tabs( "destroy" );
            initTabs(activeTab);

            let numTab = $activeSlide.data('tab');
            let idTabBlock = $('.DecorStyleTabs_item a[data-slide="'+numTab+'"]').attr('href');
            let hashTab = $(idTabBlock).data('hash');
            var scrollTop = $(window).scrollTop();
            let newUrl = `${url}#${hashTab}`;
            window.history.replaceState(undefined, undefined, newUrl);
            $(window).scrollTop(scrollTop);

            // $('[data-slide="'+ activeTab +'"]').trigger('click', 'slider');
        });
        $('.DecorStyleTabs_link').on('click', function (event, data) {
            let activeSlide = $(this).data('slide');
            let newActiveSlide = $('.DecorStyle_sliderItem[data-tab="'+activeSlide+'"]').eq(0).data('slide-id');
            DecorStyleSlider.slideTo(newActiveSlide);
        });
        // $('.DecorStyleTabs_link').on('click touchstart', function () {
        //     let activeSlide = $(this).data('slide');
        //     let newActiveSlide = $('.DecorStyle_sliderItem[data-tab="'+activeSlide+'"]').eq(0).data('slide-id');
        //     console.log('newActiveSlide', newActiveSlide)
        //     DecorStyleSlider.slideTo(newActiveSlide);
        // });


    } else {
        DecorStyleSlider = new Swiper ('.DecorStyle_slider', {
            // loop: true,
            initialSlide: 0,
            navigation: {
                nextEl: '.DecorStyle-next',
                prevEl: '.DecorStyle-prev',
            },
            resistanceRatio: 0,
            observer: true,
            observeParents: true,
            observeSlideChildren: true
        });
    }


    tippy('.DecorStyle_topBull', {
        animateFill: false,
        animation: 'fade',
        duration: 400,
        distance: 10,
        offset: 200,
        theme: 'tooltip-business',
        boundary: document.querySelector('.DecorStyle'),
        arrow: false,
        content: function(reference) {
            let data = reference.querySelector(".DecorStyle_topToolp").innerHTML;
            let template = `<div class="DecorStyle_topToolp _visible" role="tooltip">`+ data +`</div>`;
            return template;
        },
        flipBehavior: ["top", "left"],
        // trigger: 'click'
        // placement: 'right-end'
        // followCursor: true,

    });

    let setPosTabsMenu = ()=> {
        let heightImgCont = $('.DecorStyle_topItemImg:visible').outerHeight();
        let tabs = $('.DecorStyle_tabs');
        tabs.css("top", heightImgCont);
        $('.DecorStyle_topItem').css("height", heightImgCont)
    };
    setPosTabsMenu();

    $(window).on('resize', function () {
        setTimeout(setPosTabsMenu, 200)
    });
});

/*
{
    let sliders = [];

    let initSliders = ()=> {
        let windowWidth = $(window).width();

        if ( windowWidth > 1023 ) {
            sliders.forEach(function(element) {
                element.destroy(true, true);
            });
        } else {
            $('.swiper-container').each(function (index, el) {
                el.classList.add("s" + index);

                let next = $(this).find('.DecorStyle-next');
                let prev = $(this).find('.DecorStyle-prev')

                sliders.push(new Swiper(el, {
                    loop: true,
                    navigation: {
                        nextEl: next,
                        prevEl: prev,
                    },
                    observer: true,
                    observeParents: true,
                    observeSlideChildren: true
                }));
            });
        }
    };
    initSliders();

    $(window).on('resize', function(){
        initSliders();
    });
}*/

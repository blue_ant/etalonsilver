module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: "eslint:recommended",
  globals: {
    $: true,
    _: true,
    svgPanZoom: true,
    device: true,
  },
  parserOptions: {
    ecmaVersion: 2018,
  },
  rules: {},
};
